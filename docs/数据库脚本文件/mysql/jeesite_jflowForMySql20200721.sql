/*
 Navicat Premium Data Transfer

 Source Server         : 140+mysql
 Source Server Type    : MySQL
 Source Server Version : 50622
 Source Host           : 140.143.236.168:3307
 Source Schema         : jeesitejflow_mysql

 Target Server Type    : MySQL
 Target Server Version : 50622
 File Encoding         : 65001

 Date: 21/07/2020 15:03:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for frm_ctrlmodel
-- ----------------------------
DROP TABLE IF EXISTS `frm_ctrlmodel`;
CREATE TABLE `frm_ctrlmodel`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FrmID` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `CtrlObj` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控制权限',
  `IsEnableAll` int(11) NULL DEFAULT NULL COMMENT '任何人都可以',
  `IsEnableStation` int(11) NULL DEFAULT NULL COMMENT '按照岗位计算',
  `IsEnableDept` int(11) NULL DEFAULT NULL COMMENT '按照绑定的部门计算',
  `IsEnableUser` int(11) NULL DEFAULT NULL COMMENT '按照绑定的人员计算',
  `IsEnableMyDept` int(11) NULL DEFAULT NULL COMMENT '按照本部门计算',
  `IDOfUsers` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的人员ID',
  `IDOfStations` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的岗位ID',
  `IDOfDepts` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的部门ID',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '控制模型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for frm_ctrlmodeldtl
-- ----------------------------
DROP TABLE IF EXISTS `frm_ctrlmodeldtl`;
CREATE TABLE `frm_ctrlmodeldtl`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FrmID` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `CtrlObj` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控制权限',
  `OrgType` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织类型',
  `IDs` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IDs',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '控制模型表Dtl' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for frm_deptcreate
-- ----------------------------
DROP TABLE IF EXISTS `frm_deptcreate`;
CREATE TABLE `frm_deptcreate`  (
  `FrmID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表单 - 主键',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '可以创建部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FrmID`, `FK_Dept`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据部门' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for frm_empcreate
-- ----------------------------
DROP TABLE IF EXISTS `frm_empcreate`;
CREATE TABLE `frm_empcreate`  (
  `FrmID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表单 - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FrmID`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据可创建的人员' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for frm_generbill
-- ----------------------------
DROP TABLE IF EXISTS `frm_generbill`;
CREATE TABLE `frm_generbill`  (
  `WorkID` int(11) NOT NULL COMMENT 'WorkID - 主键',
  `FK_FrmTree` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据类别',
  `FrmID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据ID',
  `FrmName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据名称',
  `BillNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `Title` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `BillSta` int(11) NULL DEFAULT NULL COMMENT '状态(简),枚举类型:0 运行中;1 已完成;2 其他;',
  `BillState` int(11) NULL DEFAULT NULL COMMENT '单据状态,枚举类型:0 空白;1 草稿;2 编辑中;100 归档;',
  `Starter` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `StarterName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人名称',
  `Sender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  `SendDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据活动时间',
  `NDStep` int(11) NULL DEFAULT NULL COMMENT '步骤',
  `NDStepName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '步骤名称',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `DeptName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `PRI` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `SDTOfNode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点应完成时间',
  `SDTOfFlow` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据应完成时间',
  `PFrmID` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父单据编号',
  `PWorkID` int(11) NULL DEFAULT NULL COMMENT '父单据ID',
  `TSpan` int(11) NULL DEFAULT NULL COMMENT '时间段,枚举类型:0 本周;1 上周;2 上上周;3 更早;',
  `AtPara` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数(单据运行设置临时存储的参数)',
  `Emps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '参与人',
  `GUID` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `FK_NY` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年月',
  PRIMARY KEY (`WorkID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据控制表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for frm_method
-- ----------------------------
DROP TABLE IF EXISTS `frm_method`;
CREATE TABLE `frm_method`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FrmID` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `MethodID` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方法ID',
  `MethodName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方法名',
  `WarningMsg` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '功能执行警告信息',
  `RefMethodType` int(11) NULL DEFAULT NULL COMMENT '方法类型,枚举类型:1 模态窗口打开;2 新窗口打开;3 右侧窗口打开;',
  `IsMyBillToolBar` int(11) NULL DEFAULT NULL COMMENT '是否显示在MyBill.htm工具栏上',
  `IsMyBillToolExt` int(11) NULL DEFAULT NULL COMMENT '是否显示在MyBill.htm工具栏右边的更多按钮里',
  `IsSearchBar` int(11) NULL DEFAULT NULL COMMENT '是否显示在Search.htm工具栏上(用于批处理)',
  `PopHeight` int(11) NULL DEFAULT NULL COMMENT '弹窗高度',
  `PopWidth` int(11) NULL DEFAULT NULL COMMENT '弹窗宽度',
  `MsgSuccess` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '成功提示信息',
  `MsgErr` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '失败提示信息',
  `WhatAreYouTodo` int(11) NULL DEFAULT NULL COMMENT '执行完毕后干啥？,枚举类型:0 关闭提示窗口;1 关闭提示窗口并刷新;2 转入到Search.htm页面上去;',
  `Idx` int(11) NULL DEFAULT NULL COMMENT 'Idx',
  `ShowModel` int(11) NULL DEFAULT NULL COMMENT '显示方式,枚举类型:0 按钮;1 超链接;',
  `MethodDocTypeOfFunc` int(11) NULL DEFAULT NULL COMMENT '内容类型,枚举类型:0 SQL;1 URL;2 JavaScript;3 业务单元;',
  `MethodDoc_Url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '连接URL',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单方法' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for frm_stationcreate
-- ----------------------------
DROP TABLE IF EXISTS `frm_stationcreate`;
CREATE TABLE `frm_stationcreate`  (
  `FrmID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表单 - 主键',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '可以创建岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FrmID`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据岗位' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for frm_stationdept
-- ----------------------------
DROP TABLE IF EXISTS `frm_stationdept`;
CREATE TABLE `frm_stationdept`  (
  `FK_Frm` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '单据编号 - 主键',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '工作岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Frm`, `FK_Station`, `FK_Dept`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据岗位部门' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for frm_track
-- ----------------------------
DROP TABLE IF EXISTS `frm_track`;
CREATE TABLE `frm_track`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `ActionType` int(11) NULL DEFAULT NULL COMMENT '类型',
  `ActionTypeText` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型(名称)',
  `FID` int(11) NULL DEFAULT NULL COMMENT '流程ID',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT '工作ID',
  `NDFrom` int(11) NULL DEFAULT NULL COMMENT '从节点',
  `NDFromT` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从节点(名称)',
  `NDTo` int(11) NULL DEFAULT NULL COMMENT '到节点',
  `NDToT` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到节点(名称)',
  `EmpFrom` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从人员',
  `EmpFromT` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从人员(名称)',
  `EmpTo` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到人员',
  `EmpToT` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到人员(名称)',
  `RDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日期',
  `WorkTimeSpan` float NULL DEFAULT NULL COMMENT '时间跨度(天)',
  `Msg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '消息',
  `NodeData` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '节点数据(日志信息)',
  `Tag` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `Exer` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '执行人',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '轨迹表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_app
-- ----------------------------
DROP TABLE IF EXISTS `gpm_app`;
CREATE TABLE `gpm_app`  (
  `No` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `AppModel` int(11) NULL DEFAULT NULL COMMENT '应用类型,枚举类型:0 BS系统;1 CS系统;',
  `Name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '名称',
  `FK_AppSort` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别,外键:对应物理表:GPM_AppSort,表描述:系统类别',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `UrlExt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '默认连接',
  `SubUrl` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '第二连接',
  `UidControl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名控件',
  `PwdControl` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码控件',
  `ActionType` int(11) NULL DEFAULT NULL COMMENT '提交类型,枚举类型:0 GET;1 POST;',
  `SSOType` int(11) NULL DEFAULT NULL COMMENT '登录方式,枚举类型:0 SID验证;1 连接;2 表单提交;3 不传值;',
  `OpenWay` int(11) NULL DEFAULT NULL COMMENT '打开方式,枚举类型:0 新窗口;1 本窗口;2 覆盖新窗口;',
  `RefMenuNo` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '关联菜单编号',
  `AppRemark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ICON',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float NULL DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_appsort
-- ----------------------------
DROP TABLE IF EXISTS `gpm_appsort`;
CREATE TABLE `gpm_appsort`  (
  `No` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  `RefMenuNo` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联的菜单编号',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统类别' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_bar
-- ----------------------------
DROP TABLE IF EXISTS `gpm_bar`;
CREATE TABLE `gpm_bar`  (
  `No` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '名称',
  `Title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '标题',
  `OpenWay` int(11) NULL DEFAULT NULL COMMENT '打开方式,枚举类型:0 新窗口;1 本窗口;2 覆盖新窗口;',
  `IsLine` int(11) NULL DEFAULT NULL COMMENT '是否独占一行',
  `MoreUrl` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '更多标签Url',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '信息块' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_baremp
-- ----------------------------
DROP TABLE IF EXISTS `gpm_baremp`;
CREATE TABLE `gpm_baremp`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Bar` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '信息块编号',
  `FK_Emp` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员编号',
  `Title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '标题',
  `IsShow` int(11) NULL DEFAULT NULL COMMENT '是否显示',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '人员信息块' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_empapp
-- ----------------------------
DROP TABLE IF EXISTS `gpm_empapp`;
CREATE TABLE `gpm_empapp`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Emp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作员',
  `FK_App` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统',
  `Name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '系统-名称',
  `UrlExt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '连接',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float NULL DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员与系统权限' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_empmenu
-- ----------------------------
DROP TABLE IF EXISTS `gpm_empmenu`;
CREATE TABLE `gpm_empmenu`  (
  `FK_Menu` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单 - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单功能,主外键:对应物理表:Port_Emp,表描述:用户',
  `FK_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统,外键:对应物理表:GPM_App,表描述:系统',
  `IsChecked` int(11) NULL DEFAULT NULL COMMENT '是否选中',
  PRIMARY KEY (`FK_Menu`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '人员菜单对应' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_group
-- ----------------------------
DROP TABLE IF EXISTS `gpm_group`;
CREATE TABLE `gpm_group`  (
  `No` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_groupdept
-- ----------------------------
DROP TABLE IF EXISTS `gpm_groupdept`;
CREATE TABLE `gpm_groupdept`  (
  `FK_Group` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限组 - 主键',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Group`, `FK_Dept`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组部门' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_groupemp
-- ----------------------------
DROP TABLE IF EXISTS `gpm_groupemp`;
CREATE TABLE `gpm_groupemp`  (
  `FK_Group` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限组 - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Group`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组人员' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_groupmenu
-- ----------------------------
DROP TABLE IF EXISTS `gpm_groupmenu`;
CREATE TABLE `gpm_groupmenu`  (
  `FK_Group` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限组 - 主键',
  `FK_Menu` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单 - 主键',
  `IsChecked` int(11) NULL DEFAULT NULL COMMENT '是否选中',
  PRIMARY KEY (`FK_Group`, `FK_Menu`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组菜单' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_groupstation
-- ----------------------------
DROP TABLE IF EXISTS `gpm_groupstation`;
CREATE TABLE `gpm_groupstation`  (
  `FK_Group` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限组 - 主键',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Group`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限组岗位' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_grouptype
-- ----------------------------
DROP TABLE IF EXISTS `gpm_grouptype`;
CREATE TABLE `gpm_grouptype`  (
  `No` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组类型' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_menu
-- ----------------------------
DROP TABLE IF EXISTS `gpm_menu`;
CREATE TABLE `gpm_menu`  (
  `No` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '功能编号 - 主键',
  `ParentNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父节点,外键:对应物理表:GPM_Menu,表描述:系统菜单',
  `Name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `MenuType` int(11) NULL DEFAULT NULL COMMENT '菜单类型,枚举类型:0 系统根目录;1 系统类别;2 系统;3 目录;4 功能/界面;5 功能控制点;',
  `FK_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统,外键:对应物理表:GPM_App,表描述:系统',
  `OpenWay` int(11) NULL DEFAULT NULL COMMENT '打开方式,枚举类型:0 新窗口;1 本窗口;2 覆盖新窗口;',
  `Url` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '连接',
  `UrlExt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'PC端连接',
  `MobileUrlExt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '移动端连接',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用?',
  `Icon` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Icon',
  `MenuCtrlWay` int(11) NULL DEFAULT NULL COMMENT '控制方式,枚举类型:0 按照设置的控制;1 任何人都可以使用;2 Admin用户可以使用;',
  `Flag` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标记',
  `Tag1` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag3',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float NULL DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统菜单' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_persetting
-- ----------------------------
DROP TABLE IF EXISTS `gpm_persetting`;
CREATE TABLE `gpm_persetting`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Emp` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员',
  `FK_App` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统',
  `UserNo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'UserNo',
  `UserPass` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'UserPass',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '个人设置' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gpm_stationmenu
-- ----------------------------
DROP TABLE IF EXISTS `gpm_stationmenu`;
CREATE TABLE `gpm_stationmenu`  (
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  `FK_Menu` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单 - 主键',
  `IsChecked` int(11) NULL DEFAULT NULL COMMENT '是否选中',
  PRIMARY KEY (`FK_Station`, `FK_Menu`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位菜单' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_filemanager_folder
-- ----------------------------
DROP TABLE IF EXISTS `js_filemanager_folder`;
CREATE TABLE `js_filemanager_folder`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `parent_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10, 0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4, 0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '全节点名',
  `folder_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件夹名',
  `group_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件分组类型',
  `office_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门编码',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_file_tree_pc`(`parent_code`) USING BTREE,
  INDEX `idx_sys_file_tree_ts`(`tree_sort`) USING BTREE,
  INDEX `idx_sys_file_tree_gt`(`group_type`) USING BTREE,
  INDEX `idx_sys_file_tree_oc`(`office_code`) USING BTREE,
  INDEX `idx_sys_file_tree_cb`(`create_by`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件管理文件夹' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_filemanager_folder
-- ----------------------------
INSERT INTO `js_filemanager_folder` VALUES ('1285466275982692352', '0', '0,', 40, '0000000040,', '0', 0, '公司文件', '公司文件', 'global', NULL, '0', 'system', '2020-07-21 14:46:58', 'system', '2020-07-21 14:46:58', NULL, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1285466276876079104', '1285466275982692352', '0,1285466275982692352,', 30, '0000000040,0000000030,', '1', 1, '公司文件/规章制度', '规章制度', 'global', NULL, '0', 'system', '2020-07-21 14:46:58', 'system', '2020-07-21 14:46:58', NULL, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1285466279535267840', '1285466275982692352', '0,1285466275982692352,', 40, '0000000040,0000000040,', '1', 1, '公司文件/产品资料', '产品资料', 'global', NULL, '0', 'system', '2020-07-21 14:46:59', 'system', '2020-07-21 14:46:59', NULL, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1285466280667729920', '0', '0,', 50, '0000000050,', '0', 0, '员工之家', '员工之家', 'global', NULL, '0', 'system', '2020-07-21 14:46:59', 'system', '2020-07-21 14:46:59', NULL, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1285466281456259072', '1285466280667729920', '0,1285466280667729920,', 30, '0000000050,0000000030,', '1', 1, '员工之家/企业文化', '企业文化', 'global', NULL, '0', 'system', '2020-07-21 14:46:59', 'system', '2020-07-21 14:46:59', NULL, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1285466282634858496', '1285466280667729920', '0,1285466280667729920,', 40, '0000000050,0000000040,', '1', 1, '员工之家/公司活动', '公司活动', 'global', NULL, '0', 'system', '2020-07-21 14:46:59', 'system', '2020-07-21 14:46:59', NULL, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1285466283763126272', '0', '0,', 30, '0000000030,', '1', 0, '我的文档', '我的文档', 'self', NULL, '0', 'system', '2020-07-21 14:47:00', 'system', '2020-07-21 14:47:00', NULL, '0', 'JeeSite');
INSERT INTO `js_filemanager_folder` VALUES ('1285466284539072512', '0', '0,', 40, '0000000040,', '1', 0, '我的图片', '我的图片', 'self', NULL, '0', 'system', '2020-07-21 14:47:00', 'system', '2020-07-21 14:47:00', NULL, '0', 'JeeSite');

-- ----------------------------
-- Table structure for js_filemanager_shared
-- ----------------------------
DROP TABLE IF EXISTS `js_filemanager_shared`;
CREATE TABLE `js_filemanager_shared`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `folder_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件夹编码',
  `file_upload_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件上传编码',
  `file_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件或文件夹名',
  `receive_user_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接收者用户名称',
  `click_num` decimal(10, 0) NULL DEFAULT NULL COMMENT '点击次数',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_file_share_ruc`(`receive_user_code`) USING BTREE,
  INDEX `idx_sys_file_share_cb`(`create_by`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件管理共享表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_gen_table
-- ----------------------------
DROP TABLE IF EXISTS `js_gen_table`;
CREATE TABLE `js_gen_table`  (
  `table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实体类名称',
  `comments` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表说明',
  `parent_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联父表的表名',
  `parent_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '本表关联父表的外键名',
  `data_source_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用的模板',
  `package_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `sub_module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成子模块名',
  `function_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_name_simple` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名（简写）',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_base_dir` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成基础路径',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`table_name`) USING BTREE,
  INDEX `idx_gen_table_ptn`(`parent_table_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_gen_table
-- ----------------------------
INSERT INTO `js_gen_table` VALUES ('test_data', 'TestData', '测试数据', NULL, NULL, NULL, 'crud', 'com.jeesite.modules', 'test', '', '测试数据', '数据', 'ThinkGem', NULL, '{\"isHaveDelete\":\"1\",\"isFileUpload\":\"1\",\"isHaveDisableEnable\":\"1\",\"isImageUpload\":\"1\"}', 'system', '2020-07-21 14:46:53', 'system', '2020-07-21 14:46:53', NULL);
INSERT INTO `js_gen_table` VALUES ('test_data_child', 'TestDataChild', '测试数据子表', 'test_data', 'test_data_id', NULL, 'crud', 'com.jeesite.modules', 'test', '', '测试子表', '数据', 'ThinkGem', NULL, NULL, 'system', '2020-07-21 14:46:54', 'system', '2020-07-21 14:46:54', NULL);
INSERT INTO `js_gen_table` VALUES ('test_tree', 'TestTree', '测试树表', NULL, NULL, NULL, 'treeGrid', 'com.jeesite.modules', 'test', '', '测试树表', '数据', 'ThinkGem', NULL, '{\"treeViewName\":\"tree_name\",\"isHaveDelete\":\"1\",\"treeViewCode\":\"tree_code\",\"isFileUpload\":\"1\",\"isHaveDisableEnable\":\"1\",\"isImageUpload\":\"1\"}', 'system', '2020-07-21 14:46:55', 'system', '2020-07-21 14:46:55', NULL);

-- ----------------------------
-- Table structure for js_gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `js_gen_table_column`;
CREATE TABLE `js_gen_table_column`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表名',
  `column_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '列名',
  `column_sort` decimal(10, 0) NULL DEFAULT NULL COMMENT '列排序（升序）',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类型',
  `column_label` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列标签名',
  `comments` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '列备注说明',
  `attr_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类的属性名',
  `attr_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '类的属性类型',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键',
  `is_null` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否可为空',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否插入字段',
  `is_update` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否更新字段',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询方式',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段',
  `show_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单类型',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_gen_table_column_tn`(`table_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成表列' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_gen_table_column
-- ----------------------------
INSERT INTO `js_gen_table_column` VALUES ('1285466255254441984', 'test_data', 'id', 10, 'varchar(64)', '编号', '编号', 'id', 'String', '1', '0', '1', NULL, NULL, NULL, NULL, '1', 'hidden', '{\"fieldValid\":\"abc\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466255472545792', 'test_data', 'test_input', 20, 'varchar(200)', '单行文本', '单行文本', 'testInput', 'String', NULL, '1', '1', '1', '1', '1', 'LIKE', '1', 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466255707426816', 'test_data', 'test_textarea', 30, 'varchar(200)', '多行文本', '多行文本', 'testTextarea', 'String', NULL, '1', '1', '1', '1', '1', 'LIKE', '1', 'textarea', '{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466255929724928', 'test_data', 'test_select', 40, 'varchar(10)', '下拉框', '下拉框', 'testSelect', 'String', NULL, '1', '1', '1', '1', '1', NULL, '1', 'select', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466256143634432', 'test_data', 'test_select_multiple', 50, 'varchar(200)', '下拉多选', '下拉多选', 'testSelectMultiple', 'String', NULL, '1', '1', '1', '1', '1', NULL, '1', 'select_multiple', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466256353349632', 'test_data', 'test_radio', 60, 'varchar(10)', '单选框', '单选框', 'testRadio', 'String', NULL, '1', '1', '1', '1', '1', NULL, '1', 'radio', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466256563064832', 'test_data', 'test_checkbox', 70, 'varchar(200)', '复选框', '复选框', 'testCheckbox', 'String', NULL, '1', '1', '1', '1', '1', NULL, '1', 'checkbox', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466256776974336', 'test_data', 'test_date', 80, 'datetime', '日期选择', '日期选择', 'testDate', 'java.util.Date', NULL, '1', '1', '1', '1', '1', 'BETWEEN', '1', 'date', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466257041215488', 'test_data', 'test_datetime', 90, 'datetime', '日期时间', '日期时间', 'testDatetime', 'java.util.Date', NULL, '1', '1', '1', '1', '1', 'BETWEEN', '1', 'datetime', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466257263513600', 'test_data', 'test_user_code', 100, 'varchar(64)', '用户选择', '用户选择', 'testUser', 'com.jeesite.modules.sys.entity.User', NULL, '1', '1', '1', '1', '1', NULL, '1', 'userselect', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466257498394624', 'test_data', 'test_office_code', 110, 'varchar(64)', '机构选择', '机构选择', 'testOffice', 'com.jeesite.modules.sys.entity.Office', NULL, '1', '1', '1', '1', '1', NULL, '1', 'officeselect', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466257737469952', 'test_data', 'test_area_code', 120, 'varchar(64)', '区域选择', '区域选择', 'testAreaCode|testAreaName', 'String', NULL, '1', '1', '1', '1', '1', NULL, '1', 'areaselect', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466257955573760', 'test_data', 'test_area_name', 130, 'varchar(100)', '区域名称', '区域名称', 'testAreaName', 'String', NULL, '1', '1', '1', '1', '0', 'LIKE', '0', 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466258161094656', 'test_data', 'status', 140, 'char(1)', '状态', '状态（0正常 1删除 2停用）', 'status', 'String', NULL, '0', '1', '0', '1', '1', NULL, NULL, 'select', '{\"dictName\":\"sys_search_status\",\"dictType\":\"sys_search_status\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466258391781376', 'test_data', 'create_by', 150, 'varchar(64)', '创建者', '创建者', 'createBy', 'String', NULL, '0', '1', NULL, NULL, NULL, NULL, NULL, 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466258605690880', 'test_data', 'create_date', 160, 'datetime', '创建时间', '创建时间', 'createDate', 'java.util.Date', NULL, '0', '1', NULL, NULL, NULL, NULL, NULL, 'dateselect', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466258819600384', 'test_data', 'update_by', 170, 'varchar(64)', '更新者', '更新者', 'updateBy', 'String', NULL, '0', '1', '1', NULL, NULL, NULL, NULL, 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466259033509888', 'test_data', 'update_date', 180, 'datetime', '更新时间', '更新时间', 'updateDate', 'java.util.Date', NULL, '0', '1', '1', '1', NULL, NULL, NULL, 'dateselect', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466259239030784', 'test_data', 'remarks', 190, 'varchar(500)', '备注信息', '备注信息', 'remarks', 'String', NULL, '1', '1', '1', '1', '1', 'LIKE', '1', 'textarea', '{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466261319405568', 'test_data_child', 'id', 10, 'varchar(64)', '编号', '编号', 'id', 'String', '1', '0', '1', NULL, NULL, NULL, NULL, '1', 'hidden', '{\"fieldValid\":\"abc\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466261529120768', 'test_data_child', 'test_sort', 20, 'int(11)', '排序号', '排序号', 'testSort', 'Long', NULL, '1', '1', '1', '1', '1', NULL, '1', 'input', '{\"fieldValid\":\"digits\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466261743030272', 'test_data_child', 'test_data_id', 30, 'varchar(64)', '父表主键', '父表主键', 'testData', 'String', NULL, '1', '1', '1', '1', '1', NULL, '1', 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466261956939776', 'test_data_child', 'test_input', 40, 'varchar(200)', '单行文本', '单行文本', 'testInput', 'String', NULL, '1', '1', '1', '1', '1', 'LIKE', '1', 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466262187626496', 'test_data_child', 'test_textarea', 50, 'varchar(200)', '多行文本', '多行文本', 'testTextarea', 'String', NULL, '1', '1', '1', '1', '1', 'LIKE', '1', 'textarea', '{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466262393147392', 'test_data_child', 'test_select', 60, 'varchar(10)', '下拉框', '下拉框', 'testSelect', 'String', NULL, '1', '1', '1', '1', '1', NULL, '1', 'select', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466262602862592', 'test_data_child', 'test_select_multiple', 70, 'varchar(200)', '下拉多选', '下拉多选', 'testSelectMultiple', 'String', NULL, '1', '1', '1', '1', '1', NULL, '1', 'select_multiple', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466262816772096', 'test_data_child', 'test_radio', 80, 'varchar(10)', '单选框', '单选框', 'testRadio', 'String', NULL, '1', '1', '1', '1', '1', NULL, '1', 'radio', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466263030681600', 'test_data_child', 'test_checkbox', 90, 'varchar(200)', '复选框', '复选框', 'testCheckbox', 'String', NULL, '1', '1', '1', '1', '1', NULL, '1', 'checkbox', '{\"dictName\":\"sys_menu_type\",\"dictType\":\"sys_menu_type\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466263257174016', 'test_data_child', 'test_date', 100, 'datetime', '日期选择', '日期选择', 'testDate', 'java.util.Date', NULL, '1', '1', '1', '1', '1', 'BETWEEN', '1', 'date', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466263508832256', 'test_data_child', 'test_datetime', 110, 'datetime', '日期时间', '日期时间', 'testDatetime', 'java.util.Date', NULL, '1', '1', '1', '1', '1', 'BETWEEN', '1', 'datetime', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466263726936064', 'test_data_child', 'test_user_code', 120, 'varchar(64)', '用户选择', '用户选择', 'testUser', 'com.jeesite.modules.sys.entity.User', NULL, '1', '1', '1', '1', '1', NULL, '1', 'userselect', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466263936651264', 'test_data_child', 'test_office_code', 130, 'varchar(64)', '机构选择', '机构选择', 'testOffice', 'com.jeesite.modules.sys.entity.Office', NULL, '1', '1', '1', '1', '1', NULL, '1', 'officeselect', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466264158949376', 'test_data_child', 'test_area_code', 140, 'varchar(64)', '区域选择', '区域选择', 'testAreaCode|testAreaName', 'String', NULL, '1', '1', '1', '1', '1', NULL, '1', 'areaselect', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466264389636096', 'test_data_child', 'test_area_name', 150, 'varchar(100)', '区域名称', '区域名称', 'testAreaName', 'String', NULL, '1', '1', '1', '1', '0', 'LIKE', '0', 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466266537119744', 'test_tree', 'tree_code', 10, 'varchar(64)', '节点编码', '节点编码', 'treeCode', 'String', '1', '0', '1', NULL, NULL, NULL, NULL, '1', 'input', '{\"fieldValid\":\"abc\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466266751029248', 'test_tree', 'parent_code', 20, 'varchar(64)', '父级编号', '父级编号', 'parentCode|parentName', 'This', NULL, '0', '1', '1', NULL, NULL, NULL, NULL, 'treeselect', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466266964938752', 'test_tree', 'parent_codes', 30, 'varchar(1000)', '所有父级编号', '所有父级编号', 'parentCodes', 'String', NULL, '0', '1', '1', NULL, NULL, 'LIKE', '0', 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466267170459648', 'test_tree', 'tree_sort', 40, 'decimal(10,0)', '本级排序号', '本级排序号（升序）', 'treeSort', 'Integer', NULL, '0', '1', '1', '1', NULL, NULL, '1', 'input', '{\"fieldValid\":\"digits\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466267384369152', 'test_tree', 'tree_sorts', 50, 'varchar(1000)', '所有级别排序号', '所有级别排序号', 'treeSorts', 'String', NULL, '0', '1', '1', '0', NULL, NULL, '0', 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466267594084352', 'test_tree', 'tree_leaf', 60, 'char(1)', '是否最末级', '是否最末级', 'treeLeaf', 'String', NULL, '0', '1', '1', NULL, NULL, NULL, NULL, 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466267807993856', 'test_tree', 'tree_level', 70, 'decimal(4,0)', '层次级别', '层次级别', 'treeLevel', 'Integer', NULL, '0', '1', '1', NULL, NULL, NULL, NULL, 'input', '{\"fieldValid\":\"digits\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466268021903360', 'test_tree', 'tree_names', 80, 'varchar(1000)', '全节点名', '全节点名', 'treeNames', 'String', NULL, '0', '1', '1', NULL, '1', 'LIKE', NULL, 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466268235812864', 'test_tree', 'tree_name', 90, 'varchar(200)', '节点名称', '节点名称', 'treeName', 'String', NULL, '0', '1', '1', '1', '1', 'LIKE', '1', 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466268449722368', 'test_tree', 'status', 100, 'char(1)', '状态', '状态（0正常 1删除 2停用）', 'status', 'String', NULL, '0', '1', '0', '1', '1', NULL, NULL, 'select', '{\"dictName\":\"sys_search_status\",\"dictType\":\"sys_search_status\"}');
INSERT INTO `js_gen_table_column` VALUES ('1285466268659437568', 'test_tree', 'create_by', 110, 'varchar(64)', '创建者', '创建者', 'createBy', 'String', NULL, '0', '1', NULL, NULL, NULL, NULL, NULL, 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466268869152768', 'test_tree', 'create_date', 120, 'datetime', '创建时间', '创建时间', 'createDate', 'java.util.Date', NULL, '0', '1', NULL, NULL, NULL, NULL, NULL, 'dateselect', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466269083062272', 'test_tree', 'update_by', 130, 'varchar(64)', '更新者', '更新者', 'updateBy', 'String', NULL, '0', '1', '1', NULL, NULL, NULL, NULL, 'input', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466269288583168', 'test_tree', 'update_date', 140, 'datetime', '更新时间', '更新时间', 'updateDate', 'java.util.Date', NULL, '0', '1', '1', '1', NULL, NULL, NULL, 'dateselect', NULL);
INSERT INTO `js_gen_table_column` VALUES ('1285466269494104064', 'test_tree', 'remarks', 150, 'varchar(500)', '备注信息', '备注信息', 'remarks', 'String', NULL, '1', '1', '1', '1', '1', 'LIKE', '1', 'textarea', '{\"isNewLine\":\"1\",\"gridRowCol\":\"12/2/10\"}');

-- ----------------------------
-- Table structure for js_job_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `js_job_blob_triggers`;
CREATE TABLE `js_job_blob_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `js_job_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_job_calendars
-- ----------------------------
DROP TABLE IF EXISTS `js_job_calendars`;
CREATE TABLE `js_job_calendars`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_job_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `js_job_cron_triggers`;
CREATE TABLE `js_job_cron_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CRON_EXPRESSION` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `js_job_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_job_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `js_job_fired_triggers`;
CREATE TABLE `js_job_fired_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TRIG_INST_NAME`(`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY`(`SCHED_NAME`, `INSTANCE_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_FT_J_G`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_T_G`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TG`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_job_job_details
-- ----------------------------
DROP TABLE IF EXISTS `js_job_job_details`;
CREATE TABLE `js_job_job_details`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_J_REQ_RECOVERY`(`SCHED_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_J_GRP`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_job_locks
-- ----------------------------
DROP TABLE IF EXISTS `js_job_locks`;
CREATE TABLE `js_job_locks`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_job_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `js_job_paused_trigger_grps`;
CREATE TABLE `js_job_paused_trigger_grps`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_job_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `js_job_scheduler_state`;
CREATE TABLE `js_job_scheduler_state`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_job_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `js_job_simple_triggers`;
CREATE TABLE `js_job_simple_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `js_job_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_job_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `js_job_simprop_triggers`;
CREATE TABLE `js_job_simprop_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INT_PROP_1` int(11) NULL DEFAULT NULL,
  `INT_PROP_2` int(11) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(20) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(20) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `js_job_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `js_job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_job_triggers
-- ----------------------------
DROP TABLE IF EXISTS `js_job_triggers`;
CREATE TABLE `js_job_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PRIORITY` int(11) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_J`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_C`(`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_T_G`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_STATE`(`SCHED_NAME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_STATE`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_G_STATE`(`SCHED_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NEXT_FIRE_TIME`(`SCHED_NAME`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST`(`SCHED_NAME`, `TRIGGER_STATE`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  CONSTRAINT `js_job_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `js_job_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_area
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_area`;
CREATE TABLE `js_sys_area`  (
  `area_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '区域编码',
  `parent_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10, 0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4, 0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '全节点名',
  `area_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '区域名称',
  `area_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`area_code`) USING BTREE,
  INDEX `idx_sys_area_pc`(`parent_code`) USING BTREE,
  INDEX `idx_sys_area_ts`(`tree_sort`) USING BTREE,
  INDEX `idx_sys_area_status`(`status`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '行政区划' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_area
-- ----------------------------
INSERT INTO `js_sys_area` VALUES ('370000', '0', '0,', 370000, '0000370000,', '0', 0, '山东省', '山东省', '1', '0', 'system', '2020-07-21 14:41:29', 'system', '2020-07-21 14:41:29', NULL);
INSERT INTO `js_sys_area` VALUES ('370100', '370000', '0,370000,', 370100, '0000370000,0000370100,', '0', 1, '山东省/济南市', '济南市', '2', '0', 'system', '2020-07-21 14:41:29', 'system', '2020-07-21 14:41:29', NULL);
INSERT INTO `js_sys_area` VALUES ('370102', '370100', '0,370000,370100,', 370102, '0000370000,0000370100,0000370102,', '1', 2, '山东省/济南市/历下区', '历下区', '3', '0', 'system', '2020-07-21 14:41:30', 'system', '2020-07-21 14:41:30', NULL);
INSERT INTO `js_sys_area` VALUES ('370103', '370100', '0,370000,370100,', 370103, '0000370000,0000370100,0000370103,', '1', 2, '山东省/济南市/市中区', '市中区', '3', '0', 'system', '2020-07-21 14:41:30', 'system', '2020-07-21 14:41:30', NULL);
INSERT INTO `js_sys_area` VALUES ('370104', '370100', '0,370000,370100,', 370104, '0000370000,0000370100,0000370104,', '1', 2, '山东省/济南市/槐荫区', '槐荫区', '3', '0', 'system', '2020-07-21 14:41:31', 'system', '2020-07-21 14:41:31', NULL);
INSERT INTO `js_sys_area` VALUES ('370105', '370100', '0,370000,370100,', 370105, '0000370000,0000370100,0000370105,', '1', 2, '山东省/济南市/天桥区', '天桥区', '3', '0', 'system', '2020-07-21 14:41:31', 'system', '2020-07-21 14:41:31', NULL);
INSERT INTO `js_sys_area` VALUES ('370112', '370100', '0,370000,370100,', 370112, '0000370000,0000370100,0000370112,', '1', 2, '山东省/济南市/历城区', '历城区', '3', '0', 'system', '2020-07-21 14:41:32', 'system', '2020-07-21 14:41:32', NULL);
INSERT INTO `js_sys_area` VALUES ('370113', '370100', '0,370000,370100,', 370113, '0000370000,0000370100,0000370113,', '1', 2, '山东省/济南市/长清区', '长清区', '3', '0', 'system', '2020-07-21 14:41:32', 'system', '2020-07-21 14:41:32', NULL);
INSERT INTO `js_sys_area` VALUES ('370114', '370100', '0,370000,370100,', 370114, '0000370000,0000370100,0000370114,', '1', 2, '山东省/济南市/章丘区', '章丘区', '3', '0', 'system', '2020-07-21 14:41:33', 'system', '2020-07-21 14:41:33', NULL);
INSERT INTO `js_sys_area` VALUES ('370115', '370100', '0,370000,370100,', 370115, '0000370000,0000370100,0000370115,', '1', 2, '山东省/济南市/济阳区', '济阳区', '3', '0', 'system', '2020-07-21 14:41:34', 'system', '2020-07-21 14:41:34', NULL);
INSERT INTO `js_sys_area` VALUES ('370116', '370100', '0,370000,370100,', 370116, '0000370000,0000370100,0000370116,', '1', 2, '山东省/济南市/莱芜区', '莱芜区', '3', '0', 'system', '2020-07-21 14:41:35', 'system', '2020-07-21 14:41:35', NULL);
INSERT INTO `js_sys_area` VALUES ('370117', '370100', '0,370000,370100,', 370117, '0000370000,0000370100,0000370117,', '1', 2, '山东省/济南市/钢城区', '钢城区', '3', '0', 'system', '2020-07-21 14:41:35', 'system', '2020-07-21 14:41:35', NULL);
INSERT INTO `js_sys_area` VALUES ('370124', '370100', '0,370000,370100,', 370124, '0000370000,0000370100,0000370124,', '1', 2, '山东省/济南市/平阴县', '平阴县', '3', '0', 'system', '2020-07-21 14:41:36', 'system', '2020-07-21 14:41:36', NULL);
INSERT INTO `js_sys_area` VALUES ('370126', '370100', '0,370000,370100,', 370126, '0000370000,0000370100,0000370126,', '1', 2, '山东省/济南市/商河县', '商河县', '3', '0', 'system', '2020-07-21 14:41:37', 'system', '2020-07-21 14:41:37', NULL);
INSERT INTO `js_sys_area` VALUES ('370200', '370000', '0,370000,', 370200, '0000370000,0000370200,', '0', 1, '山东省/青岛市', '青岛市', '2', '0', 'system', '2020-07-21 14:41:38', 'system', '2020-07-21 14:41:38', NULL);
INSERT INTO `js_sys_area` VALUES ('370202', '370200', '0,370000,370200,', 370202, '0000370000,0000370200,0000370202,', '1', 2, '山东省/青岛市/市南区', '市南区', '3', '0', 'system', '2020-07-21 14:41:38', 'system', '2020-07-21 14:41:38', NULL);
INSERT INTO `js_sys_area` VALUES ('370203', '370200', '0,370000,370200,', 370203, '0000370000,0000370200,0000370203,', '1', 2, '山东省/青岛市/市北区', '市北区', '3', '0', 'system', '2020-07-21 14:41:39', 'system', '2020-07-21 14:41:39', NULL);
INSERT INTO `js_sys_area` VALUES ('370211', '370200', '0,370000,370200,', 370211, '0000370000,0000370200,0000370211,', '1', 2, '山东省/青岛市/黄岛区', '黄岛区', '3', '0', 'system', '2020-07-21 14:41:39', 'system', '2020-07-21 14:41:39', NULL);
INSERT INTO `js_sys_area` VALUES ('370212', '370200', '0,370000,370200,', 370212, '0000370000,0000370200,0000370212,', '1', 2, '山东省/青岛市/崂山区', '崂山区', '3', '0', 'system', '2020-07-21 14:41:39', 'system', '2020-07-21 14:41:39', NULL);
INSERT INTO `js_sys_area` VALUES ('370213', '370200', '0,370000,370200,', 370213, '0000370000,0000370200,0000370213,', '1', 2, '山东省/青岛市/李沧区', '李沧区', '3', '0', 'system', '2020-07-21 14:41:40', 'system', '2020-07-21 14:41:40', NULL);
INSERT INTO `js_sys_area` VALUES ('370214', '370200', '0,370000,370200,', 370214, '0000370000,0000370200,0000370214,', '1', 2, '山东省/青岛市/城阳区', '城阳区', '3', '0', 'system', '2020-07-21 14:41:41', 'system', '2020-07-21 14:41:41', NULL);
INSERT INTO `js_sys_area` VALUES ('370215', '370200', '0,370000,370200,', 370215, '0000370000,0000370200,0000370215,', '1', 2, '山东省/青岛市/即墨区', '即墨区', '3', '0', 'system', '2020-07-21 14:41:41', 'system', '2020-07-21 14:41:41', NULL);
INSERT INTO `js_sys_area` VALUES ('370281', '370200', '0,370000,370200,', 370281, '0000370000,0000370200,0000370281,', '1', 2, '山东省/青岛市/胶州市', '胶州市', '3', '0', 'system', '2020-07-21 14:41:43', 'system', '2020-07-21 14:41:43', NULL);
INSERT INTO `js_sys_area` VALUES ('370283', '370200', '0,370000,370200,', 370283, '0000370000,0000370200,0000370283,', '1', 2, '山东省/青岛市/平度市', '平度市', '3', '0', 'system', '2020-07-21 14:41:45', 'system', '2020-07-21 14:41:45', NULL);
INSERT INTO `js_sys_area` VALUES ('370285', '370200', '0,370000,370200,', 370285, '0000370000,0000370200,0000370285,', '1', 2, '山东省/青岛市/莱西市', '莱西市', '3', '0', 'system', '2020-07-21 14:41:46', 'system', '2020-07-21 14:41:46', NULL);

-- ----------------------------
-- Table structure for js_sys_company
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_company`;
CREATE TABLE `js_sys_company`  (
  `company_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公司编码',
  `parent_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10, 0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4, 0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '全节点名',
  `view_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公司代码',
  `company_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公司名称',
  `full_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公司全称',
  `area_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域编码',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`company_code`) USING BTREE,
  INDEX `idx_sys_company_cc`(`corp_code`) USING BTREE,
  INDEX `idx_sys_company_pc`(`parent_code`) USING BTREE,
  INDEX `idx_sys_company_ts`(`tree_sort`) USING BTREE,
  INDEX `idx_sys_company_status`(`status`) USING BTREE,
  INDEX `idx_sys_company_vc`(`view_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公司表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_company
-- ----------------------------
INSERT INTO `js_sys_company` VALUES ('SD', '0', '0,', 40, '0000000040,', '0', 0, '山东公司', 'SD', '山东公司', '山东公司', NULL, '0', 'system', '2020-07-21 14:46:21', 'system', '2020-07-21 14:46:21', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_company` VALUES ('SDJN', 'SD', '0,SD,', 30, '0000000040,0000000030,', '1', 1, '山东公司/济南公司', 'SDJN', '济南公司', '山东济南公司', NULL, '0', 'system', '2020-07-21 14:46:22', 'system', '2020-07-21 14:46:22', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_company` VALUES ('SDQD', 'SD', '0,SD,', 40, '0000000040,0000000040,', '1', 1, '山东公司/青岛公司', 'SDQD', '青岛公司', '山东青岛公司', NULL, '0', 'system', '2020-07-21 14:46:22', 'system', '2020-07-21 14:46:22', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for js_sys_company_office
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_company_office`;
CREATE TABLE `js_sys_company_office`  (
  `company_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公司编码',
  `office_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构编码',
  PRIMARY KEY (`company_code`, `office_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '公司部门关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_config
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_config`;
CREATE TABLE `js_sys_config`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '参数键',
  `config_value` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数值',
  `is_sys` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '系统内置（1是 0否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_sys_config_key`(`config_key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_config
-- ----------------------------
INSERT INTO `js_sys_config` VALUES ('1285464972548198400', '研发工具-代码生成默认包名', 'gen.defaultPackageName', 'com.jeesite.modules', '0', 'system', '2020-07-21 14:41:48', 'system', '2020-07-21 14:41:48', '新建项目后，修改该键值，在生成代码的时候就不要再修改了');
INSERT INTO `js_sys_config` VALUES ('1285464976721530880', '主框架页-桌面仪表盘首页地址', 'sys.index.desktopUrl', '/desktop', '0', 'system', '2020-07-21 14:41:48', 'system', '2020-07-21 14:41:48', '主页面的第一个页签首页桌面地址');
INSERT INTO `js_sys_config` VALUES ('1285464977795272704', '主框架页-主导航菜单显示风格', 'sys.index.menuStyle', '1', '0', 'system', '2020-07-21 14:41:48', 'system', '2020-07-21 14:41:48', '1：菜单全部在左侧；2：根菜单显示在顶部');
INSERT INTO `js_sys_config` VALUES ('1285464978709630976', '主框架页-侧边栏的默认显示样式', 'sys.index.sidebarStyle', '1', '0', 'system', '2020-07-21 14:41:49', 'system', '2020-07-21 14:41:49', '1：默认显示侧边栏；2：默认折叠侧边栏');
INSERT INTO `js_sys_config` VALUES ('1285464981033275392', '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue-light2', '0', 'system', '2020-07-21 14:41:49', 'system', '2020-07-21 14:41:49', '蓝：skin-blue、黑：skin-black2、白：skin-black、紫：skin-purple、绿：skin-green、红：skin-red、黄：skin-yellow、蓝灰：skin-blue-light、黑灰：skin-black-light2、白灰：skin-black-light、紫灰：skin-purple-light、绿灰：skin-green-light、红灰：skin-red-light、黄灰skin-yellow-light、浅蓝：skin-blue-light2');
INSERT INTO `js_sys_config` VALUES ('1285464981872136192', '用户登录-登录失败多少次数后显示验证码', 'sys.login.failedNumAfterValidCode', '100', '0', 'system', '2020-07-21 14:41:50', 'system', '2020-07-21 14:41:50', '设置为0强制使用验证码登录');
INSERT INTO `js_sys_config` VALUES ('1285464984434855936', '用户登录-登录失败多少次数后锁定账号', 'sys.login.failedNumAfterLockAccount', '200', '0', 'system', '2020-07-21 14:41:50', 'system', '2020-07-21 14:41:50', '登录失败多少次数后锁定账号');
INSERT INTO `js_sys_config` VALUES ('1285464985462460416', '用户登录-登录失败多少次数后锁定账号的时间', 'sys.login.failedNumAfterLockMinute', '20', '0', 'system', '2020-07-21 14:41:51', 'system', '2020-07-21 14:41:51', '锁定账号的时间（单位：分钟）');
INSERT INTO `js_sys_config` VALUES ('1285464989405106176', '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', '0', 'system', '2020-07-21 14:41:53', 'system', '2020-07-21 14:41:53', '是否开启注册用户功能');
INSERT INTO `js_sys_config` VALUES ('1285464999504990208', '账号自助-允许自助注册的用户类型', 'sys.account.registerUser.userTypes', 'member', '0', 'system', '2020-07-21 14:41:53', 'system', '2020-07-21 14:41:53', '允许注册的用户类型（多个用逗号隔开，例如：employee,member）');
INSERT INTO `js_sys_config` VALUES ('1285465000515817472', '账号自助-验证码有效时间（分钟）', 'sys.account.validCodeTimeout', '10', '0', 'system', '2020-07-21 14:41:54', 'system', '2020-07-21 14:41:54', '找回密码时，短信/邮件验证码有效时间（单位：分钟，0表示不限制）');
INSERT INTO `js_sys_config` VALUES ('1285465001505673216', '用户管理-账号默认角色-员工类型', 'sys.user.defaultRoleCodes.employee', 'default', '0', 'system', '2020-07-21 14:41:54', 'system', '2020-07-21 14:41:54', '所有员工用户都拥有的角色权限（适用于菜单授权查询）');
INSERT INTO `js_sys_config` VALUES ('1285465002378088448', '用户管理-账号初始密码', 'sys.user.initPassword', '123456', '0', 'system', '2020-07-21 14:41:54', 'system', '2020-07-21 14:41:54', '创建用户和重置密码的时候用户的密码');
INSERT INTO `js_sys_config` VALUES ('1285465003258892288', '用户管理-初始密码修改策略', 'sys.user.initPasswordModify', '1', '0', 'system', '2020-07-21 14:41:54', 'system', '2020-07-21 14:41:54', '0：初始密码修改策略关闭，没有任何提示；1：提醒用户，如果未修改初始密码，则在登录时和点击菜单就会提醒修改密码对话框；2：强制实行初始密码修改，登录后若不修改密码则不能进行系统操作');
INSERT INTO `js_sys_config` VALUES ('1285465004118724608', '用户管理-账号密码修改策略', 'sys.user.passwordModify', '1', '0', 'system', '2020-07-21 14:41:55', 'system', '2020-07-21 14:41:55', '0：密码修改策略关闭，没有任何提示；1：提醒用户，如果未修改初始密码，则在登录时和点击菜单就会提醒修改密码对话框；2：强制实行初始密码修改，登录后若不修改密码则不能进行系统操作。');
INSERT INTO `js_sys_config` VALUES ('1285465006756941824', '用户管理-账号密码修改策略验证周期', 'sys.user.passwordModifyCycle', '30', '0', 'system', '2020-07-21 14:41:55', 'system', '2020-07-21 14:41:55', '密码安全修改周期是指定时间内提醒或必须修改密码（例如设置30天）的验证时间（天），超过这个时间登录系统时需，提醒用户修改密码或强制修改密码才能继续使用系统。单位：天，如果设置30天，则第31天开始强制修改密码');
INSERT INTO `js_sys_config` VALUES ('1285465007633551360', '用户管理-密码修改多少次内不允许重复', 'sys.user.passwordModifyNotRepeatNum', '1', '0', 'system', '2020-07-21 14:41:56', 'system', '2020-07-21 14:41:56', '默认1次，表示不能与上次密码重复；若设置为3，表示并与前3次密码重复');
INSERT INTO `js_sys_config` VALUES ('1285465009885892608', '用户管理-账号密码修改最低安全等级', 'sys.user.passwordModifySecurityLevel', '0', '0', 'system', '2020-07-21 14:41:57', 'system', '2020-07-21 14:41:57', '0：不限制等级（用户在修改密码的时候不进行等级验证）\r；1：限制最低等级为很弱\r；2：限制最低等级为弱\r；3：限制最低等级为安全\r；4：限制最低等级为很安全');

-- ----------------------------
-- Table structure for js_sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_dict_data`;
CREATE TABLE `js_sys_dict_data`  (
  `dict_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典编码',
  `parent_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10, 0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4, 0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '全节点名',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典类型',
  `is_sys` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '系统内置（1是 0否）',
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典描述',
  `css_style` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'css样式（如：color:red)',
  `css_class` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'css类名（如：red）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`dict_code`) USING BTREE,
  INDEX `idx_sys_dict_data_cc`(`corp_code`) USING BTREE,
  INDEX `idx_sys_dict_data_dt`(`dict_type`) USING BTREE,
  INDEX `idx_sys_dict_data_pc`(`parent_code`) USING BTREE,
  INDEX `idx_sys_dict_data_status`(`status`) USING BTREE,
  INDEX `idx_sys_dict_data_ts`(`tree_sort`) USING BTREE,
  INDEX `idx_sys_dict_data_dv`(`dict_value`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_dict_data
-- ----------------------------
INSERT INTO `js_sys_dict_data` VALUES ('1285465092001976320', '0', '0,', 30, '0000000030,', '1', 0, '是', '是', '1', 'sys_yes_no', '1', '', '', '', '0', 'system', '2020-07-21 14:42:16', 'system', '2020-07-21 14:42:16', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465096292749312', '0', '0,', 40, '0000000040,', '1', 0, '否', '否', '0', 'sys_yes_no', '1', '', 'color:#aaa;', '', '0', 'system', '2020-07-21 14:42:18', 'system', '2020-07-21 14:42:18', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465104211595264', '0', '0,', 20, '0000000020,', '1', 0, '正常', '正常', '0', 'sys_status', '1', '', '', '', '0', 'system', '2020-07-21 14:42:18', 'system', '2020-07-21 14:42:18', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465105239199744', '0', '0,', 30, '0000000030,', '1', 0, '删除', '删除', '1', 'sys_status', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:42:20', 'system', '2020-07-21 14:42:20', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465111761342464', '0', '0,', 40, '0000000040,', '1', 0, '停用', '停用', '2', 'sys_status', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:42:20', 'system', '2020-07-21 14:42:20', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465112637952000', '0', '0,', 50, '0000000050,', '1', 0, '冻结', '冻结', '3', 'sys_status', '1', '', 'color:#fa0;', '', '0', 'system', '2020-07-21 14:42:20', 'system', '2020-07-21 14:42:20', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465113556504576', '0', '0,', 60, '0000000060,', '1', 0, '待审', '待审', '4', 'sys_status', '1', '', '', '', '0', 'system', '2020-07-21 14:42:21', 'system', '2020-07-21 14:42:21', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465116307968000', '0', '0,', 70, '0000000070,', '1', 0, '驳回', '驳回', '5', 'sys_status', '1', '', '', '', '0', 'system', '2020-07-21 14:42:21', 'system', '2020-07-21 14:42:21', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465118392537088', '0', '0,', 80, '0000000080,', '1', 0, '草稿', '草稿', '9', 'sys_status', '1', '', 'color:#aaa;', '', '0', 'system', '2020-07-21 14:42:22', 'system', '2020-07-21 14:42:22', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465120766513152', '0', '0,', 30, '0000000030,', '1', 0, '显示', '显示', '1', 'sys_show_hide', '1', '', '', '', '0', 'system', '2020-07-21 14:42:23', 'system', '2020-07-21 14:42:23', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465123903852544', '0', '0,', 40, '0000000040,', '1', 0, '隐藏', '隐藏', '0', 'sys_show_hide', '1', '', 'color:#aaa;', '', '0', 'system', '2020-07-21 14:42:25', 'system', '2020-07-21 14:42:25', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465133273927680', '0', '0,', 30, '0000000030,', '1', 0, '简体中文', '简体中文', 'zh_CN', 'sys_lang_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:26', 'system', '2020-07-21 14:42:26', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465138344841216', '0', '0,', 40, '0000000040,', '1', 0, 'English', 'English', 'en', 'sys_lang_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:27', 'system', '2020-07-21 14:42:27', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465139426971648', '0', '0,', 60, '0000000060,', '1', 0, '日本語', '日本語', 'ja_JP', 'sys_lang_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:27', 'system', '2020-07-21 14:42:27', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465142325235712', '0', '0,', 30, '0000000030,', '1', 0, 'PC电脑', 'PC电脑', 'pc', 'sys_device_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:28', 'system', '2020-07-21 14:42:28', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465145223499776', '0', '0,', 40, '0000000040,', '1', 0, '手机APP', '手机APP', 'mobileApp', 'sys_device_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:28', 'system', '2020-07-21 14:42:28', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465146171412480', '0', '0,', 50, '0000000050,', '1', 0, '手机Web', '手机Web', 'mobileWeb', 'sys_device_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:28', 'system', '2020-07-21 14:42:28', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465147115130880', '0', '0,', 60, '0000000060,', '1', 0, '微信设备', '微信设备', 'weixin', 'sys_device_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:29', 'system', '2020-07-21 14:42:29', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465149384249344', '0', '0,', 30, '0000000030,', '1', 0, '主导航菜单', '主导航菜单', 'default', 'sys_menu_sys_code', '1', '', '', '', '0', 'system', '2020-07-21 14:42:29', 'system', '2020-07-21 14:42:29', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465150269247488', '0', '0,', 30, '0000000030,', '1', 0, '菜单', '菜单', '1', 'sys_menu_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:30', 'system', '2020-07-21 14:42:30', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465154027343872', '0', '0,', 40, '0000000040,', '1', 0, '权限', '权限', '2', 'sys_menu_type', '1', '', 'color:#c243d6;', '', '0', 'system', '2020-07-21 14:42:30', 'system', '2020-07-21 14:42:30', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465154941702144', '0', '0,', 30, '0000000030,', '1', 0, '默认权重', '默认权重', '20', 'sys_menu_weight', '1', '', '', '', '0', 'system', '2020-07-21 14:42:30', 'system', '2020-07-21 14:42:30', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465155881226240', '0', '0,', 40, '0000000040,', '1', 0, '二级管理员', '二级管理员', '40', 'sys_menu_weight', '1', '', '', '', '0', 'system', '2020-07-21 14:42:31', 'system', '2020-07-21 14:42:31', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465156858499072', '0', '0,', 50, '0000000050,', '1', 0, '系统管理员', '系统管理员', '60', 'sys_menu_weight', '1', '', '', '', '0', 'system', '2020-07-21 14:42:31', 'system', '2020-07-21 14:42:31', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465157789634560', '0', '0,', 60, '0000000060,', '1', 0, '超级管理员', '超级管理员', '80', 'sys_menu_weight', '1', '', 'color:#c243d6;', '', '0', 'system', '2020-07-21 14:42:32', 'system', '2020-07-21 14:42:32', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465162889908224', '0', '0,', 30, '0000000030,', '1', 0, '国家', '国家', '0', 'sys_area_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:33', 'system', '2020-07-21 14:42:33', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465166778028032', '0', '0,', 40, '0000000040,', '1', 0, '省份直辖市', '省份直辖市', '1', 'sys_area_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:33', 'system', '2020-07-21 14:42:33', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465167793049600', '0', '0,', 50, '0000000050,', '1', 0, '地市', '地市', '2', 'sys_area_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:34', 'system', '2020-07-21 14:42:34', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465170125082624', '0', '0,', 60, '0000000060,', '1', 0, '区县', '区县', '3', 'sys_area_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:36', 'system', '2020-07-21 14:42:36', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465180292075520', '0', '0,', 30, '0000000030,', '1', 0, '省级公司', '省级公司', '1', 'sys_office_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:37', 'system', '2020-07-21 14:42:37', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465181202239488', '0', '0,', 40, '0000000040,', '1', 0, '市级公司', '市级公司', '2', 'sys_office_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:38', 'system', '2020-07-21 14:42:38', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465187548221440', '0', '0,', 50, '0000000050,', '1', 0, '部门', '部门', '3', 'sys_office_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:40', 'system', '2020-07-21 14:42:40', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465193822900224', '0', '0,', 30, '0000000030,', '1', 0, '正常', '正常', '0', 'sys_search_status', '1', '', '', '', '0', 'system', '2020-07-21 14:42:40', 'system', '2020-07-21 14:42:40', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465194758230016', '0', '0,', 40, '0000000040,', '1', 0, '停用', '停用', '2', 'sys_search_status', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:42:40', 'system', '2020-07-21 14:42:40', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465197044125696', '0', '0,', 30, '0000000030,', '1', 0, '男', '男', '1', 'sys_user_sex', '1', '', '', '', '0', 'system', '2020-07-21 14:42:41', 'system', '2020-07-21 14:42:41', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465197937512448', '0', '0,', 40, '0000000040,', '1', 0, '女', '女', '2', 'sys_user_sex', '1', '', '', '', '0', 'system', '2020-07-21 14:42:41', 'system', '2020-07-21 14:42:41', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465200340848640', '0', '0,', 30, '0000000030,', '1', 0, '正常', '正常', '0', 'sys_user_status', '1', '', '', '', '0', 'system', '2020-07-21 14:42:41', 'system', '2020-07-21 14:42:41', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465201343287296', '0', '0,', 40, '0000000040,', '1', 0, '停用', '停用', '2', 'sys_user_status', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:42:42', 'system', '2020-07-21 14:42:42', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465204191219712', '0', '0,', 50, '0000000050,', '1', 0, '冻结', '冻结', '3', 'sys_user_status', '1', '', 'color:#fa0;', '', '0', 'system', '2020-07-21 14:42:42', 'system', '2020-07-21 14:42:42', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465205315293184', '0', '0,', 30, '0000000030,', '1', 0, '员工', '员工', 'employee', 'sys_user_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:43', 'system', '2020-07-21 14:42:43', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465207659909120', '0', '0,', 40, '0000000040,', '1', 0, '会员', '会员', 'member', 'sys_user_type', '1', '', '', '', '2', 'system', '2020-07-21 14:42:43', 'system', '2020-07-21 14:42:43', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465208616210432', '0', '0,', 50, '0000000050,', '1', 0, '单位', '单位', 'btype', 'sys_user_type', '1', '', '', '', '2', 'system', '2020-07-21 14:42:44', 'system', '2020-07-21 14:42:44', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465213552906240', '0', '0,', 60, '0000000060,', '1', 0, '个人', '个人', 'persion', 'sys_user_type', '1', '', '', '', '2', 'system', '2020-07-21 14:42:45', 'system', '2020-07-21 14:42:45', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465217529106432', '0', '0,', 70, '0000000070,', '1', 0, '专家', '专家', 'expert', 'sys_user_type', '1', '', '', '', '2', 'system', '2020-07-21 14:42:46', 'system', '2020-07-21 14:42:46', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465220087631872', '0', '0,', 30, '0000000030,', '1', 0, '高管', '高管', '1', 'sys_role_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:47', 'system', '2020-07-21 14:42:47', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465225036910592', '0', '0,', 40, '0000000040,', '1', 0, '中层', '中层', '2', 'sys_role_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:48', 'system', '2020-07-21 14:42:48', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465229042470912', '0', '0,', 50, '0000000050,', '1', 0, '基层', '基层', '3', 'sys_role_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:49', 'system', '2020-07-21 14:42:49', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465233291300864', '0', '0,', 60, '0000000060,', '1', 0, '其它', '其它', '4', 'sys_role_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:49', 'system', '2020-07-21 14:42:49', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465234243407872', '0', '0,', 30, '0000000030,', '1', 0, '未设置', '未设置', '0', 'sys_role_data_scope', '1', '', '', '', '0', 'system', '2020-07-21 14:42:50', 'system', '2020-07-21 14:42:50', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465236881625088', '0', '0,', 40, '0000000040,', '1', 0, '全部数据', '全部数据', '1', 'sys_role_data_scope', '1', '', '', '', '0', 'system', '2020-07-21 14:42:50', 'system', '2020-07-21 14:42:50', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465239494676480', '0', '0,', 50, '0000000050,', '1', 0, '自定义数据', '自定义数据', '2', 'sys_role_data_scope', '1', '', '', '', '0', 'system', '2020-07-21 14:42:51', 'system', '2020-07-21 14:42:51', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465242124505088', '0', '0,', 60, '0000000060,', '1', 0, '本部门数据', '本部门数据', '3', 'sys_role_data_scope', '1', '', '', '', '0', 'system', '2020-07-21 14:42:52', 'system', '2020-07-21 14:42:52', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465245840658432', '0', '0,', 70, '0000000070,', '1', 0, '本公司数据', '本公司数据', '4', 'sys_role_data_scope', '1', '', '', '', '0', 'system', '2020-07-21 14:42:52', 'system', '2020-07-21 14:42:52', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465246834708480', '0', '0,', 80, '0000000080,', '1', 0, '本部门和本公司数据', '本部门和本公司数据', '5', 'sys_role_data_scope', '1', '', '', '', '0', 'system', '2020-07-21 14:42:53', 'system', '2020-07-21 14:42:53', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465249233850368', '0', '0,', 30, '0000000030,', '1', 0, '组织管理', '组织管理', 'office_user', 'sys_role_biz_scope', '1', '', '', '', '0', 'system', '2020-07-21 14:42:53', 'system', '2020-07-21 14:42:53', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465251662352384', '0', '0,', 30, '0000000030,', '1', 0, '高管', '高管', '1', 'sys_post_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:54', 'system', '2020-07-21 14:42:54', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465252568322048', '0', '0,', 40, '0000000040,', '1', 0, '中层', '中层', '2', 'sys_post_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:55', 'system', '2020-07-21 14:42:55', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465256653574144', '0', '0,', 50, '0000000050,', '1', 0, '基层', '基层', '3', 'sys_post_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:55', 'system', '2020-07-21 14:42:55', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465257630846976', '0', '0,', 60, '0000000060,', '1', 0, '其它', '其它', '4', 'sys_post_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:55', 'system', '2020-07-21 14:42:55', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465258578759680', '0', '0,', 30, '0000000030,', '1', 0, '接入日志', '接入日志', 'access', 'sys_log_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:56', 'system', '2020-07-21 14:42:56', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465264748580864', '0', '0,', 40, '0000000040,', '1', 0, '修改日志', '修改日志', 'update', 'sys_log_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:57', 'system', '2020-07-21 14:42:57', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465267000922112', '0', '0,', 50, '0000000050,', '1', 0, '查询日志', '查询日志', 'select', 'sys_log_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:58', 'system', '2020-07-21 14:42:58', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465270071152640', '0', '0,', 60, '0000000060,', '1', 0, '登录登出', '登录登出', 'loginLogout', 'sys_log_type', '1', '', '', '', '0', 'system', '2020-07-21 14:42:59', 'system', '2020-07-21 14:42:59', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465275221757952', '0', '0,', 30, '0000000030,', '1', 0, '默认', '默认', 'DEFAULT', 'sys_job_group', '1', '', '', '', '0', 'system', '2020-07-21 14:42:59', 'system', '2020-07-21 14:42:59', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465276287111168', '0', '0,', 40, '0000000040,', '1', 0, '系统', '系统', 'SYSTEM', 'sys_job_group', '1', '', '', '', '0', 'system', '2020-07-21 14:42:59', 'system', '2020-07-21 14:42:59', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465277214052352', '0', '0,', 30, '0000000030,', '1', 0, '错过计划等待本次计划完成后立即执行一次', '错过计划等待本次计划完成后立即执行一次', '1', 'sys_job_misfire_instruction', '1', '', '', '', '0', 'system', '2020-07-21 14:43:00', 'system', '2020-07-21 14:43:00', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465278195519488', '0', '0,', 40, '0000000040,', '1', 0, '本次执行时间根据上次结束时间重新计算（时间间隔方式）', '本次执行时间根据上次结束时间重新计算（时间间隔方式）', '2', 'sys_job_misfire_instruction', '1', '', '', '', '0', 'system', '2020-07-21 14:43:00', 'system', '2020-07-21 14:43:00', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465279139237888', '0', '0,', 30, '0000000030,', '1', 0, '正常', '正常', '0', 'sys_job_status', '1', '', '', '', '0', 'system', '2020-07-21 14:43:00', 'system', '2020-07-21 14:43:00', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465280061984768', '0', '0,', 40, '0000000040,', '1', 0, '删除', '删除', '1', 'sys_job_status', '1', '', '', '', '0', 'system', '2020-07-21 14:43:01', 'system', '2020-07-21 14:43:01', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465286655430656', '0', '0,', 50, '0000000050,', '1', 0, '暂停', '暂停', '2', 'sys_job_status', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:43:02', 'system', '2020-07-21 14:43:02', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465289310425088', '0', '0,', 30, '0000000030,', '1', 0, '完成', '完成', '3', 'sys_job_status', '1', '', '', '', '0', 'system', '2020-07-21 14:43:03', 'system', '2020-07-21 14:43:03', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465291818618880', '0', '0,', 40, '0000000040,', '1', 0, '错误', '错误', '4', 'sys_job_status', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:43:03', 'system', '2020-07-21 14:43:03', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465292758142976', '0', '0,', 50, '0000000050,', '1', 0, '运行', '运行', '5', 'sys_job_status', '1', '', '', '', '0', 'system', '2020-07-21 14:43:04', 'system', '2020-07-21 14:43:04', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465296470102016', '0', '0,', 30, '0000000030,', '1', 0, '计划日志', '计划日志', 'scheduler', 'sys_job_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:05', 'system', '2020-07-21 14:43:05', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465299066376192', '0', '0,', 40, '0000000040,', '1', 0, '任务日志', '任务日志', 'job', 'sys_job_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:05', 'system', '2020-07-21 14:43:05', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465301641678848', '0', '0,', 50, '0000000050,', '1', 0, '触发日志', '触发日志', 'trigger', 'sys_job_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:05', 'system', '2020-07-21 14:43:05', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465302509899776', '0', '0,', 30, '0000000030,', '1', 0, '计划创建', '计划创建', 'jobScheduled', 'sys_job_event', '1', '', '', '', '0', 'system', '2020-07-21 14:43:06', 'system', '2020-07-21 14:43:06', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465303373926400', '0', '0,', 40, '0000000040,', '1', 0, '计划移除', '计划移除', 'jobUnscheduled', 'sys_job_event', '1', '', '', '', '0', 'system', '2020-07-21 14:43:06', 'system', '2020-07-21 14:43:06', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465305764679680', '0', '0,', 50, '0000000050,', '1', 0, '计划暂停', '计划暂停', 'triggerPaused', 'sys_job_event', '1', '', '', '', '0', 'system', '2020-07-21 14:43:07', 'system', '2020-07-21 14:43:07', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465309728296960', '0', '0,', 60, '0000000060,', '1', 0, '计划恢复', '计划恢复', 'triggerResumed', 'sys_job_event', '1', '', '', '', '0', 'system', '2020-07-21 14:43:12', 'system', '2020-07-21 14:43:12', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465328141291520', '0', '0,', 70, '0000000070,', '1', 0, '调度错误', '调度错误', 'schedulerError', 'sys_job_event', '1', '', '', '', '0', 'system', '2020-07-21 14:43:12', 'system', '2020-07-21 14:43:12', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465329080815616', '0', '0,', 80, '0000000080,', '1', 0, '任务执行', '任务执行', 'jobToBeExecuted', 'sys_job_event', '1', '', '', '', '0', 'system', '2020-07-21 14:43:12', 'system', '2020-07-21 14:43:12', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465331643535360', '0', '0,', 90, '0000000090,', '1', 0, '任务结束', '任务结束', 'jobWasExecuted', 'sys_job_event', '1', '', '', '', '0', 'system', '2020-07-21 14:43:13', 'system', '2020-07-21 14:43:13', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465332620808192', '0', '0,', 100, '0000000100,', '1', 0, '任务停止', '任务停止', 'jobExecutionVetoed', 'sys_job_event', '1', '', '', '', '0', 'system', '2020-07-21 14:43:13', 'system', '2020-07-21 14:43:13', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465335191916544', '0', '0,', 110, '0000000110,', '1', 0, '触发计划', '触发计划', 'triggerFired', 'sys_job_event', '1', '', '', '', '0', 'system', '2020-07-21 14:43:14', 'system', '2020-07-21 14:43:14', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465337536532480', '0', '0,', 120, '0000000120,', '1', 0, '触发验证', '触发验证', 'vetoJobExecution', 'sys_job_event', '1', '', '', '', '0', 'system', '2020-07-21 14:43:14', 'system', '2020-07-21 14:43:14', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465340103446528', '0', '0,', 130, '0000000130,', '1', 0, '触发完成', '触发完成', 'triggerComplete', 'sys_job_event', '1', '', '', '', '0', 'system', '2020-07-21 14:43:15', 'system', '2020-07-21 14:43:15', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465341131051008', '0', '0,', 140, '0000000140,', '1', 0, '触发错过', '触发错过', 'triggerMisfired', 'sys_job_event', '1', '', '', '', '0', 'system', '2020-07-21 14:43:15', 'system', '2020-07-21 14:43:15', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465344008343552', '0', '0,', 30, '0000000030,', '1', 0, 'PC', 'PC', 'pc', 'sys_msg_type', '1', '消息类型', '', '', '0', 'system', '2020-07-21 14:43:16', 'system', '2020-07-21 14:43:16', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465344952061952', '0', '0,', 40, '0000000040,', '1', 0, 'APP', 'APP', 'app', 'sys_msg_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:16', 'system', '2020-07-21 14:43:16', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465347875491840', '0', '0,', 50, '0000000050,', '1', 0, '短信', '短信', 'sms', 'sys_msg_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:19', 'system', '2020-07-21 14:43:19', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465357857935360', '0', '0,', 60, '0000000060,', '1', 0, '邮件', '邮件', 'email', 'sys_msg_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:19', 'system', '2020-07-21 14:43:19', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465358801653760', '0', '0,', 70, '0000000070,', '1', 0, '微信', '微信', 'weixin', 'sys_msg_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:20', 'system', '2020-07-21 14:43:20', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465365650952192', '0', '0,', 30, '0000000030,', '1', 0, '待推送', '待推送', '0', 'sys_msg_push_status', '1', '推送状态', '', '', '0', 'system', '2020-07-21 14:43:22', 'system', '2020-07-21 14:43:22', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465370918998016', '0', '0,', 30, '0000000030,', '1', 0, '成功', '成功', '1', 'sys_msg_push_status', '1', '', '', '', '0', 'system', '2020-07-21 14:43:22', 'system', '2020-07-21 14:43:22', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465373737570304', '0', '0,', 40, '0000000040,', '1', 0, '失败', '失败', '2', 'sys_msg_push_status', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:43:23', 'system', '2020-07-21 14:43:23', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465376589697024', '0', '0,', 30, '0000000030,', '1', 0, '未送达', '未送达', '0', 'sys_msg_read_status', '1', '读取状态', '', '', '0', 'system', '2020-07-21 14:43:23', 'system', '2020-07-21 14:43:23', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465377508249600', '0', '0,', 40, '0000000040,', '1', 0, '已读', '已读', '1', 'sys_msg_read_status', '1', '', '', '', '0', 'system', '2020-07-21 14:43:24', 'system', '2020-07-21 14:43:24', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465378464550912', '0', '0,', 50, '0000000050,', '1', 0, '未读', '未读', '2', 'sys_msg_read_status', '1', '', '', '', '0', 'system', '2020-07-21 14:43:27', 'system', '2020-07-21 14:43:27', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465392599355392', '0', '0,', 30, '0000000030,', '1', 0, '普通', '普通', '1', 'msg_inner_content_level', '1', '内容级别', '', '', '0', 'system', '2020-07-21 14:43:28', 'system', '2020-07-21 14:43:28', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465398186168320', '0', '0,', 40, '0000000040,', '1', 0, '一般', '一般', '2', 'msg_inner_content_level', '1', '', '', '', '0', 'system', '2020-07-21 14:43:30', 'system', '2020-07-21 14:43:30', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465405064826880', '0', '0,', 50, '0000000050,', '1', 0, '紧急', '紧急', '3', 'msg_inner_content_level', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:43:30', 'system', '2020-07-21 14:43:30', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465407237476352', '0', '0,', 30, '0000000030,', '1', 0, '公告', '公告', '1', 'msg_inner_content_type', '1', '内容类型', '', '', '0', 'system', '2020-07-21 14:43:32', 'system', '2020-07-21 14:43:32', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465412769763328', '0', '0,', 40, '0000000040,', '1', 0, '新闻', '新闻', '2', 'msg_inner_content_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:32', 'system', '2020-07-21 14:43:32', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465413684121600', '0', '0,', 50, '0000000050,', '1', 0, '会议', '会议', '3', 'msg_inner_content_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:33', 'system', '2020-07-21 14:43:33', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465418104918016', '0', '0,', 60, '0000000060,', '1', 0, '其它', '其它', '4', 'msg_inner_content_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:34', 'system', '2020-07-21 14:43:34', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465422202753024', '0', '0,', 30, '0000000030,', '1', 0, '全部', '全部', '0', 'msg_inner_receiver_type', '1', '接受类型', '', '', '0', 'system', '2020-07-21 14:43:35', 'system', '2020-07-21 14:43:35', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465424794832896', '0', '0,', 30, '0000000030,', '1', 0, '用户', '用户', '1', 'msg_inner_receiver_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:35', 'system', '2020-07-21 14:43:35', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465425734356992', '0', '0,', 40, '0000000040,', '1', 0, '部门', '部门', '2', 'msg_inner_receiver_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:35', 'system', '2020-07-21 14:43:35', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465426703241216', '0', '0,', 50, '0000000050,', '1', 0, '角色', '角色', '3', 'msg_inner_receiver_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:36', 'system', '2020-07-21 14:43:36', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465430608138240', '0', '0,', 60, '0000000060,', '1', 0, '岗位', '岗位', '4', 'msg_inner_receiver_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:38', 'system', '2020-07-21 14:43:38', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465437025423360', '0', '0,', 30, '0000000030,', '1', 0, '正常', '正常', '0', 'msg_inner_msg_status', '1', '消息状态', '', '', '0', 'system', '2020-07-21 14:43:38', 'system', '2020-07-21 14:43:38', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465439391010816', '0', '0,', 40, '0000000040,', '1', 0, '删除', '删除', '1', 'msg_inner_msg_status', '1', '', '', '', '0', 'system', '2020-07-21 14:43:38', 'system', '2020-07-21 14:43:38', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465440338923520', '0', '0,', 50, '0000000050,', '1', 0, '审核', '审核', '4', 'msg_inner_msg_status', '1', '', '', '', '0', 'system', '2020-07-21 14:43:39', 'system', '2020-07-21 14:43:39', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465445158178816', '0', '0,', 60, '0000000060,', '1', 0, '驳回', '驳回', '5', 'msg_inner_msg_status', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:43:49', 'system', '2020-07-21 14:43:49', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465486203637760', '0', '0,', 70, '0000000070,', '1', 0, '草稿', '草稿', '9', 'msg_inner_msg_status', '1', '', '', '', '0', 'system', '2020-07-21 14:43:50', 'system', '2020-07-21 14:43:50', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465489101901824', '0', '0,', 30, '0000000030,', '1', 0, '公共文件柜', '公共文件柜', 'global', 'filemanager_group_type', '1', '文件组类型', '', '', '0', 'system', '2020-07-21 14:43:51', 'system', '2020-07-21 14:43:51', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465493346537472', '0', '0,', 40, '0000000040,', '1', 0, '个人文件柜', '个人文件柜', 'self', 'filemanager_group_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:52', 'system', '2020-07-21 14:43:52', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465498329370624', '0', '0,', 50, '0000000050,', '1', 0, '部门文件柜', '部门文件柜', 'office', 'filemanager_group_type', '1', '', '', '', '0', 'system', '2020-07-21 14:43:53', 'system', '2020-07-21 14:43:53', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465502150381568', '0', '0,', 30, '0000000030,', '1', 0, '活动', '活动', '1', 'bpm_def_status', '1', '', '', '', '0', 'system', '2020-07-21 14:43:54', 'system', '2020-07-21 14:43:54', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465504931205120', '0', '0,', 60, '0000000060,', '1', 0, '挂起', '挂起', '2', 'bpm_def_status', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:43:54', 'system', '2020-07-21 14:43:54', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465505795231744', '0', '0,', 30, '0000000030,', '1', 0, '进行', '进行', '1', 'bpm_ins_status', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:44:04', 'system', '2020-07-21 14:44:04', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465546345762816', '0', '0,', 60, '0000000060,', '1', 0, '结束', '结束', '2', 'bpm_ins_status', '1', '', 'color:#aaa;', '', '0', 'system', '2020-07-21 14:44:04', 'system', '2020-07-21 14:44:04', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465549994807296', '0', '0,', 30, '0000000030,', '1', 0, 'URL表单', 'URL表单', 'url', 'bpm_form_type', '1', '', '', '', '0', 'system', '2020-07-21 14:44:05', 'system', '2020-07-21 14:44:05', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465550955302912', '0', '0,', 30, '0000000030,', '1', 0, '已完成', '已完成', '0', 'bpm_biz_status', '1', '', '', '', '0', 'system', '2020-07-21 14:44:05', 'system', '2020-07-21 14:44:05', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465553346056192', '0', '0,', 60, '0000000060,', '1', 0, '审核中', '审核中', '4', 'bpm_biz_status', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:44:06', 'system', '2020-07-21 14:44:06', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465557288701952', '0', '0,', 80, '0000000080,', '1', 0, '已退回', '已退回', '5', 'bpm_biz_status', '1', '', 'color:#a00;', '', '0', 'system', '2020-07-21 14:44:07', 'system', '2020-07-21 14:44:07', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465561143267328', '0', '0,', 120, '0000000120,', '1', 0, '已终止', '已终止', '1', 'bpm_biz_status', '1', '', 'color:#aaa;', '', '0', 'system', '2020-07-21 14:44:07', 'system', '2020-07-21 14:44:07', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465562120540160', '0', '0,', 260, '0000000260,', '1', 0, '草稿', '草稿', '9', 'bpm_biz_status', '1', '', 'color:#aaa;', '', '0', 'system', '2020-07-21 14:44:08', 'system', '2020-07-21 14:44:08', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465564423213056', '0', '0,', 30, '0000000030,', '1', 0, '一般', '一般', '50', 'bpm_task_priority', '1', '', '', '', '0', 'system', '2020-07-21 14:44:08', 'system', '2020-07-21 14:44:08', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465566755246080', '0', '0,', 60, '0000000060,', '1', 0, '重要', '重要', '100', 'bpm_task_priority', '1', '', 'color:#f0f;', '', '0', 'system', '2020-07-21 14:44:09', 'system', '2020-07-21 14:44:09', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465570907607040', '0', '0,', 90, '0000000090,', '1', 0, '紧急', '紧急', '150', 'bpm_task_priority', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:44:10', 'system', '2020-07-21 14:44:10', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465571842936832', '0', '0,', 30, '0000000030,', '1', 0, '待办', '待办', '1', 'bpm_task_status', '1', '', 'color:#f00;', '', '0', 'system', '2020-07-21 14:44:10', 'system', '2020-07-21 14:44:10', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465572774072320', '0', '0,', 60, '0000000060,', '1', 0, '已办', '已办', '2', 'bpm_task_status', '1', '', 'color:#aaa;', '', '0', 'system', '2020-07-21 14:44:10', 'system', '2020-07-21 14:44:10', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465573747150848', '0', '0,', 30, '0000000030,', '1', 0, '流程启动', '流程启动', 'PROCESS_STARTED', 'bpm_event_type', '1', '', '', '', '0', 'system', '2020-07-21 14:44:10', 'system', '2020-07-21 14:44:10', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465574669897728', '0', '0,', 60, '0000000060,', '1', 0, '流程完成', '流程完成', 'PROCESS_COMPLETED', 'bpm_event_type', '1', '', '', '', '0', 'system', '2020-07-21 14:44:11', 'system', '2020-07-21 14:44:11', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465577140342784', '0', '0,', 90, '0000000090,', '1', 0, '流程取消', '流程取消', 'PROCESS_CANCELLED', 'bpm_event_type', '1', '', '', '', '0', 'system', '2020-07-21 14:44:11', 'system', '2020-07-21 14:44:11', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465578084061184', '0', '0,', 200, '0000000200,', '1', 0, '任务分配', '任务分配', 'TASK_ASSIGNED', 'bpm_event_type', '1', '', '', '', '0', 'system', '2020-07-21 14:44:12', 'system', '2020-07-21 14:44:12', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465583045922816', '0', '0,', 220, '0000000220,', '1', 0, '任务创建', '任务创建', 'TASK_CREATED', 'bpm_event_type', '1', '', '', '', '0', 'system', '2020-07-21 14:44:13', 'system', '2020-07-21 14:44:13', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465583989641216', '0', '0,', 250, '0000000250,', '1', 0, '任务完成', '任务完成', 'TASK_COMPLETED', 'bpm_event_type', '1', '', '', '', '0', 'system', '2020-07-21 14:44:13', 'system', '2020-07-21 14:44:13', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465584908193792', '0', '0,', 30, '0000000030,', '1', 0, 'Groovy', 'Groovy', 'groovy', 'bpm_script_lang', '1', '', '', '', '0', 'system', '2020-07-21 14:44:13', 'system', '2020-07-21 14:44:13', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465585843523584', '0', '0,', 60, '0000000060,', '1', 0, 'Beetl', 'Beetl', 'beetl', 'bpm_script_lang', '1', '', '', '', '0', 'system', '2020-07-21 14:44:13', 'system', '2020-07-21 14:44:13', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465586837573632', '0', '0,', 30, '0000000030,', '1', 0, '系统脚本', '系统脚本', 'sys', 'bpm_script_type', '1', '', '', '', '0', 'system', '2020-07-21 14:44:15', 'system', '2020-07-21 14:44:15', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_dict_data` VALUES ('1285465593787535360', '0', '0,', 60, '0000000060,', '1', 0, '流程脚本', '流程脚本', 'bpm', 'bpm_script_type', '1', '', '', '', '0', 'system', '2020-07-21 14:44:15', 'system', '2020-07-21 14:44:15', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for js_sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_dict_type`;
CREATE TABLE `js_sys_dict_type`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典类型',
  `is_sys` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否系统字典',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_dict_type_is`(`is_sys`) USING BTREE,
  INDEX `idx_sys_dict_type_status`(`status`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_dict_type
-- ----------------------------
INSERT INTO `js_sys_dict_type` VALUES ('1285465027996897280', '是否', 'sys_yes_no', '1', '0', 'system', '2020-07-21 14:42:00', 'system', '2020-07-21 14:42:00', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465028907061248', '状态', 'sys_status', '1', '0', 'system', '2020-07-21 14:42:00', 'system', '2020-07-21 14:42:00', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465029687201792', '显示隐藏', 'sys_show_hide', '1', '0', 'system', '2020-07-21 14:42:01', 'system', '2020-07-21 14:42:01', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465030463148032', '国际化语言类型', 'sys_lang_type', '1', '0', 'system', '2020-07-21 14:42:01', 'system', '2020-07-21 14:42:01', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465032593854464', '客户端设备类型', 'sys_device_type', '1', '0', 'system', '2020-07-21 14:42:01', 'system', '2020-07-21 14:42:01', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465033399160832', '菜单归属系统', 'sys_menu_sys_code', '1', '0', 'system', '2020-07-21 14:42:01', 'system', '2020-07-21 14:42:01', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465034175107072', '菜单类型', 'sys_menu_type', '1', '0', 'system', '2020-07-21 14:42:02', 'system', '2020-07-21 14:42:02', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465036465197056', '菜单权重', 'sys_menu_weight', '1', '0', 'system', '2020-07-21 14:42:03', 'system', '2020-07-21 14:42:03', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465038642040832', '区域类型', 'sys_area_type', '1', '0', 'system', '2020-07-21 14:42:03', 'system', '2020-07-21 14:42:03', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465039535427584', '机构类型', 'sys_office_type', '1', '0', 'system', '2020-07-21 14:42:03', 'system', '2020-07-21 14:42:03', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465041733242880', '查询状态', 'sys_search_status', '1', '0', 'system', '2020-07-21 14:42:03', 'system', '2020-07-21 14:42:03', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465042647601152', '用户性别', 'sys_user_sex', '1', '0', 'system', '2020-07-21 14:42:04', 'system', '2020-07-21 14:42:04', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465043482267648', '用户状态', 'sys_user_status', '1', '0', 'system', '2020-07-21 14:42:04', 'system', '2020-07-21 14:42:04', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465044291768320', '用户类型', 'sys_user_type', '1', '0', 'system', '2020-07-21 14:42:04', 'system', '2020-07-21 14:42:04', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465045109657600', '角色分类', 'sys_role_type', '1', '0', 'system', '2020-07-21 14:42:05', 'system', '2020-07-21 14:42:05', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465047286501376', '角色数据范围', 'sys_role_data_scope', '1', '0', 'system', '2020-07-21 14:42:05', 'system', '2020-07-21 14:42:05', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465049740169216', '角色业务范围', 'sys_role_biz_scope', '1', '0', 'system', '2020-07-21 14:42:05', 'system', '2020-07-21 14:42:05', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465050671304704', '岗位分类', 'sys_post_type', '1', '0', 'system', '2020-07-21 14:42:06', 'system', '2020-07-21 14:42:06', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465051489193984', '日志类型', 'sys_log_type', '1', '0', 'system', '2020-07-21 14:42:06', 'system', '2020-07-21 14:42:06', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465053972221952', '作业分组', 'sys_job_group', '1', '0', 'system', '2020-07-21 14:42:06', 'system', '2020-07-21 14:42:06', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465054865608704', '作业错过策略', 'sys_job_misfire_instruction', '1', '0', 'system', '2020-07-21 14:42:09', 'system', '2020-07-21 14:42:09', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465064600588288', '作业状态', 'sys_job_status', '1', '0', 'system', '2020-07-21 14:42:09', 'system', '2020-07-21 14:42:09', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465066882289664', '作业任务类型', 'sys_job_type', '1', '0', 'system', '2020-07-21 14:42:09', 'system', '2020-07-21 14:42:09', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465067637264384', '作业任务事件', 'sys_job_event', '1', '0', 'system', '2020-07-21 14:42:10', 'system', '2020-07-21 14:42:10', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465069759582208', '消息类型', 'sys_msg_type', '1', '0', 'system', '2020-07-21 14:42:10', 'system', '2020-07-21 14:42:10', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465070858489856', '推送状态', 'sys_msg_push_status', '1', '0', 'system', '2020-07-21 14:42:10', 'system', '2020-07-21 14:42:10', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465071667990528', '读取状态', 'sys_msg_read_status', '1', '0', 'system', '2020-07-21 14:42:11', 'system', '2020-07-21 14:42:11', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465072477491200', '内容级别', 'msg_inner_content_level', '1', '0', 'system', '2020-07-21 14:42:11', 'system', '2020-07-21 14:42:11', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465073295380480', '内容类型', 'msg_inner_content_type', '1', '0', 'system', '2020-07-21 14:42:11', 'system', '2020-07-21 14:42:11', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465074100686848', '接受类型', 'msg_inner_receiver_type', '1', '0', 'system', '2020-07-21 14:42:11', 'system', '2020-07-21 14:42:11', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465074910187520', '消息状态', 'msg_inner_msg_status', '1', '0', 'system', '2020-07-21 14:42:11', 'system', '2020-07-21 14:42:11', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465075723882496', '文件组类型', 'filemanager_group_type', '1', '0', 'system', '2020-07-21 14:42:12', 'system', '2020-07-21 14:42:12', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465076516605952', '流程定义状态', 'bpm_def_status', '1', '0', 'system', '2020-07-21 14:42:12', 'system', '2020-07-21 14:42:12', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465077321912320', '流程实例状态', 'bpm_ins_status', '1', '0', 'system', '2020-07-21 14:42:12', 'system', '2020-07-21 14:42:12', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465079507144704', '流程表单类型', 'bpm_form_type', '1', '0', 'system', '2020-07-21 14:42:12', 'system', '2020-07-21 14:42:12', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465080408920064', '流程业务状态', 'bpm_biz_status', '1', '0', 'system', '2020-07-21 14:42:13', 'system', '2020-07-21 14:42:13', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465082967445504', '流程任务优先级', 'bpm_task_priority', '1', '0', 'system', '2020-07-21 14:42:13', 'system', '2020-07-21 14:42:13', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465083789529088', '流程任务状态', 'bpm_task_status', '1', '0', 'system', '2020-07-21 14:42:13', 'system', '2020-07-21 14:42:13', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465084594835456', '流程事件类型', 'bpm_event_type', '1', '0', 'system', '2020-07-21 14:42:15', 'system', '2020-07-21 14:42:15', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465089326010368', '流程脚本类型', 'bpm_script_type', '1', '0', 'system', '2020-07-21 14:42:15', 'system', '2020-07-21 14:42:15', NULL);
INSERT INTO `js_sys_dict_type` VALUES ('1285465090198425600', '流程脚本语言', 'bpm_script_lang', '1', '0', 'system', '2020-07-21 14:42:15', 'system', '2020-07-21 14:42:15', NULL);

-- ----------------------------
-- Table structure for js_sys_employee
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_employee`;
CREATE TABLE `js_sys_employee`  (
  `emp_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工编码',
  `emp_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工姓名',
  `emp_name_en` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工英文名',
  `emp_no` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '员工工号',
  `office_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构编码',
  `office_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构名称',
  `company_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司编码',
  `company_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公司名称',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0在职 1删除 2离职）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`emp_code`) USING BTREE,
  INDEX `idx_sys_employee_cco`(`company_code`) USING BTREE,
  INDEX `idx_sys_employee_cc`(`corp_code`) USING BTREE,
  INDEX `idx_sys_employee_ud`(`update_date`) USING BTREE,
  INDEX `idx_sys_employee_oc`(`office_code`) USING BTREE,
  INDEX `idx_sys_employee_status`(`status`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '员工表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_employee
-- ----------------------------
INSERT INTO `js_sys_employee` VALUES ('user10_zywb', '用户10', NULL, 'user10_zywb', 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:36', 'system', '2020-07-21 14:46:36', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user11_h3nb', '用户11', NULL, 'user11_h3nb', 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:37', 'system', '2020-07-21 14:46:37', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user12_cjoc', '用户12', NULL, 'user12_cjoc', 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:39', 'system', '2020-07-21 14:46:39', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user13_v385', '用户13', NULL, 'user13_v385', 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:40', 'system', '2020-07-21 14:46:40', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user14_3xop', '用户14', NULL, 'user14_3xop', 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:41', 'system', '2020-07-21 14:46:41', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user15_61pf', '用户15', NULL, 'user15_61pf', 'SDQD01', '企管部', 'SDQD', '青岛公司', '0', 'system', '2020-07-21 14:46:43', 'system', '2020-07-21 14:46:43', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user16_txve', '用户16', NULL, 'user16_txve', 'SDQD01', '企管部', 'SDQD', '青岛公司', '0', 'system', '2020-07-21 14:46:43', 'system', '2020-07-21 14:46:43', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user17_gs1x', '用户17', NULL, 'user17_gs1x', 'SDQD01', '企管部', 'SDQD', '青岛公司', '0', 'system', '2020-07-21 14:46:44', 'system', '2020-07-21 14:46:44', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user18_vqbf', '用户18', NULL, 'user18_vqbf', 'SDQD02', '财务部', 'SDQD', '青岛公司', '0', 'system', '2020-07-21 14:46:45', 'system', '2020-07-21 14:46:45', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user19_jzf1', '用户19', NULL, 'user19_jzf1', 'SDQD02', '财务部', 'SDQD', '青岛公司', '0', 'system', '2020-07-21 14:46:47', 'system', '2020-07-21 14:46:47', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user1_mhhh', '用户01', NULL, 'user1_mhhh', 'SDJN01', '企管部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:24', 'system', '2020-07-21 14:46:24', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user20_tbcj', '用户20', NULL, 'user20_tbcj', 'SDQD02', '财务部', 'SDQD', '青岛公司', '0', 'system', '2020-07-21 14:46:48', 'system', '2020-07-21 14:46:48', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user21_zmvi', '用户21', NULL, 'user21_zmvi', 'SDQD03', '研发部', 'SDQD', '青岛公司', '0', 'system', '2020-07-21 14:46:49', 'system', '2020-07-21 14:46:49', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user22_sdc5', '用户22', NULL, 'user22_sdc5', 'SDQD03', '研发部', 'SDQD', '青岛公司', '0', 'system', '2020-07-21 14:46:50', 'system', '2020-07-21 14:46:50', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user23_732v', '用户23', NULL, 'user23_732v', 'SDQD03', '研发部', 'SDQD', '青岛公司', '0', 'system', '2020-07-21 14:46:51', 'system', '2020-07-21 14:46:51', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user2_wpxe', '用户02', NULL, 'user2_wpxe', 'SDJN01', '企管部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:25', 'system', '2020-07-21 14:46:25', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user3_6ojk', '用户03', NULL, 'user3_6ojk', 'SDJN01', '企管部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:27', 'system', '2020-07-21 14:46:27', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user4_dgwm', '用户04', NULL, 'user4_dgwm', 'SDJN02', '财务部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:28', 'system', '2020-07-21 14:46:28', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user5_0wsp', '用户05', NULL, 'user5_0wsp', 'SDJN02', '财务部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:30', 'system', '2020-07-21 14:46:30', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user6_9xba', '用户06', NULL, 'user6_9xba', 'SDJN02', '财务部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:31', 'system', '2020-07-21 14:46:31', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user7_hhr4', '用户07', NULL, 'user7_hhr4', 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:32', 'system', '2020-07-21 14:46:32', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user8_20v7', '用户08', NULL, 'user8_20v7', 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:33', 'system', '2020-07-21 14:46:33', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_employee` VALUES ('user9_hejq', '用户09', NULL, 'user9_hejq', 'SDJN03', '研发部', 'SDJN', '济南公司', '0', 'system', '2020-07-21 14:46:34', 'system', '2020-07-21 14:46:34', NULL, '0', 'JeeSite');

-- ----------------------------
-- Table structure for js_sys_employee_office
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_employee_office`;
CREATE TABLE `js_sys_employee_office`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `emp_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工编码',
  `office_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构编码',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位编码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '员工附属机构关系表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_employee_post
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_employee_post`;
CREATE TABLE `js_sys_employee_post`  (
  `emp_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工编码',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  PRIMARY KEY (`emp_code`, `post_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '员工与岗位关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_employee_post
-- ----------------------------
INSERT INTO `js_sys_employee_post` VALUES ('user10_zywb', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user11_h3nb', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user12_cjoc', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user13_v385', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user14_3xop', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user15_61pf', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user16_txve', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user17_gs1x', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user18_vqbf', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user19_jzf1', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user1_mhhh', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user20_tbcj', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user21_zmvi', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user22_sdc5', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user23_732v', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user2_wpxe', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user3_6ojk', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user4_dgwm', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user5_0wsp', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user6_9xba', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user7_hhr4', 'dept');
INSERT INTO `js_sys_employee_post` VALUES ('user8_20v7', 'user');
INSERT INTO `js_sys_employee_post` VALUES ('user9_hejq', 'user');

-- ----------------------------
-- Table structure for js_sys_file_entity
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_file_entity`;
CREATE TABLE `js_sys_file_entity`  (
  `file_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件编号',
  `file_md5` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件MD5',
  `file_path` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件相对路径',
  `file_content_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件内容类型',
  `file_extension` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件后缀扩展名',
  `file_size` decimal(31, 0) NOT NULL COMMENT '文件大小(单位B)',
  `file_meta` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件信息(JSON格式)',
  PRIMARY KEY (`file_id`) USING BTREE,
  INDEX `idx_sys_file_entity_md5`(`file_md5`) USING BTREE,
  INDEX `idx_sys_file_entity_size`(`file_size`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件实体表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_file_upload
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_file_upload`;
CREATE TABLE `js_sys_file_upload`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `file_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件编号',
  `file_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件名称',
  `file_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文件分类（image、media、file）',
  `file_sort` decimal(10, 0) NULL DEFAULT NULL COMMENT '文件排序（升序）',
  `biz_key` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_file_biz_ft`(`file_type`) USING BTREE,
  INDEX `idx_sys_file_biz_fi`(`file_id`) USING BTREE,
  INDEX `idx_sys_file_biz_status`(`status`) USING BTREE,
  INDEX `idx_sys_file_biz_cb`(`create_by`) USING BTREE,
  INDEX `idx_sys_file_biz_ud`(`update_date`) USING BTREE,
  INDEX `idx_sys_file_biz_bt`(`biz_type`) USING BTREE,
  INDEX `idx_sys_file_biz_bk`(`biz_key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件上传表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_job
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_job`;
CREATE TABLE `js_sys_job`  (
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务描述',
  `invoke_target` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Cron执行表达式',
  `misfire_instruction` decimal(1, 0) NOT NULL COMMENT '计划执行错误策略',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否并发执行',
  `instance_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'JeeSiteScheduler' COMMENT '集群的实例名字',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1删除 2暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`job_name`, `job_group`) USING BTREE,
  INDEX `idx_sys_job_status`(`status`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '作业调度表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_job
-- ----------------------------
INSERT INTO `js_sys_job` VALUES ('MsgLocalMergePushTask', 'SYSTEM', '消息推送服务 (合并推送)', 'msgLocalMergePushTask.execute()', '0 0/30 * * * ?', 2, '0', 'JeeSiteScheduler', '2', 'system', '2020-07-21 14:46:52', 'system', '2020-07-21 14:46:52', NULL);
INSERT INTO `js_sys_job` VALUES ('MsgLocalPushTask', 'SYSTEM', '消息推送服务 (实时推送)', 'msgLocalPushTask.execute()', '0/3 * * * * ?', 2, '0', 'JeeSiteScheduler', '2', 'system', '2020-07-21 14:46:52', 'system', '2020-07-21 14:46:52', NULL);

-- ----------------------------
-- Table structure for js_sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_job_log`;
CREATE TABLE `js_sys_job_log`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `job_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志类型',
  `job_event` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志事件',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `is_exception` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否异常',
  `exception_info` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '异常信息',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_job_log_jn`(`job_name`) USING BTREE,
  INDEX `idx_sys_job_log_jg`(`job_group`) USING BTREE,
  INDEX `idx_sys_job_log_t`(`job_type`) USING BTREE,
  INDEX `idx_sys_job_log_e`(`job_event`) USING BTREE,
  INDEX `idx_sys_job_log_ie`(`is_exception`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '作业调度日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_lang
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_lang`;
CREATE TABLE `js_sys_lang`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `module_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '归属模块',
  `lang_code` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '语言编码',
  `lang_text` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '语言译文',
  `lang_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '语言类型',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_lang_type`(`lang_type`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '国际化语言' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_log`;
CREATE TABLE `js_sys_log`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `log_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日志类型',
  `log_title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日志标题',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_by_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名称',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `request_uri` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求URI',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作方式',
  `request_params` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '操作提交的数据',
  `diff_modify_data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '新旧数据比较结果',
  `biz_key` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务类型',
  `remote_addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作IP地址',
  `server_addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '请求服务器地址',
  `is_exception` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否异常',
  `exception_info` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '异常信息',
  `user_agent` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户代理',
  `device_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设备名称/操作系统',
  `browser_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '浏览器名称',
  `execute_time` decimal(19, 0) NULL DEFAULT NULL COMMENT '执行时间',
  `corp_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_log_cb`(`create_by`) USING BTREE,
  INDEX `idx_sys_log_cc`(`corp_code`) USING BTREE,
  INDEX `idx_sys_log_lt`(`log_type`) USING BTREE,
  INDEX `idx_sys_log_bk`(`biz_key`) USING BTREE,
  INDEX `idx_sys_log_bt`(`biz_type`) USING BTREE,
  INDEX `idx_sys_log_ie`(`is_exception`) USING BTREE,
  INDEX `idx_sys_log_cd`(`create_date`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_log
-- ----------------------------
INSERT INTO `js_sys_log` VALUES ('1285468999021539328', 'loginLogout', '系统登录', 'system', '超级管理员', '2020-07-21 14:57:47', '/js/a/login', 'POST', 'username=F3EDC7D2C193E0B8DCF554C726719ED2&password=*&validCode=&rememberUserCode=on&__url=', NULL, NULL, NULL, '0:0:0:0:0:0:0:1', 'http://localhost:8980', '0', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Windows 10', 'Firefox 7', 0, '0', 'JeeSite');
INSERT INTO `js_sys_log` VALUES ('1285469227615301632', 'access', '系统管理/组织管理/用户管理', 'system', '超级管理员', '2020-07-21 14:58:41', '/js/a/sys/empUser/index', 'GET', '', NULL, '', 'EmpUser', '0:0:0:0:0:0:0:1', 'http://localhost:8980', '0', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Windows 10', 'Firefox 7', 144, '0', 'JeeSite');
INSERT INTO `js_sys_log` VALUES ('1285469230832332800', 'select', '系统管理/组织管理/用户管理/查看', 'system', '超级管理员', '2020-07-21 14:58:42', '/js/a/sys/empUser/list', 'GET', '', NULL, '', 'EmpUser', '0:0:0:0:0:0:0:1', 'http://localhost:8980', '0', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Windows 10', 'Firefox 7', 540, '0', 'JeeSite');
INSERT INTO `js_sys_log` VALUES ('1285469235328626688', 'select', '系统管理/组织管理/用户管理', 'system', '超级管理员', '2020-07-21 14:58:43', '/js/a/sys/empUser/listData', 'POST', 'ctrlPermi=2&loginCode=&userName=&refName=&mobile=&status=&employee.office.officeName=&employee.office.officeCode=&employee.company.companyName=&employee.company.companyCode=&email=&employee.postCode=&phone=&pageNo=&pageSize=&orderBy=', NULL, '', 'EmpUser', '0:0:0:0:0:0:0:1', 'http://localhost:8980', '0', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Windows 10', 'Firefox 7', 689, '0', 'JeeSite');
INSERT INTO `js_sys_log` VALUES ('1285469261257814016', 'select', '系统管理/系统设置/菜单管理', 'system', '超级管理员', '2020-07-21 14:58:49', '/js/a/sys/menu/list', 'GET', '', NULL, '', 'Menu', '0:0:0:0:0:0:0:1', 'http://localhost:8980', '0', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Windows 10', 'Firefox 7', 755, '0', 'JeeSite');
INSERT INTO `js_sys_log` VALUES ('1285469262876815360', 'select', '系统管理/系统设置/菜单管理', 'system', '超级管理员', '2020-07-21 14:58:50', '/js/a/sys/menu/listData', 'POST', 'moduleCodes=&sysCode=default&menuNameOrig=&pageNo=&pageSize=&orderBy=', NULL, '', 'Menu', '0:0:0:0:0:0:0:1', 'http://localhost:8980', '0', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Windows 10', 'Firefox 7', 236, '0', 'JeeSite');
INSERT INTO `js_sys_log` VALUES ('1285469269298294784', 'select', '系统管理/系统设置/菜单管理', 'system', '超级管理员', '2020-07-21 14:58:51', '/js/a/sys/menu/listData', 'POST', 'id=&nodeid=1028924699103330304&parentid=0&n_level=0&_search=false&nd=1595314731120&pageSize=&pageNo=&orderBy=&sord=asc&moduleCodes=&sysCode=default&menuNameOrig=&parentCode=1028924699103330304', NULL, '', 'Menu', '0:0:0:0:0:0:0:1', 'http://localhost:8980', '0', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Windows 10', 'Firefox 7', 195, '0', 'JeeSite');
INSERT INTO `js_sys_log` VALUES ('1285469279280738304', 'select', '系统管理/系统设置/菜单管理', 'system', '超级管理员', '2020-07-21 14:58:54', '/js/a/sys/menu/form', 'GET', 'menuCode=1028924891047264256', NULL, '1028924891047264256', 'Menu', '0:0:0:0:0:0:0:1', 'http://localhost:8980', '0', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Windows 10', 'Firefox 7', 248, '0', 'JeeSite');
INSERT INTO `js_sys_log` VALUES ('1285469337048887296', 'update', '系统管理/系统设置/菜单管理', 'system', '超级管理员', '2020-07-21 14:59:07', '/js/a/sys/menu/save', 'POST', 'sysCode=default&menuCode=1028924891047264256&parent.menuNameOrig=JFlow工作流&parent.id=1028924699103330304&menuType=1&menuNameOrig=流程设计器&moduleCodes=jflow&!moduleCodes=&menuHref=//WF/Admin/Portal/Default.htm&menuTarget=_blank&treeSort=30&permission=&menuIcon=&menuColor=&menuTitle=&isShow=1&weight=40&remarks=&extend.extendS1=&extend.extendS2=&extend.extendS3=&extend.extendS4=&extend.extendS5=&extend.extendS6=&extend.extendS7=&extend.extendS8=&extend.extendI1=&extend.extendI2=&extend.extendI3=&extend.extendI4=&extend.extendF1=&extend.extendF2=&extend.extendF3=&extend.extendF4=&extend.extendD1=&extend.extendD2=&extend.extendD3=&extend.extendD4=', NULL, '1028924891047264256', 'Menu', '0:0:0:0:0:0:0:1', 'http://localhost:8980', '0', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Windows 10', 'Firefox 7', 671, '0', 'JeeSite');
INSERT INTO `js_sys_log` VALUES ('1285469339301228544', 'select', '系统管理/系统设置/菜单管理', 'system', '超级管理员', '2020-07-21 14:59:08', '/js/a/sys/menu/listData', 'POST', 'id=&nodeid=1028924699103330304&parentid=0&n_level=0&_search=false&nd=1595314747634&pageSize=&pageNo=&orderBy=&sord=asc&moduleCodes=&sysCode=default&menuNameOrig=&parentCode=1028924699103330304', NULL, '', 'Menu', '0:0:0:0:0:0:0:1', 'http://localhost:8980', '0', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:78.0) Gecko/20100101 Firefox/78.0', 'Windows 10', 'Firefox 7', 369, '0', 'JeeSite');

-- ----------------------------
-- Table structure for js_sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_menu`;
CREATE TABLE `js_sys_menu`  (
  `menu_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单编码',
  `parent_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10, 0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4, 0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '全节点名',
  `menu_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单类型（1菜单 2权限 3开发）',
  `menu_href` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '链接',
  `menu_target` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目标',
  `menu_icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `menu_color` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '颜色',
  `menu_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单标题',
  `permission` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `weight` decimal(4, 0) NULL DEFAULT NULL COMMENT '菜单权重',
  `is_show` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否显示（1显示 0隐藏）',
  `sys_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '归属系统（default:主导航菜单、mobileApp:APP菜单）',
  `module_codes` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '归属模块（多个用逗号隔开）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `extend_s1` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`menu_code`) USING BTREE,
  INDEX `idx_sys_menu_pc`(`parent_code`) USING BTREE,
  INDEX `idx_sys_menu_ts`(`tree_sort`) USING BTREE,
  INDEX `idx_sys_menu_status`(`status`) USING BTREE,
  INDEX `idx_sys_menu_mt`(`menu_type`) USING BTREE,
  INDEX `idx_sys_menu_sc`(`sys_code`) USING BTREE,
  INDEX `idx_sys_menu_is`(`is_show`) USING BTREE,
  INDEX `idx_sys_menu_wt`(`weight`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_menu
-- ----------------------------
INSERT INTO `js_sys_menu` VALUES ('1028924699103330304', '0', '0,', 9030, '0000009030,', '0', 0, 'JFlow工作流', 'JFlow工作流', '1', '', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028924891047264256', '1028924699103330304', '0,1028924699103330304,', 30, '0000009030,0000000030,', '1', 1, 'JFlow工作流/流程设计器', '流程设计器', '1', '//WF/Admin/Portal/Default.htm', '_blank', '', '', '', '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:59:07', '', '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028925472004505600', '1028924699103330304', '0,1028924699103330304,', 60, '0000009030,0000000060,', '0', 1, 'JFlow工作流/流程办理', '流程办理', '1', '', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028933723626536960', '1028925472004505600', '0,1028924699103330304,1028925472004505600,', 30, '0000009030,0000000060,0000000030,', '1', 2, 'JFlow工作流/流程办理/发起', '发起', '1', '//WF/Start.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028933804492718080', '1028925472004505600', '0,1028924699103330304,1028925472004505600,', 60, '0000009030,0000000060,0000000060,', '1', 2, 'JFlow工作流/流程办理/待办', '待办', '1', '//WF/Todolist.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028933895135821824', '1028925472004505600', '0,1028924699103330304,1028925472004505600,', 90, '0000009030,0000000060,0000000090,', '1', 2, 'JFlow工作流/流程办理/会签', '会签', '1', '//WF/HuiQianList.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028933988475863040', '1028925472004505600', '0,1028924699103330304,1028925472004505600,', 120, '0000009030,0000000060,0000000120,', '1', 2, 'JFlow工作流/流程办理/在途', '在途', '1', '//WF/Runing.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028934263999692800', '1028924699103330304', '0,1028924699103330304,', 90, '0000009030,0000000090,', '0', 1, 'JFlow工作流/流程查询', '流程查询', '1', '', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028934340843536384', '1028934263999692800', '0,1028924699103330304,1028934263999692800,', 30, '0000009030,0000000090,0000000030,', '1', 2, 'JFlow工作流/流程查询/我发起的', '我发起的', '1', '//WF/Comm/Search.htm?EnsName=BP.WF.Data.MyStartFlows', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028934445013270528', '1028934263999692800', '0,1028924699103330304,1028934263999692800,', 60, '0000009030,0000000090,0000000060,', '1', 2, 'JFlow工作流/流程查询/我审批的', '我审批的', '1', '//WF/Comm/Search.htm?EnsName=BP.WF.Data.MyJoinFlows', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028934560448905216', '1028934263999692800', '0,1028924699103330304,1028934263999692800,', 90, '0000009030,0000000090,0000000090,', '1', 2, 'JFlow工作流/流程查询/流程分布', '流程分布', '1', '//WF/RptSearch/DistributedOfMy.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028934755261743104', '1028934263999692800', '0,1028924699103330304,1028934263999692800,', 120, '0000009030,0000000090,0000000120,', '1', 2, 'JFlow工作流/流程查询/我的流程', '我的流程', '1', '//WF/Search.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028934869279703040', '1028934263999692800', '0,1028924699103330304,1028934263999692800,', 150, '0000009030,0000000090,0000000150,', '1', 2, 'JFlow工作流/流程查询/单流程查询', '单流程查询', '1', '//WF/RptDfine/Flowlist.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028935178097917952', '1028934263999692800', '0,1028924699103330304,1028934263999692800,', 180, '0000009030,0000000090,0000000180,', '1', 2, 'JFlow工作流/流程查询/综合查询', '综合查询', '1', '//WF/RptSearch/Default.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028935301783748608', '1028925472004505600', '0,1028924699103330304,1028925472004505600,', 150, '0000009030,0000000060,0000000150,', '1', 2, 'JFlow工作流/流程办理/共享任务', '共享任务', '1', '//WF/TaskPoolSharing.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028935378346573824', '1028925472004505600', '0,1028924699103330304,1028925472004505600,', 180, '0000009030,0000000060,0000000180,', '1', 2, 'JFlow工作流/流程办理/申请下来的任务', '申请下来的任务', '1', '//WF/TaskPoolApply.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028935550501781504', '1028924699103330304', '0,1028924699103330304,', 120, '0000009030,0000000120,', '0', 1, 'JFlow工作流/高级功能', '高级功能', '1', '', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028935644458385408', '1028935550501781504', '0,1028924699103330304,1028935550501781504,', 30, '0000009030,0000000120,0000000030,', '1', 2, 'JFlow工作流/高级功能/我的草稿', '我的草稿', '1', '//WF/Draft.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:18', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028935717149868032', '1028935550501781504', '0,1028924699103330304,1028935550501781504,', 60, '0000009030,0000000120,0000000060,', '1', 2, 'JFlow工作流/高级功能/抄送', '抄送', '1', '//WF/CC.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:18', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028935775526191104', '1028935550501781504', '0,1028924699103330304,1028935550501781504,', 90, '0000009030,0000000120,0000000090,', '1', 2, 'JFlow工作流/高级功能/我的关注', '我的关注', '1', '//WF/Focus.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:18', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028935931373944832', '1028935550501781504', '0,1028924699103330304,1028935550501781504,', 120, '0000009030,0000000120,0000000120,', '1', 2, 'JFlow工作流/高级功能/授权待办', '授权待办', '1', '//WF/TodolistOfAuth.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:18', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1028936085451702272', '1028935550501781504', '0,1028924699103330304,1028935550501781504,', 150, '0000009030,0000000120,0000000150,', '1', 2, 'JFlow工作流/高级功能/挂起的工作', '挂起的工作', '1', '//WF/HungUpList.htm', '', '', '', NULL, '', 40, '1', 'default', 'jflow', '0', 'system', '2020-07-21 14:54:18', 'system', '2020-07-21 14:55:34', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465604080357376', '0', '0,', 9000, '0000009000,', '0', 0, '系统管理', '系统管理', '1', '', '', 'icon-settings', '', NULL, '', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:18', 'system', '2020-07-21 14:44:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465608316604416', '1285465604080357376', '0,1285465604080357376,', 300, '0000009000,0000000300,', '0', 1, '系统管理/组织管理', '组织管理', '1', '', '', 'icon-grid', '', NULL, '', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:20', 'system', '2020-07-21 14:44:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465615946043392', '1285465608316604416', '0,1285465604080357376,1285465608316604416,', 40, '0000009000,0000000300,0000000040,', '0', 2, '系统管理/组织管理/用户管理', '用户管理', '1', '/sys/empUser/index', '', 'icon-user', '', NULL, '', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:20', 'system', '2020-07-21 14:44:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465619737694208', '1285465615946043392', '0,1285465604080357376,1285465608316604416,1285465615946043392,', 30, '0000009000,0000000300,0000000040,0000000030,', '1', 3, '系统管理/组织管理/用户管理/查看', '查看', '2', '', '', '', '', NULL, 'sys:empUser:view', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:21', 'system', '2020-07-21 14:44:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465622149419008', '1285465615946043392', '0,1285465604080357376,1285465608316604416,1285465615946043392,', 40, '0000009000,0000000300,0000000040,0000000040,', '1', 3, '系统管理/组织管理/用户管理/编辑', '编辑', '2', '', '', '', '', NULL, 'sys:empUser:edit', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:22', 'system', '2020-07-21 14:44:22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465626008178688', '1285465615946043392', '0,1285465604080357376,1285465608316604416,1285465615946043392,', 60, '0000009000,0000000300,0000000040,0000000060,', '1', 3, '系统管理/组织管理/用户管理/分配角色', '分配角色', '2', '', '', '', '', NULL, 'sys:empUser:authRole', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:23', 'system', '2020-07-21 14:44:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465631322361856', '1285465615946043392', '0,1285465604080357376,1285465608316604416,1285465615946043392,', 50, '0000009000,0000000300,0000000040,0000000050,', '1', 3, '系统管理/组织管理/用户管理/分配数据', '分配数据', '2', '', '', '', '', NULL, 'sys:empUser:authDataScope', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:24', 'system', '2020-07-21 14:44:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465636590407680', '1285465615946043392', '0,1285465604080357376,1285465608316604416,1285465615946043392,', 60, '0000009000,0000000300,0000000040,0000000060,', '1', 3, '系统管理/组织管理/用户管理/停用启用', '停用启用', '2', '', '', '', '', NULL, 'sys:empUser:updateStatus', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:25', 'system', '2020-07-21 14:44:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465639002132480', '1285465615946043392', '0,1285465604080357376,1285465608316604416,1285465615946043392,', 70, '0000009000,0000000300,0000000040,0000000070,', '1', 3, '系统管理/组织管理/用户管理/重置密码', '重置密码', '2', '', '', '', '', NULL, 'sys:empUser:resetpwd', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:27', 'system', '2020-07-21 14:44:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465646027591680', '1285465608316604416', '0,1285465604080357376,1285465608316604416,', 50, '0000009000,0000000300,0000000050,', '0', 2, '系统管理/组织管理/机构管理', '机构管理', '1', '/sys/office/index', '', 'icon-grid', '', NULL, '', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:28', 'system', '2020-07-21 14:44:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465649731162112', '1285465646027591680', '0,1285465604080357376,1285465608316604416,1285465646027591680,', 30, '0000009000,0000000300,0000000050,0000000030,', '1', 3, '系统管理/组织管理/机构管理/查看', '查看', '2', '', '', '', '', NULL, 'sys:office:view', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:29', 'system', '2020-07-21 14:44:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465655485747200', '1285465646027591680', '0,1285465604080357376,1285465608316604416,1285465646027591680,', 40, '0000009000,0000000300,0000000050,0000000040,', '1', 3, '系统管理/组织管理/机构管理/编辑', '编辑', '2', '', '', '', '', NULL, 'sys:office:edit', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:30', 'system', '2020-07-21 14:44:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465659742965760', '1285465608316604416', '0,1285465604080357376,1285465608316604416,', 70, '0000009000,0000000300,0000000070,', '0', 2, '系统管理/组织管理/公司管理', '公司管理', '1', '/sys/company/list', '', 'icon-fire', '', NULL, '', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:32', 'system', '2020-07-21 14:44:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465668139962368', '1285465659742965760', '0,1285465604080357376,1285465608316604416,1285465659742965760,', 30, '0000009000,0000000300,0000000070,0000000030,', '1', 3, '系统管理/组织管理/公司管理/查看', '查看', '2', '', '', '', '', NULL, 'sys:company:view', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:33', 'system', '2020-07-21 14:44:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465674691465216', '1285465659742965760', '0,1285465604080357376,1285465608316604416,1285465659742965760,', 40, '0000009000,0000000300,0000000070,0000000040,', '1', 3, '系统管理/组织管理/公司管理/编辑', '编辑', '2', '', '', '', '', NULL, 'sys:company:edit', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:34', 'system', '2020-07-21 14:44:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465677069635584', '1285465608316604416', '0,1285465604080357376,1285465608316604416,', 70, '0000009000,0000000300,0000000070,', '0', 2, '系统管理/组织管理/岗位管理', '岗位管理', '1', '/sys/post/list', '', 'icon-trophy', '', NULL, '', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:36', 'system', '2020-07-21 14:44:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465682606116864', '1285465677069635584', '0,1285465604080357376,1285465608316604416,1285465677069635584,', 30, '0000009000,0000000300,0000000070,0000000030,', '1', 3, '系统管理/组织管理/岗位管理/查看', '查看', '2', '', '', '', '', NULL, 'sys:post:view', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:36', 'system', '2020-07-21 14:44:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465686250967040', '1285465677069635584', '0,1285465604080357376,1285465608316604416,1285465677069635584,', 40, '0000009000,0000000300,0000000070,0000000040,', '1', 3, '系统管理/组织管理/岗位管理/编辑', '编辑', '2', '', '', '', '', NULL, 'sys:post:edit', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:37', 'system', '2020-07-21 14:44:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465690034229248', '1285465604080357376', '0,1285465604080357376,', 400, '0000009000,0000000400,', '0', 1, '系统管理/权限管理', '权限管理', '1', '', '', 'icon-social-dropbox', '', NULL, '', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:38', 'system', '2020-07-21 14:44:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465696736727040', '1285465690034229248', '0,1285465604080357376,1285465690034229248,', 30, '0000009000,0000000400,0000000030,', '1', 2, '系统管理/权限管理/角色管理', '角色管理', '1', '/sys/role/list', '', 'icon-people', '', NULL, 'sys:role', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:40', 'system', '2020-07-21 14:44:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465699106508800', '1285465690034229248', '0,1285465604080357376,1285465690034229248,', 40, '0000009000,0000000400,0000000040,', '1', 2, '系统管理/权限管理/二级管理员', '二级管理员', '1', '/sys/secAdmin/list', '', 'icon-user-female', '', NULL, 'sys:secAdmin', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:41', 'system', '2020-07-21 14:44:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465707356704768', '1285465690034229248', '0,1285465604080357376,1285465690034229248,', 50, '0000009000,0000000400,0000000050,', '1', 2, '系统管理/权限管理/系统管理员', '系统管理员', '1', '/sys/corpAdmin/list', '', 'icon-badge', '', NULL, 'sys:corpAdmin', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:43', 'system', '2020-07-21 14:44:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465714222780416', '1285465604080357376', '0,1285465604080357376,', 500, '0000009000,0000000500,', '0', 1, '系统管理/系统设置', '系统设置', '1', '', '', 'icon-settings', '', NULL, '', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:45', 'system', '2020-07-21 14:44:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465724402356224', '1285465714222780416', '0,1285465604080357376,1285465714222780416,', 30, '0000009000,0000000500,0000000030,', '1', 2, '系统管理/系统设置/菜单管理', '菜单管理', '1', '/sys/menu/list', '', 'icon-book-open', '', NULL, 'sys:menu', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:47', 'system', '2020-07-21 14:44:47', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465728626020352', '1285465714222780416', '0,1285465604080357376,1285465714222780416,', 40, '0000009000,0000000500,0000000040,', '1', 2, '系统管理/系统设置/模块管理', '模块管理', '1', '/sys/module/list', '', 'icon-grid', '', NULL, 'sys:module', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:48', 'system', '2020-07-21 14:44:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465732296036352', '1285465714222780416', '0,1285465604080357376,1285465714222780416,', 50, '0000009000,0000000500,0000000050,', '1', 2, '系统管理/系统设置/参数设置', '参数设置', '1', '/sys/config/list', '', 'icon-wrench', '', NULL, 'sys:config', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:49', 'system', '2020-07-21 14:44:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465737547304960', '1285465714222780416', '0,1285465604080357376,1285465714222780416,', 60, '0000009000,0000000500,0000000060,', '0', 2, '系统管理/系统设置/字典管理', '字典管理', '1', '/sys/dictType/list', '', 'icon-social-dropbox', '', NULL, '', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:50', 'system', '2020-07-21 14:44:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465742945374208', '1285465737547304960', '0,1285465604080357376,1285465714222780416,1285465737547304960,', 30, '0000009000,0000000500,0000000060,0000000030,', '1', 3, '系统管理/系统设置/字典管理/类型查看', '类型查看', '2', '', '', '', '', NULL, 'sys:dictType:view', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:51', 'system', '2020-07-21 14:44:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465745193521152', '1285465737547304960', '0,1285465604080357376,1285465714222780416,1285465737547304960,', 40, '0000009000,0000000500,0000000060,0000000040,', '1', 3, '系统管理/系统设置/字典管理/类型编辑', '类型编辑', '2', '', '', '', '', NULL, 'sys:dictType:edit', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:53', 'system', '2020-07-21 14:44:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465755876413440', '1285465737547304960', '0,1285465604080357376,1285465714222780416,1285465737547304960,', 50, '0000009000,0000000500,0000000060,0000000050,', '1', 3, '系统管理/系统设置/字典管理/数据查看', '数据查看', '2', '', '', '', '', NULL, 'sys:dictData:view', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:54', 'system', '2020-07-21 14:44:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465760053940224', '1285465737547304960', '0,1285465604080357376,1285465714222780416,1285465737547304960,', 60, '0000009000,0000000500,0000000060,0000000060,', '1', 3, '系统管理/系统设置/字典管理/数据编辑', '数据编辑', '2', '', '', '', '', NULL, 'sys:dictData:edit', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:44:59', 'system', '2020-07-21 14:44:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465778584375296', '1285465714222780416', '0,1285465604080357376,1285465714222780416,', 70, '0000009000,0000000500,0000000070,', '1', 2, '系统管理/系统设置/行政区划', '行政区划', '1', '/sys/area/list', '', 'icon-map', '', NULL, 'sys:area', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:00', 'system', '2020-07-21 14:45:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465782891925504', '1285465714222780416', '0,1285465604080357376,1285465714222780416,', 80, '0000009000,0000000500,0000000080,', '1', 2, '系统管理/系统设置/国际化管理', '国际化管理', '1', '/sys/lang/list', '', 'icon-globe', '', NULL, 'sys:lang', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:01', 'system', '2020-07-21 14:45:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465788357103616', '1285465714222780416', '0,1285465604080357376,1285465714222780416,', 90, '0000009000,0000000500,0000000090,', '1', 2, '系统管理/系统设置/产品许可信息', '产品许可信息', '1', '//licence', '', 'icon-paper-plane', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:02', 'system', '2020-07-21 14:45:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465795311259648', '1285465604080357376', '0,1285465604080357376,', 600, '0000009000,0000000600,', '0', 1, '系统管理/系统监控', '系统监控', '1', '', '', 'icon-ghost', '', NULL, '', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:06', 'system', '2020-07-21 14:45:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465812352716800', '1285465795311259648', '0,1285465604080357376,1285465795311259648,', 40, '0000009000,0000000600,0000000040,', '1', 2, '系统管理/系统监控/访问日志', '访问日志', '1', '/sys/log/list', '', 'fa fa-bug', '', NULL, 'sys:log', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:08', 'system', '2020-07-21 14:45:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465817889198080', '1285465795311259648', '0,1285465604080357376,1285465795311259648,', 50, '0000009000,0000000600,0000000050,', '1', 2, '系统管理/系统监控/数据监控', '数据监控', '1', '//druid/index.html', '', 'icon-disc', '', NULL, 'sys:state:druid', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:10', 'system', '2020-07-21 14:45:10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465828655976448', '1285465795311259648', '0,1285465604080357376,1285465795311259648,', 60, '0000009000,0000000600,0000000060,', '1', 2, '系统管理/系统监控/缓存监控', '缓存监控', '1', '/state/cache/index', '', 'icon-social-dribbble', '', NULL, 'sys:stste:cache', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:11', 'system', '2020-07-21 14:45:11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465831264833536', '1285465795311259648', '0,1285465604080357376,1285465795311259648,', 70, '0000009000,0000000600,0000000070,', '1', 2, '系统管理/系统监控/服务器监控', '服务器监控', '1', '/state/server/index', '', 'icon-speedometer', '', NULL, 'sys:state:server', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:12', 'system', '2020-07-21 14:45:12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465835006152704', '1285465795311259648', '0,1285465604080357376,1285465795311259648,', 80, '0000009000,0000000600,0000000080,', '1', 2, '系统管理/系统监控/作业监控', '作业监控', '1', '/job/list', '', 'icon-notebook', '', NULL, 'sys:job', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:13', 'system', '2020-07-21 14:45:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465842262298624', '1285465795311259648', '0,1285465604080357376,1285465795311259648,', 90, '0000009000,0000000600,0000000090,', '1', 2, '系统管理/系统监控/在线用户', '在线用户', '1', '/sys/online/list', '', 'icon-social-twitter', '', NULL, 'sys:online', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:15', 'system', '2020-07-21 14:45:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465848000106496', '1285465795311259648', '0,1285465604080357376,1285465795311259648,', 100, '0000009000,0000000600,0000000100,', '1', 2, '系统管理/系统监控/在线文档', '在线文档', '1', '//swagger-ui.html', '', 'icon-book-open', '', NULL, 'sys:swagger', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:16', 'system', '2020-07-21 14:45:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465851955335168', '1285465604080357376', '0,1285465604080357376,', 700, '0000009000,0000000700,', '0', 1, '系统管理/消息推送', '消息推送', '1', '', '', 'icon-envelope-letter', '', NULL, '', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:17', 'system', '2020-07-21 14:45:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465857634422784', '1285465851955335168', '0,1285465604080357376,1285465851955335168,', 30, '0000009000,0000000700,0000000030,', '1', 2, '系统管理/消息推送/未完成消息', '未完成消息', '1', '/msg/msgPush/list', '', '', '', NULL, 'msg:msgPush', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:18', 'system', '2020-07-21 14:45:18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465860083896320', '1285465851955335168', '0,1285465604080357376,1285465851955335168,', 40, '0000009000,0000000700,0000000040,', '1', 2, '系统管理/消息推送/已完成消息', '已完成消息', '1', '/msg/msgPush/list?pushed=true', '', '', '', NULL, 'msg:msgPush', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:20', 'system', '2020-07-21 14:45:20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465870141837312', '1285465851955335168', '0,1285465604080357376,1285465851955335168,', 50, '0000009000,0000000700,0000000050,', '1', 2, '系统管理/消息推送/消息模板管理', '消息模板管理', '1', '/msg/msgTemplate/list', '', '', '', NULL, 'msg:msgTemplate', 60, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:23', 'system', '2020-07-21 14:45:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465882536005632', '1285465604080357376', '0,1285465604080357376,', 900, '0000009000,0000000900,', '0', 1, '系统管理/研发工具', '研发工具', '1', '', '', 'fa fa-code', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:24', 'system', '2020-07-21 14:45:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465884788346880', '1285465882536005632', '0,1285465604080357376,1285465882536005632,', 30, '0000009000,0000000900,0000000030,', '1', 2, '系统管理/研发工具/代码生成工具', '代码生成工具', '1', '/gen/genTable/list', '', 'fa fa-code', '', NULL, 'gen:genTable', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:25', 'system', '2020-07-21 14:45:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465888881987584', '1285465882536005632', '0,1285465604080357376,1285465882536005632,', 100, '0000009000,0000000900,0000000100,', '0', 2, '系统管理/研发工具/代码生成实例', '代码生成实例', '1', '', '', 'icon-social-dropbox', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:27', 'system', '2020-07-21 14:45:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465898646327296', '1285465888881987584', '0,1285465604080357376,1285465882536005632,1285465888881987584,', 30, '0000009000,0000000900,0000000100,0000000030,', '1', 3, '系统管理/研发工具/代码生成实例/单表_主子表', '单表/主子表', '1', '/test/testData/list', '', '', '', NULL, 'test:testData', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:28', 'system', '2020-07-21 14:45:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465900990943232', '1285465888881987584', '0,1285465604080357376,1285465882536005632,1285465888881987584,', 40, '0000009000,0000000900,0000000100,0000000040,', '1', 3, '系统管理/研发工具/代码生成实例/树表_树结构表', '树表/树结构表', '1', '/test/testTree/list', '', '', '', NULL, 'test:testTree', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:29', 'system', '2020-07-21 14:45:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465912672079872', '1285465882536005632', '0,1285465604080357376,1285465882536005632,', 200, '0000009000,0000000900,0000000200,', '0', 2, '系统管理/研发工具/数据表格实例', '数据表格实例', '1', '', '', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:33', 'system', '2020-07-21 14:45:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465921010356224', '1285465912672079872', '0,1285465604080357376,1285465882536005632,1285465912672079872,', 30, '0000009000,0000000900,0000000200,0000000030,', '1', 3, '系统管理/研发工具/数据表格实例/多表头分组小计合计', '多表头分组小计合计', '1', '/demo/dataGrid/groupGrid', '', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:33', 'system', '2020-07-21 14:45:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465923455635456', '1285465912672079872', '0,1285465604080357376,1285465882536005632,1285465912672079872,', 40, '0000009000,0000000900,0000000200,0000000040,', '1', 3, '系统管理/研发工具/数据表格实例/编辑表格多行编辑', '编辑表格多行编辑', '1', '/demo/dataGrid/editGrid', '', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:34', 'system', '2020-07-21 14:45:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465927142428672', '1285465912672079872', '0,1285465604080357376,1285465882536005632,1285465912672079872,', 50, '0000009000,0000000900,0000000200,0000000050,', '1', 3, '系统管理/研发工具/数据表格实例/多表联动示例', '多表联动示例', '1', '/demo/dataGrid/multiGrid', '', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:35', 'system', '2020-07-21 14:45:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465933077368832', '1285465882536005632', '0,1285465604080357376,1285465882536005632,', 300, '0000009000,0000000900,0000000300,', '0', 2, '系统管理/研发工具/表单组件实例', '表单组件实例', '1', '', '', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:36', 'system', '2020-07-21 14:45:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465935409401856', '1285465933077368832', '0,1285465604080357376,1285465882536005632,1285465933077368832,', 30, '0000009000,0000000900,0000000300,0000000030,', '1', 3, '系统管理/研发工具/表单组件实例/组件应用实例', '组件应用实例', '1', '/demo/form/editForm', '', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:38', 'system', '2020-07-21 14:45:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465942258700288', '1285465933077368832', '0,1285465604080357376,1285465882536005632,1285465933077368832,', 40, '0000009000,0000000900,0000000300,0000000040,', '1', 3, '系统管理/研发工具/表单组件实例/栅格布局实例', '栅格布局实例', '1', '/demo/form/layoutForm', '', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:39', 'system', '2020-07-21 14:45:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465947384139776', '1285465933077368832', '0,1285465604080357376,1285465882536005632,1285465933077368832,', 50, '0000009000,0000000900,0000000300,0000000050,', '1', 3, '系统管理/研发工具/表单组件实例/表格表单实例', '表格表单实例', '1', '/demo/form/tableForm', '', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:39', 'system', '2020-07-21 14:45:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465951104487424', '1285465933077368832', '0,1285465604080357376,1285465882536005632,1285465933077368832,', 60, '0000009000,0000000900,0000000300,0000000060,', '1', 3, '系统管理/研发工具/表单组件实例/多页签应用示例', '多页签应用示例', '1', '/demo/form/tabPage', '', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:41', 'system', '2020-07-21 14:45:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465956183789568', '1285465882536005632', '0,1285465604080357376,1285465882536005632,', 400, '0000009000,0000000900,0000000400,', '0', 2, '系统管理/研发工具/前端界面实例', '前端界面实例', '1', '', '', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:43', 'system', '2020-07-21 14:45:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465964396236800', '1285465956183789568', '0,1285465604080357376,1285465882536005632,1285465956183789568,', 30, '0000009000,0000000900,0000000400,0000000030,', '1', 3, '系统管理/研发工具/前端界面实例/图标样式查找', '图标样式查找', '1', '//tags/iconselect', '', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:43', 'system', '2020-07-21 14:45:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465966652772352', '1285465604080357376', '0,1285465604080357376,', 999, '0000009000,0000000999,', '0', 1, '系统管理/JeeSite社区', 'JeeSite社区', '1', '', '', 'icon-directions', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:44', 'system', '2020-07-21 14:45:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465970322788352', '1285465966652772352', '0,1285465604080357376,1285465966652772352,', 30, '0000009000,0000000999,0000000030,', '1', 2, '系统管理/JeeSite社区/官方网站', '官方网站', '1', 'http://jeesite.com', '_blank', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:45', 'system', '2020-07-21 14:45:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465974001192960', '1285465966652772352', '0,1285465604080357376,1285465966652772352,', 50, '0000009000,0000000999,0000000050,', '1', 2, '系统管理/JeeSite社区/作者博客', '作者博客', '1', 'https://my.oschina.net/thinkgem', '_blank', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:46', 'system', '2020-07-21 14:45:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465976303865856', '1285465966652772352', '0,1285465604080357376,1285465966652772352,', 40, '0000009000,0000000999,0000000040,', '1', 2, '系统管理/JeeSite社区/问题反馈', '问题反馈', '1', 'https://gitee.com/thinkgem/jeesite4/issues', '_blank', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:46', 'system', '2020-07-21 14:45:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465978577178624', '1285465966652772352', '0,1285465604080357376,1285465966652772352,', 60, '0000009000,0000000999,0000000060,', '1', 2, '系统管理/JeeSite社区/开源社区', '开源社区', '1', 'http://jeesite.net', '_blank', '', '', NULL, '', 80, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:48', 'system', '2020-07-21 14:45:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465984637947904', '0', '0,', 9060, '0000009060,', '0', 0, '我的工作', '我的工作', '1', '', '', 'icon-screen-desktop', '', NULL, '', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:49', 'system', '2020-07-21 14:45:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465989683695616', '1285465984637947904', '0,1285465984637947904,', 500, '0000009060,0000000500,', '0', 1, '我的工作/文件管理', '文件管理', '1', '', '', 'icon-folder', '', NULL, '', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:50', 'system', '2020-07-21 14:45:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285465993903165440', '1285465989683695616', '0,1285465984637947904,1285465989683695616,', 30, '0000009060,0000000500,0000000030,', '1', 2, '我的工作/文件管理/文件管理', '文件管理', '1', '/filemanager/index', '', '', '', NULL, 'filemanager', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:51', 'system', '2020-07-21 14:45:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466001587130368', '1285465989683695616', '0,1285465984637947904,1285465989683695616,', 50, '0000009060,0000000500,0000000050,', '1', 2, '我的工作/文件管理/文件分享', '文件分享', '1', '/filemanager/filemanagerShared/list', '', '', '', NULL, 'filemanager', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:53', 'system', '2020-07-21 14:45:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466008331571200', '1285465984637947904', '0,1285465984637947904,', 600, '0000009060,0000000600,', '0', 1, '我的工作/站内消息', '站内消息', '1', '/msg/msgInner/list', '', 'icon-speech', '', NULL, '', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:54', 'system', '2020-07-21 14:45:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466012005781504', '1285466008331571200', '0,1285465984637947904,1285466008331571200,', 30, '0000009060,0000000600,0000000030,', '1', 2, '我的工作/站内消息/查看', '查看', '2', '', '', '', '', NULL, 'msg:msgInner:view', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:55', 'system', '2020-07-21 14:45:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466017156386816', '1285466008331571200', '0,1285465984637947904,1285466008331571200,', 40, '0000009060,0000000600,0000000040,', '1', 2, '我的工作/站内消息/编辑', '编辑', '2', '', '', '', '', NULL, 'msg:msgInner:edit', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:56', 'system', '2020-07-21 14:45:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466020918677504', '1285466008331571200', '0,1285465984637947904,1285466008331571200,', 50, '0000009060,0000000600,0000000050,', '1', 2, '我的工作/站内消息/审核', '审核', '2', '', '', '', '', NULL, 'msg:msgInner:auth', 40, '1', 'default', 'core', '0', 'system', '2020-07-21 14:45:57', 'system', '2020-07-21 14:45:57', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466023238127616', '0', '0,', 9090, '0000009090,', '0', 0, '业务流程', '业务流程', '1', '', '', 'icon-social-foursqare', '', NULL, '', 40, '1', 'default', 'bpm', '2', 'system', '2020-07-21 14:45:58', 'system', '2020-07-21 14:55:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466027042361344', '1285466023238127616', '0,1285466023238127616,', 90, '0000009090,0000000090,', '0', 1, '业务流程/流程办理', '流程办理', '1', '', '', 'icon-control-forward', '', NULL, '', 40, '1', 'default', 'bpm', '2', 'system', '2020-07-21 14:45:59', 'system', '2020-07-21 14:55:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466033640001536', '1285466027042361344', '0,1285466023238127616,1285466027042361344,', 30, '0000009090,0000000090,0000000030,', '1', 2, '业务流程/流程办理/待办任务', '待办任务', '1', '/bpm/bpmMyTask/todoList', '', '', '', NULL, '', 40, '1', 'default', 'bpm', '2', 'system', '2020-07-21 14:46:01', 'system', '2020-07-21 14:55:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466041202331648', '1285466027042361344', '0,1285466023238127616,1285466027042361344,', 60, '0000009090,0000000090,0000000060,', '1', 2, '业务流程/流程办理/已办任务', '已办任务', '1', '/bpm/bpmMyTask/historyList', '', '', '', NULL, '', 40, '1', 'default', 'bpm', '2', 'system', '2020-07-21 14:46:03', 'system', '2020-07-21 14:55:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466048970182656', '1285466027042361344', '0,1285466023238127616,1285466027042361344,', 90, '0000009090,0000000090,0000000090,', '1', 2, '业务流程/流程办理/与我相关流程', '与我相关流程', '1', '/bpm/bpmMyRuntime/list', '', '', '', NULL, '', 40, '1', 'default', 'bpm', '2', 'system', '2020-07-21 14:46:05', 'system', '2020-07-21 14:55:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466055265832960', '1285466027042361344', '0,1285466023238127616,1285466027042361344,', 120, '0000009090,0000000090,0000000120,', '1', 2, '业务流程/流程办理/请假单管理(示例)', '请假单管理(示例)', '1', '/oa/oaLeave/list', '', '', '', NULL, 'oa:oaLeave', 40, '1', 'default', 'bpm', '2', 'system', '2020-07-21 14:46:06', 'system', '2020-07-21 14:55:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466064560410624', '1285466023238127616', '0,1285466023238127616,', 150, '0000009090,0000000150,', '0', 1, '业务流程/流程管控', '流程管控', '1', '', '', 'icon-paypal', '', NULL, '', 40, '1', 'default', 'bpm', '2', 'system', '2020-07-21 14:46:08', 'system', '2020-07-21 14:55:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466071481012224', '1285466064560410624', '0,1285466023238127616,1285466064560410624,', 60, '0000009090,0000000150,0000000060,', '1', 2, '业务流程/流程管控/流程模型设计', '流程模型设计', '1', '//bpm/modeler/index.html', '', '', '', NULL, 'bpm:modeler', 40, '1', 'default', 'bpm', '2', 'system', '2020-07-21 14:46:09', 'system', '2020-07-21 14:55:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466073716576256', '1285466064560410624', '0,1285466023238127616,1285466064560410624,', 90, '0000009090,0000000150,0000000090,', '1', 2, '业务流程/流程管控/流程定义管理', '流程定义管理', '1', '/bpm/bpmCategory/index/process', '', '', '', NULL, 'bpm:bpmProcess', 40, '1', 'default', 'bpm', '2', 'system', '2020-07-21 14:46:10', 'system', '2020-07-21 14:55:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466079957700608', '1285466064560410624', '0,1285466023238127616,1285466064560410624,', 30, '0000009090,0000000150,0000000030,', '1', 2, '业务流程/流程管控/流程分类管理', '流程分类管理', '1', '/bpm/bpmCategory/list', '', '', '', NULL, 'bpm:bpmCategory', 40, '1', 'default', 'bpm', '2', 'system', '2020-07-21 14:46:11', 'system', '2020-07-21 14:55:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466083824848896', '1285466064560410624', '0,1285466023238127616,1285466064560410624,', 120, '0000009090,0000000150,0000000120,', '1', 2, '业务流程/流程管控/流程实例管理', '流程实例管理', '1', '/bpm/bpmRuntime/list', '', '', '', NULL, 'bpm:bpmRuntime', 40, '1', 'default', 'bpm', '2', 'system', '2020-07-21 14:46:12', 'system', '2020-07-21 14:55:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466087360647168', '1285466064560410624', '0,1285466023238127616,1285466064560410624,', 150, '0000009090,0000000150,0000000150,', '1', 2, '业务流程/流程管控/流程任务管理', '流程任务管理', '1', '/bpm/bpmTask/list', '', '', '', NULL, 'bpm:bpmTask', 40, '1', 'default', 'bpm', '2', 'system', '2020-07-21 14:46:13', 'system', '2020-07-21 14:55:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_menu` VALUES ('1285466090082750464', '1285466064560410624', '0,1285466023238127616,1285466064560410624,', 180, '0000009090,0000000150,0000000180,', '1', 2, '业务流程/流程管控/流程脚本管理', '流程脚本管理', '1', '/bpm/bpmScript/list', '', '', '', NULL, 'bpm:bpmScript', 40, '1', 'default', 'bpm', '2', 'system', '2020-07-21 14:46:14', 'system', '2020-07-21 14:55:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for js_sys_module
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_module`;
CREATE TABLE `js_sys_module`  (
  `module_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模块编码',
  `module_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模块名称',
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块描述',
  `main_class_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主类全名',
  `current_version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当前版本',
  `upgrade_info` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '升级信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`module_code`) USING BTREE,
  INDEX `idx_sys_module_status`(`status`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '模块表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_module
-- ----------------------------
INSERT INTO `js_sys_module` VALUES ('bpm', '业务流程', '流程设计器、流程监管控制、流程办理、流程追踪', 'com.jeesite.modules.bpm.entity.BpmEntity', '4.1.8', NULL, '0', 'system', '2020-07-21 14:42:00', 'system', '2020-07-21 14:42:00', NULL);
INSERT INTO `js_sys_module` VALUES ('cms', '内容管理', '网站、站点、栏目、文章、链接、评论、留言板', 'com.jeesite.modules.cms.web.CmsController', '4.0.0', NULL, '0', 'system', '2020-07-21 14:41:58', 'system', '2020-07-21 14:41:58', NULL);
INSERT INTO `js_sys_module` VALUES ('core', '核心模块', '用户、角色、组织、模块、菜单、字典、参数', 'com.jeesite.modules.sys.web.LoginController', '4.1.9', 'upgrade 2020-07-21 14:55:31 (4.1.8 -> 4.1.9)', '0', 'system', '2020-07-21 14:41:58', 'system', '2020-07-21 14:55:32', NULL);
INSERT INTO `js_sys_module` VALUES ('filemanager', '文件管理', '公共文件柜、个人文件柜、文件分享', 'com.jeesite.modules.filemanager.web.FilemanagerController', '4.1.6', NULL, '0', 'system', '2020-07-21 14:41:59', 'system', '2020-07-21 14:41:59', NULL);
INSERT INTO `js_sys_module` VALUES ('jflow', 'JFlow工作流', '适合OA、可视化的java工作流引擎、自定义表单引擎。', 'com.jeesite.modules.jflow.config.JflowConfig', '4.1.9', 'upgrade 2020-07-21 14:55:33 (4.1.8 -> 4.1.9)', '0', 'system', '2020-07-21 14:54:17', 'system', '2020-07-21 14:55:33', NULL);

-- ----------------------------
-- Table structure for js_sys_msg_inner
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_msg_inner`;
CREATE TABLE `js_sys_msg_inner`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `msg_title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息标题',
  `content_level` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容级别（1普通 2一般 3紧急）',
  `content_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '内容类型（1公告 2新闻 3会议 4其它）',
  `msg_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息内容',
  `receive_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受者类型（0全部 1用户 2部门 3角色 4岗位）',
  `receive_codes` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '接受者字符串',
  `receive_names` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '接受者名称字符串',
  `send_user_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送者用户编码',
  `send_user_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送者用户姓名',
  `send_date` datetime(0) NULL DEFAULT NULL COMMENT '发送时间',
  `is_attac` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否有附件',
  `notify_types` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '通知类型（PC APP 短信 邮件 微信）多选',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1删除 4审核 5驳回 9草稿）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_msg_inner_cb`(`create_by`) USING BTREE,
  INDEX `idx_sys_msg_inner_status`(`status`) USING BTREE,
  INDEX `idx_sys_msg_inner_cl`(`content_level`) USING BTREE,
  INDEX `idx_sys_msg_inner_sc`(`send_user_code`) USING BTREE,
  INDEX `idx_sys_msg_inner_sd`(`send_date`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '内部消息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_msg_inner_record
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_msg_inner_record`;
CREATE TABLE `js_sys_msg_inner_record`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `msg_inner_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所属消息',
  `receive_user_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受者用户姓名',
  `read_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '读取状态（0未送达 1已读 2未读）',
  `read_date` datetime(0) NULL DEFAULT NULL COMMENT '阅读时间',
  `is_star` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否标星',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_msg_inner_r_mi`(`msg_inner_id`) USING BTREE,
  INDEX `idx_sys_msg_inner_r_ruc`(`receive_user_code`) USING BTREE,
  INDEX `idx_sys_msg_inner_r_stat`(`read_status`) USING BTREE,
  INDEX `idx_sys_msg_inner_r_star`(`is_star`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '内部消息发送记录表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_msg_push
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_msg_push`;
CREATE TABLE `js_sys_msg_push`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `msg_type` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息类型（PC APP 短信 邮件 微信）',
  `msg_title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息标题',
  `msg_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息内容',
  `biz_key` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务类型',
  `receive_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受者账号',
  `receive_user_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受者用户姓名',
  `send_user_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送者用户编码',
  `send_user_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送者用户姓名',
  `send_date` datetime(0) NOT NULL COMMENT '发送时间',
  `is_merge_push` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否合并推送',
  `plan_push_date` datetime(0) NULL DEFAULT NULL COMMENT '计划推送时间',
  `push_number` int(11) NULL DEFAULT NULL COMMENT '推送尝试次数',
  `push_return_code` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推送返回结果码',
  `push_return_msg_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推送返回消息编号',
  `push_return_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '推送返回的内容信息',
  `push_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推送状态（0未推送 1成功  2失败）',
  `push_date` datetime(0) NULL DEFAULT NULL COMMENT '推送时间',
  `read_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '读取状态（0未送达 1已读 2未读）',
  `read_date` datetime(0) NULL DEFAULT NULL COMMENT '读取时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_msg_push_type`(`msg_type`) USING BTREE,
  INDEX `idx_sys_msg_push_rc`(`receive_code`) USING BTREE,
  INDEX `idx_sys_msg_push_uc`(`receive_user_code`) USING BTREE,
  INDEX `idx_sys_msg_push_suc`(`send_user_code`) USING BTREE,
  INDEX `idx_sys_msg_push_pd`(`plan_push_date`) USING BTREE,
  INDEX `idx_sys_msg_push_ps`(`push_status`) USING BTREE,
  INDEX `idx_sys_msg_push_rs`(`read_status`) USING BTREE,
  INDEX `idx_sys_msg_push_bk`(`biz_key`) USING BTREE,
  INDEX `idx_sys_msg_push_bt`(`biz_type`) USING BTREE,
  INDEX `idx_sys_msg_push_imp`(`is_merge_push`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息推送表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_msg_pushed
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_msg_pushed`;
CREATE TABLE `js_sys_msg_pushed`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `msg_type` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息类型（PC APP 短信 邮件 微信）',
  `msg_title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息标题',
  `msg_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息内容',
  `biz_key` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务主键',
  `biz_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务类型',
  `receive_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受者账号',
  `receive_user_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受者用户编码',
  `receive_user_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受者用户姓名',
  `send_user_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送者用户编码',
  `send_user_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送者用户姓名',
  `send_date` datetime(0) NOT NULL COMMENT '发送时间',
  `is_merge_push` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否合并推送',
  `plan_push_date` datetime(0) NULL DEFAULT NULL COMMENT '计划推送时间',
  `push_number` int(11) NULL DEFAULT NULL COMMENT '推送尝试次数',
  `push_return_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '推送返回的内容信息',
  `push_return_code` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推送返回结果码',
  `push_return_msg_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推送返回消息编号',
  `push_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '推送状态（0未推送 1成功  2失败）',
  `push_date` datetime(0) NULL DEFAULT NULL COMMENT '推送时间',
  `read_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '读取状态（0未送达 1已读 2未读）',
  `read_date` datetime(0) NULL DEFAULT NULL COMMENT '读取时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_msg_pushed_type`(`msg_type`) USING BTREE,
  INDEX `idx_sys_msg_pushed_rc`(`receive_code`) USING BTREE,
  INDEX `idx_sys_msg_pushed_uc`(`receive_user_code`) USING BTREE,
  INDEX `idx_sys_msg_pushed_suc`(`send_user_code`) USING BTREE,
  INDEX `idx_sys_msg_pushed_pd`(`plan_push_date`) USING BTREE,
  INDEX `idx_sys_msg_pushed_ps`(`push_status`) USING BTREE,
  INDEX `idx_sys_msg_pushed_rs`(`read_status`) USING BTREE,
  INDEX `idx_sys_msg_pushed_bk`(`biz_key`) USING BTREE,
  INDEX `idx_sys_msg_pushed_bt`(`biz_type`) USING BTREE,
  INDEX `idx_sys_msg_pushed_imp`(`is_merge_push`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息已推送表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_msg_template
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_msg_template`;
CREATE TABLE `js_sys_msg_template`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `module_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属模块',
  `tpl_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模板键值',
  `tpl_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模板名称',
  `tpl_type` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模板类型',
  `tpl_content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '模板内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_sys_msg_tpl_key`(`tpl_key`) USING BTREE,
  INDEX `idx_sys_msg_tpl_type`(`tpl_type`) USING BTREE,
  INDEX `idx_sys_msg_tpl_status`(`status`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息模板' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_office
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_office`;
CREATE TABLE `js_sys_office`  (
  `office_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构编码',
  `parent_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10, 0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4, 0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '全节点名',
  `view_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构代码',
  `office_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构名称',
  `full_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构全称',
  `office_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '机构类型',
  `leader` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '办公电话',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系地址',
  `zip_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮政编码',
  `email` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`office_code`) USING BTREE,
  INDEX `idx_sys_office_cc`(`corp_code`) USING BTREE,
  INDEX `idx_sys_office_pc`(`parent_code`) USING BTREE,
  INDEX `idx_sys_office_status`(`status`) USING BTREE,
  INDEX `idx_sys_office_ot`(`office_type`) USING BTREE,
  INDEX `idx_sys_office_vc`(`view_code`) USING BTREE,
  INDEX `idx_sys_office_ts`(`tree_sort`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组织机构表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_office
-- ----------------------------
INSERT INTO `js_sys_office` VALUES ('SD', '0', '0,', 40, '0000000040,', '0', 0, '山东公司', 'SD', '山东公司', '山东公司', '1', NULL, NULL, NULL, NULL, NULL, '0', 'system', '2020-07-21 14:46:17', 'system', '2020-07-21 14:46:17', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_office` VALUES ('SDJN', 'SD', '0,SD,', 30, '0000000040,0000000030,', '0', 1, '山东公司/济南公司', 'SDJN', '济南公司', '山东济南公司', '2', NULL, NULL, NULL, NULL, NULL, '0', 'system', '2020-07-21 14:46:17', 'system', '2020-07-21 14:46:17', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_office` VALUES ('SDJN01', 'SDJN', '0,SD,SDJN,', 30, '0000000040,0000000030,0000000030,', '1', 2, '山东公司/济南公司/企管部', 'SDJN01', '企管部', '山东济南企管部', '3', NULL, NULL, NULL, NULL, NULL, '0', 'system', '2020-07-21 14:46:18', 'system', '2020-07-21 14:46:18', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_office` VALUES ('SDJN02', 'SDJN', '0,SD,SDJN,', 40, '0000000040,0000000030,0000000040,', '1', 2, '山东公司/济南公司/财务部', 'SDJN02', '财务部', '山东济南财务部', '3', NULL, NULL, NULL, NULL, NULL, '0', 'system', '2020-07-21 14:46:18', 'system', '2020-07-21 14:46:18', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_office` VALUES ('SDJN03', 'SDJN', '0,SD,SDJN,', 50, '0000000040,0000000030,0000000050,', '1', 2, '山东公司/济南公司/研发部', 'SDJN03', '研发部', '山东济南研发部', '3', NULL, NULL, NULL, NULL, NULL, '0', 'system', '2020-07-21 14:46:19', 'system', '2020-07-21 14:46:19', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_office` VALUES ('SDQD', 'SD', '0,SD,', 40, '0000000040,0000000040,', '0', 1, '山东公司/青岛公司', 'SDQD', '青岛公司', '山东青岛公司', '2', NULL, NULL, NULL, NULL, NULL, '0', 'system', '2020-07-21 14:46:19', 'system', '2020-07-21 14:46:19', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_office` VALUES ('SDQD01', 'SDQD', '0,SD,SDQD,', 30, '0000000040,0000000040,0000000030,', '1', 2, '山东公司/青岛公司/企管部', 'SDQD01', '企管部', '山东青岛企管部', '3', NULL, NULL, NULL, NULL, NULL, '0', 'system', '2020-07-21 14:46:20', 'system', '2020-07-21 14:46:20', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_office` VALUES ('SDQD02', 'SDQD', '0,SD,SDQD,', 40, '0000000040,0000000040,0000000040,', '1', 2, '山东公司/青岛公司/财务部', 'SDQD02', '财务部', '山东青岛财务部', '3', NULL, NULL, NULL, NULL, NULL, '0', 'system', '2020-07-21 14:46:20', 'system', '2020-07-21 14:46:20', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_office` VALUES ('SDQD03', 'SDQD', '0,SD,SDQD,', 50, '0000000040,0000000040,0000000050,', '1', 2, '山东公司/青岛公司/研发部', 'SDQD03', '研发部', '山东青岛研发部', '3', NULL, NULL, NULL, NULL, NULL, '0', 'system', '2020-07-21 14:46:20', 'system', '2020-07-21 14:46:20', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for js_sys_post
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_post`;
CREATE TABLE `js_sys_post`  (
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位分类（高管、中层、基层）',
  `post_sort` decimal(10, 0) NULL DEFAULT NULL COMMENT '岗位排序（升序）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  PRIMARY KEY (`post_code`) USING BTREE,
  INDEX `idx_sys_post_cc`(`corp_code`) USING BTREE,
  INDEX `idx_sys_post_status`(`status`) USING BTREE,
  INDEX `idx_sys_post_ps`(`post_sort`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '员工岗位表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_post
-- ----------------------------
INSERT INTO `js_sys_post` VALUES ('ceo', '总经理', '1', 10, '0', 'system', '2020-07-21 14:46:22', 'system', '2020-07-21 14:46:22', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_post` VALUES ('cfo', '财务经理', '2', 20, '0', 'system', '2020-07-21 14:46:23', 'system', '2020-07-21 14:46:23', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_post` VALUES ('dept', '部门经理', '2', 40, '0', 'system', '2020-07-21 14:46:23', 'system', '2020-07-21 14:46:23', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_post` VALUES ('hrm', '人力经理', '2', 30, '0', 'system', '2020-07-21 14:46:23', 'system', '2020-07-21 14:46:23', NULL, '0', 'JeeSite');
INSERT INTO `js_sys_post` VALUES ('user', '普通员工', '3', 50, '0', 'system', '2020-07-21 14:46:23', 'system', '2020-07-21 14:46:23', NULL, '0', 'JeeSite');

-- ----------------------------
-- Table structure for js_sys_role
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_role`;
CREATE TABLE `js_sys_role`  (
  `role_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色编码',
  `role_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色分类（高管、中层、基层、其它）',
  `role_sort` decimal(10, 0) NULL DEFAULT NULL COMMENT '角色排序（升序）',
  `is_sys` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统内置（1是 0否）',
  `user_type` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户类型（employee员工 member会员）',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据范围设置（0未设置  1全部数据 2自定义数据）',
  `biz_scope` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '适应业务范围（不同的功能，不同的数据权限支持）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`role_code`) USING BTREE,
  INDEX `idx_sys_role_cc`(`corp_code`) USING BTREE,
  INDEX `idx_sys_role_is`(`is_sys`) USING BTREE,
  INDEX `idx_sys_role_status`(`status`) USING BTREE,
  INDEX `idx_sys_role_rs`(`role_sort`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_role
-- ----------------------------
INSERT INTO `js_sys_role` VALUES ('corpAdmin', '系统管理员', NULL, 200, '1', 'none', '0', NULL, '0', 'system', '2020-07-21 14:44:17', 'system', '2020-07-21 14:44:17', '客户方使用的管理员角色，客户方管理员，集团管理员', '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_role` VALUES ('default', '默认角色', NULL, 100, '1', 'none', '0', NULL, '0', 'system', '2020-07-21 14:44:17', 'system', '2020-07-21 14:44:17', '非管理员用户，共有的默认角色，在参数配置里指定', '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_role` VALUES ('dept', '部门经理', NULL, 40, '0', 'employee', '0', NULL, '0', 'system', '2020-07-21 14:44:16', 'system', '2020-07-21 14:44:16', '部门经理', '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_role` VALUES ('user', '普通员工', NULL, 50, '0', 'employee', '0', NULL, '0', 'system', '2020-07-21 14:44:16', 'system', '2020-07-21 14:44:16', '普通员工', '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for js_sys_role_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_role_data_scope`;
CREATE TABLE `js_sys_role_data_scope`  (
  `role_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '控制角色编码',
  `ctrl_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '控制类型',
  `ctrl_data` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '控制数据',
  `ctrl_permi` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '控制权限',
  PRIMARY KEY (`role_code`, `ctrl_type`, `ctrl_data`, `ctrl_permi`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色数据权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_role_menu`;
CREATE TABLE `js_sys_role_menu`  (
  `role_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色编码',
  `menu_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单编码',
  PRIMARY KEY (`role_code`, `menu_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色与菜单关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_role_menu
-- ----------------------------
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465604080357376');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465608316604416');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465615946043392');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465619737694208');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465622149419008');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465626008178688');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465631322361856');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465636590407680');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465639002132480');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465646027591680');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465649731162112');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465655485747200');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465659742965760');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465668139962368');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465674691465216');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465677069635584');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465682606116864');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465686250967040');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465690034229248');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465696736727040');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465699106508800');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465707356704768');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465714222780416');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465724402356224');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465728626020352');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465732296036352');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465737547304960');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465742945374208');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465745193521152');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465755876413440');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465760053940224');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465778584375296');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465782891925504');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465788357103616');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465795311259648');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465812352716800');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465817889198080');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465828655976448');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465831264833536');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465835006152704');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465842262298624');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465848000106496');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465851955335168');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465857634422784');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465860083896320');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465870141837312');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465882536005632');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465884788346880');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465888881987584');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465898646327296');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465900990943232');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465912672079872');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465921010356224');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465923455635456');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465927142428672');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465933077368832');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465935409401856');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465942258700288');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465947384139776');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465951104487424');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465956183789568');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465964396236800');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465966652772352');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465970322788352');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465974001192960');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465976303865856');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465978577178624');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465984637947904');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465989683695616');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285465993903165440');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466001587130368');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466008331571200');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466012005781504');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466017156386816');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466020918677504');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466023238127616');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466027042361344');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466033640001536');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466041202331648');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466048970182656');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466055265832960');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466064560410624');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466071481012224');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466073716576256');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466079957700608');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466083824848896');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466087360647168');
INSERT INTO `js_sys_role_menu` VALUES ('corpAdmin', '1285466090082750464');

-- ----------------------------
-- Table structure for js_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_user`;
CREATE TABLE `js_sys_user`  (
  `user_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户编码',
  `login_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录账号',
  `user_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录密码',
  `email` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '办公电话',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户性别',
  `avatar` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像路径',
  `sign` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个性签名',
  `wx_openid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的微信号',
  `mobile_imei` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的手机串号',
  `user_type` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户类型',
  `ref_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户类型引用编号',
  `ref_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户类型引用姓名',
  `mgr_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员类型（0非管理员 1系统管理员  2二级管理员）',
  `pwd_security_level` decimal(1, 0) NULL DEFAULT NULL COMMENT '密码安全级别（0初始 1很弱 2弱 3安全 4很安全）',
  `pwd_update_date` datetime(0) NULL DEFAULT NULL COMMENT '密码最后更新时间',
  `pwd_update_record` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码修改记录',
  `pwd_question` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密保问题',
  `pwd_question_answer` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密保问题答案',
  `pwd_question_2` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密保问题2',
  `pwd_question_answer_2` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密保问题答案2',
  `pwd_question_3` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密保问题3',
  `pwd_question_answer_3` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密保问题答案3',
  `pwd_quest_update_date` datetime(0) NULL DEFAULT NULL COMMENT '密码问题修改时间',
  `last_login_ip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后登陆IP',
  `last_login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登陆时间',
  `freeze_date` datetime(0) NULL DEFAULT NULL COMMENT '冻结时间',
  `freeze_cause` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '冻结原因',
  `user_weight` decimal(8, 0) NULL DEFAULT 0 COMMENT '用户权重（降序）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1删除 2停用 3冻结）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `corp_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '租户代码',
  `corp_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'JeeSite' COMMENT '租户名称',
  `extend_s1` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 1',
  `extend_s2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 2',
  `extend_s3` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 3',
  `extend_s4` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 4',
  `extend_s5` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 5',
  `extend_s6` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 6',
  `extend_s7` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 7',
  `extend_s8` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展 String 8',
  `extend_i1` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 1',
  `extend_i2` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 2',
  `extend_i3` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 3',
  `extend_i4` decimal(19, 0) NULL DEFAULT NULL COMMENT '扩展 Integer 4',
  `extend_f1` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 1',
  `extend_f2` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 2',
  `extend_f3` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 3',
  `extend_f4` decimal(19, 4) NULL DEFAULT NULL COMMENT '扩展 Float 4',
  `extend_d1` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 1',
  `extend_d2` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 2',
  `extend_d3` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 3',
  `extend_d4` datetime(0) NULL DEFAULT NULL COMMENT '扩展 Date 4',
  PRIMARY KEY (`user_code`) USING BTREE,
  INDEX `idx_sys_user_lc`(`login_code`) USING BTREE,
  INDEX `idx_sys_user_mobile`(`mobile`) USING BTREE,
  INDEX `idx_sys_user_wo`(`wx_openid`) USING BTREE,
  INDEX `idx_sys_user_imei`(`mobile_imei`) USING BTREE,
  INDEX `idx_sys_user_rt`(`user_type`) USING BTREE,
  INDEX `idx_sys_user_rc`(`ref_code`) USING BTREE,
  INDEX `idx_sys_user_mt`(`mgr_type`) USING BTREE,
  INDEX `idx_sys_user_us`(`user_weight`) USING BTREE,
  INDEX `idx_sys_user_ud`(`update_date`) USING BTREE,
  INDEX `idx_sys_user_status`(`status`) USING BTREE,
  INDEX `idx_sys_user_cc`(`corp_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_user
-- ----------------------------
INSERT INTO `js_sys_user` VALUES ('admin', 'admin', '系统管理员', '7be2b0b398f439dbfea46e0372edb03fb5a4be78c75a6515668ab828', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'none', NULL, NULL, '1', 1, '2020-07-21 14:46:16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:17', 'system', '2020-07-21 14:46:17', '客户方使用的系统管理员，用于一些常用的基础数据配置。', '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('system', 'system', '超级管理员', '497557dd14a66ded46c81b06aafb7da6a36eaa3baf5d720c7726ea06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'none', NULL, NULL, '0', 1, '2020-07-21 14:46:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0:0:0:0:0:0:0:1', '2020-07-21 14:57:47', NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:16', 'system', '2020-07-21 14:46:16', '开发者使用的最高级别管理员，主要用于开发和调试。', '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user10_zywb', 'user10', '用户10', 'df59714359efcce02acb8378f31b1b34bff1bf85a974944564cfcdf1', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user10_zywb', '用户10', '0', 0, '2020-07-21 14:46:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:36', 'system', '2020-07-21 14:46:36', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user11_h3nb', 'user11', '用户11', '6749fe2a85e0d769a30e504690a887932f3ae237be8ca48f53f6e51d', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user11_h3nb', '用户11', '0', 0, '2020-07-21 14:46:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:37', 'system', '2020-07-21 14:46:37', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user12_cjoc', 'user12', '用户12', '2b9fafaaf0174f1d4431cc96cb928dffd9312998fd1be0bd8f5eedc7', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user12_cjoc', '用户12', '0', 0, '2020-07-21 14:46:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:38', 'system', '2020-07-21 14:46:38', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user13_v385', 'user13', '用户13', '5fd70ff6669f99ca2b24bf2b3b79f9139c2c4aa685340c2569774de6', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user13_v385', '用户13', '0', 0, '2020-07-21 14:46:39', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:39', 'system', '2020-07-21 14:46:39', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user14_3xop', 'user14', '用户14', '6cdf19aefb7ad495bb9d9da57056c5306457de7837354364fc4243bb', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user14_3xop', '用户14', '0', 0, '2020-07-21 14:46:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:41', 'system', '2020-07-21 14:46:41', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user15_61pf', 'user15', '用户15', '728ea6275e697b31544f4a450f8a84a80f9096ac4436f369fdebf857', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user15_61pf', '用户15', '0', 0, '2020-07-21 14:46:42', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:42', 'system', '2020-07-21 14:46:42', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user16_txve', 'user16', '用户16', '1d8973d16c3fd5cc31224a5a0328a09a2f69bfe542ba28e4dd70fe8b', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user16_txve', '用户16', '0', 0, '2020-07-21 14:46:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:43', 'system', '2020-07-21 14:46:43', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user17_gs1x', 'user17', '用户17', '85673165c8424f7c692a0b1315493865ad86a4112aefb4eb9aba9420', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user17_gs1x', '用户17', '0', 0, '2020-07-21 14:46:44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:44', 'system', '2020-07-21 14:46:44', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user18_vqbf', 'user18', '用户18', 'cd9e719a8d0e739b99e0f84be2ea5304c501b15b4a61a636d8bf3d4c', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user18_vqbf', '用户18', '0', 0, '2020-07-21 14:46:45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:45', 'system', '2020-07-21 14:46:45', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user19_jzf1', 'user19', '用户19', '8479dd907e4470ef93576ae918193d6987a476e2f09341ceb1df39ef', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user19_jzf1', '用户19', '0', 0, '2020-07-21 14:46:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:46', 'system', '2020-07-21 14:46:46', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user1_mhhh', 'user1', '用户01', 'd9b5a40c6d1686add0a61ea10f6b336e171fbe5d0dd3db866f12f5a1', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user1_mhhh', '用户01', '0', 0, '2020-07-21 14:46:23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:23', 'system', '2020-07-21 14:46:23', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user20_tbcj', 'user20', '用户20', '59e5fcdbf87bc71ac0d48505a599a78499159d99d05cee4be66efb5d', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user20_tbcj', '用户20', '0', 0, '2020-07-21 14:46:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:48', 'system', '2020-07-21 14:46:48', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user21_zmvi', 'user21', '用户21', '592edf72582f49d3813c5a5703678ed58d908ab760e1259cda58cff6', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user21_zmvi', '用户21', '0', 0, '2020-07-21 14:46:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:49', 'system', '2020-07-21 14:46:49', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user22_sdc5', 'user22', '用户22', '26e445c49025f6a6a77d31b723030605fd3a268daa75839a30025880', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user22_sdc5', '用户22', '0', 0, '2020-07-21 14:46:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:50', 'system', '2020-07-21 14:46:50', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user23_732v', 'user23', '用户23', 'db7b6f6fe6f4bd5ab16df6ff659b0ded64c8fd86e9edcf7273fca44b', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user23_732v', '用户23', '0', 0, '2020-07-21 14:46:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:51', 'system', '2020-07-21 14:46:51', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user2_wpxe', 'user2', '用户02', '348e48c67bf45de71bc3ccb9ee45c19bfd88f893f52e1039720fdcf5', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user2_wpxe', '用户02', '0', 0, '2020-07-21 14:46:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:25', 'system', '2020-07-21 14:46:25', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user3_6ojk', 'user3', '用户03', '7d84ecfed7aae88802526c82776398e7588160d2cbf84d8ca6dceadc', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user3_6ojk', '用户03', '0', 0, '2020-07-21 14:46:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:27', 'system', '2020-07-21 14:46:27', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user4_dgwm', 'user4', '用户04', '580a110d790d71c743144bc99eabef67747310e101d48b481ef99e21', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user4_dgwm', '用户04', '0', 0, '2020-07-21 14:46:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:28', 'system', '2020-07-21 14:46:28', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user5_0wsp', 'user5', '用户05', 'e7f8043310e6e6f33b021e5e5fc92189803d9480e351e8fef88c6394', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user5_0wsp', '用户05', '0', 0, '2020-07-21 14:46:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:30', 'system', '2020-07-21 14:46:30', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user6_9xba', 'user6', '用户06', 'c2df94fe95d7dd600841cee51cadcb786896d5d8562d1896b1c42eec', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user6_9xba', '用户06', '0', 0, '2020-07-21 14:46:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:31', 'system', '2020-07-21 14:46:31', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user7_hhr4', 'user7', '用户07', 'cb42a2230c5e9d8eed600263d280febcb210e9fefeab063409728409', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user7_hhr4', '用户07', '0', 0, '2020-07-21 14:46:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:32', 'system', '2020-07-21 14:46:32', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user8_20v7', 'user8', '用户08', '404231e1031b48be2c60540368dcd684d11fb4debd82293fe633c99f', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user8_20v7', '用户08', '0', 0, '2020-07-21 14:46:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:33', 'system', '2020-07-21 14:46:33', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `js_sys_user` VALUES ('user9_hejq', 'user9', '用户09', 'a4512820de1786ca8993c26860d518e8689cae2db590683e4d16664d', 'user@test.com', '18555555555', '053188888888', NULL, NULL, NULL, NULL, NULL, 'employee', 'user9_hejq', '用户09', '0', 0, '2020-07-21 14:46:34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', 'system', '2020-07-21 14:46:34', 'system', '2020-07-21 14:46:34', NULL, '0', 'JeeSite', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for js_sys_user_data_scope
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_user_data_scope`;
CREATE TABLE `js_sys_user_data_scope`  (
  `user_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '控制用户编码',
  `ctrl_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '控制类型',
  `ctrl_data` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '控制数据',
  `ctrl_permi` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '控制权限',
  PRIMARY KEY (`user_code`, `ctrl_type`, `ctrl_data`, `ctrl_permi`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户数据权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for js_sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `js_sys_user_role`;
CREATE TABLE `js_sys_user_role`  (
  `user_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户编码',
  `role_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色编码',
  PRIMARY KEY (`user_code`, `role_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与角色关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of js_sys_user_role
-- ----------------------------
INSERT INTO `js_sys_user_role` VALUES ('user10_zywb', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user11_h3nb', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user12_cjoc', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user13_v385', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user14_3xop', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user15_61pf', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user16_txve', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user17_gs1x', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user18_vqbf', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user19_jzf1', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user1_mhhh', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user20_tbcj', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user21_zmvi', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user22_sdc5', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user23_732v', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user2_wpxe', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user3_6ojk', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user4_dgwm', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user5_0wsp', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user6_9xba', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user7_hhr4', 'dept');
INSERT INTO `js_sys_user_role` VALUES ('user8_20v7', 'user');
INSERT INTO `js_sys_user_role` VALUES ('user9_hejq', 'user');

-- ----------------------------
-- Table structure for nd1rpt
-- ----------------------------
DROP TABLE IF EXISTS `nd1rpt`;
CREATE TABLE `nd1rpt`  (
  `Title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ShenQingRen` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ShenQingRiJi` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ShenQingRenBuMen` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OID` int(11) NOT NULL DEFAULT 0,
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FID` int(11) NULL DEFAULT 0 COMMENT 'FID',
  `CDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Rec` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Emps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FK_NY` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MyNum` int(11) NULL DEFAULT 1 COMMENT '个数',
  `QingJiaRiQiCong` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Dao` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `QingJiaTianShu` float NULL DEFAULT NULL COMMENT '请假天数',
  `QingJiaYuanYin` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BMJLSP_Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `BMJLSP_Checker` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '@WebUser.No',
  `BMJLSP_RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ZJLSP_Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ZJLSP_Checker` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '@WebUser.No',
  `ZJLSP_RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PNodeID` int(11) NULL DEFAULT 0,
  `PWorkID` int(11) NULL DEFAULT 0,
  `FlowDaySpan` decimal(20, 4) NULL DEFAULT 0.0000 COMMENT '跨度(天)',
  `FlowEndNode` int(11) NULL DEFAULT 0,
  `FlowEnderRDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FlowStartRDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `GUID` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `PEmp` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `PrjNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `BillNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `PFlowNo` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `PrjName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `FlowEmps` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `FlowNote` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `WFSta` int(11) NULL DEFAULT 0,
  `WFState` int(11) NULL DEFAULT 0,
  `FlowEnder` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `FlowStarter` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for nd1track
-- ----------------------------
DROP TABLE IF EXISTS `nd1track`;
CREATE TABLE `nd1track`  (
  `MyPK` int(11) NOT NULL DEFAULT 0,
  `ActionType` int(11) NULL DEFAULT 0 COMMENT '类型',
  `ActionTypeText` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FID` int(11) NULL DEFAULT 0 COMMENT '流程ID',
  `WorkID` int(11) NULL DEFAULT 0 COMMENT '工作ID',
  `NDFrom` int(11) NULL DEFAULT 0 COMMENT '从节点',
  `NDFromT` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NDTo` int(11) NULL DEFAULT 0 COMMENT '到节点',
  `NDToT` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EmpFrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EmpFromT` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EmpTo` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `EmpToT` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `WorkTimeSpan` float NULL DEFAULT NULL COMMENT '时间跨度(天)',
  `Msg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `NodeData` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `Tag` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Exer` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for port_dept_bak
-- ----------------------------
DROP TABLE IF EXISTS `port_dept_bak`;
CREATE TABLE `port_dept_bak`  (
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `NameOfPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门路径',
  `ParentNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父节点编号',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `OrgNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隶属组织',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of port_dept_bak
-- ----------------------------
INSERT INTO `port_dept_bak` VALUES ('100', '集团总部', NULL, '0', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1001', '集团市场部', NULL, '100', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1002', '集团研发部', NULL, '100', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1003', '集团服务部', NULL, '100', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1004', '集团财务部', NULL, '100', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1005', '集团人力资源部', NULL, '100', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1060', '南方分公司', NULL, '100', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1061', '市场部', NULL, '1060', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1062', '财务部', NULL, '1060', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1063', '销售部', NULL, '1060', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1070', '北方分公司', NULL, '100', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1071', '市场部', NULL, '1070', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1072', '财务部', NULL, '1070', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1073', '销售部', NULL, '1070', 0, '');
INSERT INTO `port_dept_bak` VALUES ('1099', '外来单位', NULL, '100', 0, '');

-- ----------------------------
-- Table structure for port_deptemp_bak
-- ----------------------------
DROP TABLE IF EXISTS `port_deptemp_bak`;
CREATE TABLE `port_deptemp_bak`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Dept` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作员,外键:对应物理表:Port_Emp,表描述:用户',
  `OrgNo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织编码',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门人员信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of port_deptemp_bak
-- ----------------------------
INSERT INTO `port_deptemp_bak` VALUES ('1001_zhanghaicheng', '1001', 'zhanghaicheng', NULL);
INSERT INTO `port_deptemp_bak` VALUES ('1001_zhangyifan', '1001', 'zhangyifan', NULL);
INSERT INTO `port_deptemp_bak` VALUES ('1001_zhoushengyu', '1001', 'zhoushengyu', NULL);
INSERT INTO `port_deptemp_bak` VALUES ('1002_qifenglin', '1002', 'qifenglin', NULL);
INSERT INTO `port_deptemp_bak` VALUES ('1002_zhoutianjiao', '1002', 'zhoutianjiao', NULL);
INSERT INTO `port_deptemp_bak` VALUES ('1003_fuhui', '1003', 'fuhui', NULL);
INSERT INTO `port_deptemp_bak` VALUES ('1003_guoxiangbin', '1003', 'guoxiangbin', NULL);
INSERT INTO `port_deptemp_bak` VALUES ('1004_guobaogeng', '1004', 'guobaogeng', NULL);
INSERT INTO `port_deptemp_bak` VALUES ('1004_yangyilei', '1004', 'yangyilei', NULL);
INSERT INTO `port_deptemp_bak` VALUES ('1005_liping', '1005', 'liping', NULL);
INSERT INTO `port_deptemp_bak` VALUES ('1005_liyan', '1005', 'liyan', NULL);
INSERT INTO `port_deptemp_bak` VALUES ('100_zhoupeng', '100', 'zhoupeng', NULL);

-- ----------------------------
-- Table structure for port_deptempstation_bak
-- ----------------------------
DROP TABLE IF EXISTS `port_deptempstation_bak`;
CREATE TABLE `port_deptempstation_bak`  (
  `MyPK` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Dept` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `FK_Station` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位',
  `FK_Emp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作员',
  `OrgNo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织编码',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门岗位人员对应' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of port_deptempstation_bak
-- ----------------------------
INSERT INTO `port_deptempstation_bak` VALUES ('1001_zhanghaicheng_02', '1001', '02', 'zhanghaicheng', NULL);
INSERT INTO `port_deptempstation_bak` VALUES ('1001_zhangyifan_07', '1001', '07', 'zhangyifan', NULL);
INSERT INTO `port_deptempstation_bak` VALUES ('1001_zhoushengyu_07', '1001', '07', 'zhoushengyu', NULL);
INSERT INTO `port_deptempstation_bak` VALUES ('1002_qifenglin_03', '1002', '03', 'qifenglin', NULL);
INSERT INTO `port_deptempstation_bak` VALUES ('1002_zhoutianjiao_08', '1002', '08', 'zhoutianjiao', NULL);
INSERT INTO `port_deptempstation_bak` VALUES ('1003_fuhui_09', '1003', '09', 'fuhui', NULL);
INSERT INTO `port_deptempstation_bak` VALUES ('1003_guoxiangbin_04', '1003', '04', 'guoxiangbin', NULL);
INSERT INTO `port_deptempstation_bak` VALUES ('1004_guobaogeng_10', '1004', '10', 'guobaogeng', NULL);
INSERT INTO `port_deptempstation_bak` VALUES ('1004_yangyilei_05', '1004', '05', 'yangyilei', NULL);
INSERT INTO `port_deptempstation_bak` VALUES ('1005_liping_06', '1005', '06', 'liping', NULL);
INSERT INTO `port_deptempstation_bak` VALUES ('1005_liyan_11', '1005', '11', 'liyan', NULL);
INSERT INTO `port_deptempstation_bak` VALUES ('100_zhoupeng_01', '100', '01', 'zhoupeng', NULL);
INSERT INTO `port_deptempstation_bak` VALUES ('1099_Guest_12', '1005', '12', 'Guest', NULL);

-- ----------------------------
-- Table structure for port_deptstation_bak
-- ----------------------------
DROP TABLE IF EXISTS `port_deptstation_bak`;
CREATE TABLE `port_deptstation_bak`  (
  `FK_Dept` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门 - 主键',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Dept`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门岗位对应' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of port_deptstation_bak
-- ----------------------------
INSERT INTO `port_deptstation_bak` VALUES ('100', '01');
INSERT INTO `port_deptstation_bak` VALUES ('1001', '07');
INSERT INTO `port_deptstation_bak` VALUES ('1002', '08');
INSERT INTO `port_deptstation_bak` VALUES ('1003', '09');
INSERT INTO `port_deptstation_bak` VALUES ('1004', '10');
INSERT INTO `port_deptstation_bak` VALUES ('1005', '11');

-- ----------------------------
-- Table structure for port_emp_bak
-- ----------------------------
DROP TABLE IF EXISTS `port_emp_bak`;
CREATE TABLE `port_emp_bak`  (
  `No` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Pass` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门,外键:对应物理表:Port_Dept,表描述:部门',
  `SID` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SID',
  `EmpNo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工号',
  `Ding_UserID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'DING用户ID',
  `Wei_UserID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信用户ID',
  `Tel` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `Email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `PinYin` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拼音',
  `OrgNo` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织编号',
  `SignType` int(11) NULL DEFAULT NULL COMMENT '签字类型,枚举类型:0 不签名;1 图片签名;2 电子签名;',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '序号',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of port_emp_bak
-- ----------------------------
INSERT INTO `port_emp_bak` VALUES ('admin', 'admin', '123', '100', '', '', '', '', '0531-82374939', 'zhoupeng@ccflow.org', ',admin,admin,', '', 0, 0);
INSERT INTO `port_emp_bak` VALUES ('fuhui', '福惠', '123', '1003', '', '', '', '', '0531-82374939', 'fuhui@ccflow.org', ',fuhui,fh,fh/jtfwb,', '', 0, 0);
INSERT INTO `port_emp_bak` VALUES ('guobaogeng', '郭宝庚', '123', '1004', '', '', '', '', '0531-82374939', 'guobaogeng@ccflow.org', ',guobaogeng,gbg,gbg/jtcwb,', '', 0, 0);
INSERT INTO `port_emp_bak` VALUES ('guoxiangbin', '郭祥斌', '123', '1003', '', '', '', '', '0531-82374939', 'guoxiangbin@ccflow.org', ',guoxiangbin,gxb,gxb/jtfwb,', '', 0, 0);
INSERT INTO `port_emp_bak` VALUES ('liping', '李萍', '123', '1005', '', '', '', '', '0531-82374939', 'liping@ccflow.org', ',liping,lp,lp/jtrlzyb,', '', 0, 0);
INSERT INTO `port_emp_bak` VALUES ('liyan', '李言', '123', '1005', '', '', '', '', '0531-82374939', 'liyan@ccflow.org', ',liyan,ly,ly/jtrlzyb,', '', 0, 0);
INSERT INTO `port_emp_bak` VALUES ('qifenglin', '祁凤林', '123', '1002', '', '', '', '', '0531-82374939', 'qifenglin@ccflow.org', ',qifenglin,qfl,qfl/jtyfb,', '', 0, 0);
INSERT INTO `port_emp_bak` VALUES ('yangyilei', '杨依雷', '123', '1004', '', '', '', '', '0531-82374939', 'yangyilei@ccflow.org', ',yangyilei,yyl,yyl/jtcwb,', '', 0, 0);
INSERT INTO `port_emp_bak` VALUES ('zhanghaicheng', '张海成', '123', '1001', '', '', '', '', '0531-82374939', 'zhanghaicheng@ccflow.org', ',zhanghaicheng,zhc,zhc/jtscb,', '', 0, 0);
INSERT INTO `port_emp_bak` VALUES ('zhangyifan', '张一帆', '123', '1001', '', '', '', '', '0531-82374939', 'zhangyifan@ccflow.org', ',zhangyifan,zyf,zyf/jtscb,', '', 0, 0);
INSERT INTO `port_emp_bak` VALUES ('zhoupeng', '周朋', '123', '100', '', '', '', '', '0531-82374939', 'zhoupeng@ccflow.org', ',zhoupeng,zp,zp/jtzb,', '', 0, 0);
INSERT INTO `port_emp_bak` VALUES ('zhoushengyu', '周升雨', '123', '1001', '', '', '', '', '0531-82374939', 'zhoushengyu@ccflow.org', ',zhoushengyu,zsy,zsy/jtscb,', '', 0, 0);
INSERT INTO `port_emp_bak` VALUES ('zhoutianjiao', '周天娇', '123', '1002', '', '', '', '', '0531-82374939', 'zhoutianjiao@ccflow.org', ',zhoutianjiao,ztj,ztj/jtyfb,', '', 0, 0);

-- ----------------------------
-- Table structure for port_org
-- ----------------------------
DROP TABLE IF EXISTS `port_org`;
CREATE TABLE `port_org`  (
  `No` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号(与部门编号相同) - 主键',
  `Name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织名称',
  `ParentNo` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级组织编号',
  `ParentName` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级组织名称',
  `Adminer` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员登录帐号',
  `AdminerName` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员名称',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '独立组织' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for port_orgadminer
-- ----------------------------
DROP TABLE IF EXISTS `port_orgadminer`;
CREATE TABLE `port_orgadminer`  (
  `OrgNo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织 - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`OrgNo`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '组织管理员' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for port_station_bak
-- ----------------------------
DROP TABLE IF EXISTS `port_station_bak`;
CREATE TABLE `port_station_bak`  (
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `FK_StationType` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位类型,外键:对应物理表:Port_StationType,表描述:岗位类型',
  `OrgNo` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OrgNo',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of port_station_bak
-- ----------------------------
INSERT INTO `port_station_bak` VALUES ('01', '总经理', '1', '0', 0);
INSERT INTO `port_station_bak` VALUES ('02', '市场部经理', '2', '0', 0);
INSERT INTO `port_station_bak` VALUES ('03', '研发部经理', '2', '0', 0);
INSERT INTO `port_station_bak` VALUES ('04', '客服部经理', '2', '0', 0);
INSERT INTO `port_station_bak` VALUES ('05', '财务部经理', '2', '0', 0);
INSERT INTO `port_station_bak` VALUES ('06', '人力资源部经理', '2', '0', 0);
INSERT INTO `port_station_bak` VALUES ('07', '销售人员岗', '3', '0', 0);
INSERT INTO `port_station_bak` VALUES ('08', '程序员岗', '3', '0', 0);
INSERT INTO `port_station_bak` VALUES ('09', '技术服务岗', '3', '0', 0);
INSERT INTO `port_station_bak` VALUES ('10', '出纳岗', '3', '0', 0);
INSERT INTO `port_station_bak` VALUES ('11', '人力资源助理岗', '3', '0', 0);
INSERT INTO `port_station_bak` VALUES ('12', '外来人员岗', '3', '0', 0);

-- ----------------------------
-- Table structure for port_stationtype_bak
-- ----------------------------
DROP TABLE IF EXISTS `port_stationtype_bak`;
CREATE TABLE `port_stationtype_bak`  (
  `No` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序',
  `OrgNo` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OrgNo',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位类型' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of port_stationtype_bak
-- ----------------------------
INSERT INTO `port_stationtype_bak` VALUES ('1', '高层', 0, NULL);
INSERT INTO `port_stationtype_bak` VALUES ('2', '中层', 0, NULL);
INSERT INTO `port_stationtype_bak` VALUES ('3', '基层', 0, NULL);

-- ----------------------------
-- Table structure for port_team
-- ----------------------------
DROP TABLE IF EXISTS `port_team`;
CREATE TABLE `port_team`  (
  `No` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `FK_TeamType` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型,外键:对应物理表:Port_TeamType,表描述:用户组类型',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for port_teamemp
-- ----------------------------
DROP TABLE IF EXISTS `port_teamemp`;
CREATE TABLE `port_teamemp`  (
  `FK_Team` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户组 - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Team`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组人员' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for port_teamtype
-- ----------------------------
DROP TABLE IF EXISTS `port_teamtype`;
CREATE TABLE `port_teamtype`  (
  `No` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户组类型' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for pub_nd
-- ----------------------------
DROP TABLE IF EXISTS `pub_nd`;
CREATE TABLE `pub_nd`  (
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '年度' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for pub_ny
-- ----------------------------
DROP TABLE IF EXISTS `pub_ny`;
CREATE TABLE `pub_ny`  (
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '年月' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for pub_yf
-- ----------------------------
DROP TABLE IF EXISTS `pub_yf`;
CREATE TABLE `pub_yf`  (
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '月份' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `TableID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表ID',
  `TableName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `DictType` int(11) NULL DEFAULT NULL COMMENT '数据类型,枚举类型:0 编号名称;1 树结构;',
  `OrgNo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OrgNo',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统字典表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_dictdtl
-- ----------------------------
DROP TABLE IF EXISTS `sys_dictdtl`;
CREATE TABLE `sys_dictdtl`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `DictMyPK` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外键表ID',
  `BH` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'BH',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父节点ID',
  `OrgNo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OrgNo',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统字典表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_docfile
-- ----------------------------
DROP TABLE IF EXISTS `sys_docfile`;
CREATE TABLE `sys_docfile`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FileName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `FileSize` int(11) NULL DEFAULT NULL COMMENT '大小',
  `FileType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件类型',
  `D1` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'D1',
  `D2` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'D2',
  `D3` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'D3',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '备注字段文件管理者' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_encfg
-- ----------------------------
DROP TABLE IF EXISTS `sys_encfg`;
CREATE TABLE `sys_encfg`  (
  `No` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实体名称 - 主键',
  `GroupTitle` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组标签',
  `Url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '要打开的Url',
  `FJSavePath` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存路径',
  `FJWebPath` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件Web路径',
  `Datan` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段数据分析方式',
  `UI` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'UI设置',
  `ColorSet` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '颜色设置',
  `FieldSet` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段设置',
  `AtPara` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '实体配置' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_encfg
-- ----------------------------
INSERT INTO `sys_encfg` VALUES ('BP.Frm.FrmBill', '@No=基础信息,单据基础配置信息.@BtnNewLable=单据按钮权限,用于控制每个功能按钮启用规则.@BtnImpExcel=列表按钮,列表按钮控制@Designer=设计者,流程开发设计者信息', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.Frm.FrmDict', '@No=基础信息,单据基础配置信息.@BtnNewLable=单据按钮权限,用于控制每个功能按钮启用规则.@BtnImpExcel=列表按钮,列表按钮控制@Designer=设计者,流程开发设计者信息', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.Sys.FrmUI.FrmAttachmentExt', '@MyPK=基础信息,附件的基本配置.\n@DeleteWay=权限控制,控制附件的下载与上传权限.@IsRowLock=WebOffice属性,设置与公文有关系的属性配置.\n@IsToHeLiuHZ=流程相关,控制节点附件的分合流.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.FlowExt', '@No=基础信息,基础信息权限信息.@IsBatchStart=数据&表单,数据导入导出.@IsFrmEnable=轨迹@DesignerNo=设计者,流程开发设计者信息', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.FlowSheet', '@No=基本配置@FlowRunWay=启动方式,配置工作流程如何自动发起，该选项要与流程服务一起工作才有效.@StartLimitRole=启动限制规则@StartGuideWay=发起前置导航@CFlowWay=延续流程@DTSWay=流程数据与业务数据同步@PStarter=轨迹查看权限', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.FrmNodeComponent', '@NodeID=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SFLab=父子流程组件,在该节点上配置与显示父子流程.@FrmThreadLab=子线程组件,对合流节点有效，用于配置与现实子线程运行的情况。@FrmTrackLab=轨迹组件,用于显示流程运行的轨迹图.@FTCLab=流转自定义,在每个节点上自己控制节点的处理人.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.MapDataExt', '@No=基本属性@Designer=设计者信息', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.MapDtlExt', '@No=基础信息,基础信息权限信息.@IsExp=数据导入导出,数据导入导出.@IsEnableLink=超链接,显示在从表的右边.@IsCopyNDData=流程相关,与流程相关的配置非流程可以忽略.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.MapFrmFool', '@No=基础属性,基础属性.@Designer=设计者信息,设计者的单位信息，人员信息，可以上传到表单云.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.NodeExt', '@NodeID=基本配置@SendLab=按钮权限,控制工作节点可操作按钮.@RunModel=运行模式,分合流,父子流程@AutoJumpRole0=跳转,自动跳转规则当遇到该节点时如何让其自动的执行下一步.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_encfg` VALUES ('BP.WF.Template.NodeSheet', '@NodeID=基本配置@FormType=表单@FWCSta=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SFSta=父子流程,对启动，查看父子流程的控件设置.@SendLab=按钮权限,控制工作节点可操作按钮.@RunModel=运行模式,分合流,父子流程@AutoJumpRole0=跳转,自动跳转规则当遇到该节点时如何让其自动的执行下一步.@MPhone_WorkModel=移动,与手机平板电脑相关的应用设置.@OfficeOpen=公文按钮,只有当该节点是公文流程时候有效', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_enum
-- ----------------------------
DROP TABLE IF EXISTS `sys_enum`;
CREATE TABLE `sys_enum`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Lab` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Lab',
  `EnumKey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'EnumKey',
  `IntKey` int(11) NULL DEFAULT NULL COMMENT 'Val',
  `Lang` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '语言',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '枚举数据' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_enum
-- ----------------------------
INSERT INTO `sys_enum` VALUES ('ActionType_CH_0', 'GET', 'ActionType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ActionType_CH_1', 'POST', 'ActionType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_0', '短信', 'AlertType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_1', '邮件', 'AlertType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_2', '邮件与短信', 'AlertType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertType_CH_3', '系统(内部)消息', 'AlertType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_0', '不接收', 'AlertWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_1', '短信', 'AlertWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_2', '邮件', 'AlertWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_3', '内部消息', 'AlertWay', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_4', 'QQ消息', 'AlertWay', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_5', 'RTX消息', 'AlertWay', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('AlertWay_CH_6', 'MSN消息', 'AlertWay', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('AppModel_CH_0', 'BS系统', 'AppModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AppModel_CH_1', 'CS系统', 'AppModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AppType_CH_0', '外部Url连接', 'AppType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AppType_CH_1', '本地可执行文件', 'AppType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AthRunModel_CH_0', '流水模式', 'AthRunModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AthRunModel_CH_1', '固定模式', 'AthRunModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AthRunModel_CH_2', '自定义页面', 'AthRunModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('AthSaveWay_CH_0', '保存到web服务器', 'AthSaveWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AthSaveWay_CH_1', '保存到数据库', 'AthSaveWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AthSaveWay_CH_2', 'ftp服务器', 'AthSaveWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('AthUploadWay_CH_0', '继承模式', 'AthUploadWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AthUploadWay_CH_1', '协作模式', 'AthUploadWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AuthorWay_CH_0', '不授权', 'AuthorWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('AuthorWay_CH_1', '全部流程授权', 'AuthorWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('AuthorWay_CH_2', '指定流程授权', 'AuthorWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('BillOpenModel_CH_0', '下载本地', 'BillOpenModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('BillOpenModel_CH_1', '在线WebOffice打开', 'BillOpenModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('BillState_CH_0', '空白', 'BillState', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('BillState_CH_1', '草稿', 'BillState', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('BillState_CH_100', '归档', 'BillState', 100, 'CH');
INSERT INTO `sys_enum` VALUES ('BillState_CH_2', '编辑中', 'BillState', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('BillSta_CH_0', '运行中', 'BillSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('BillSta_CH_1', '已完成', 'BillSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('BillSta_CH_2', '其他', 'BillSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('BtnNewModel_CH_0', '表格模式', 'BtnNewModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('BtnNewModel_CH_1', '卡片模式', 'BtnNewModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('BtnNewModel_CH_2', '不可用', 'BtnNewModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_0', '上一步可以撤销', 'CancelRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_1', '不能撤销', 'CancelRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_2', '上一步与开始节点可以撤销', 'CancelRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CancelRole_CH_3', '指定的节点可以撤销', 'CancelRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_0', '不能抄送', 'CCRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_1', '手工抄送', 'CCRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_2', '自动抄送', 'CCRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_3', '手工与自动', 'CCRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('CCRole_CH_4', '按表单SysCCEmps字段计算', 'CCRole', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('CCWriteTo_CH_0', '写入抄送列表', 'CCWriteTo', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CCWriteTo_CH_1', '写入待办', 'CCWriteTo', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CCWriteTo_CH_2', '写入待办与抄送列表', 'CCWriteTo', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertRole_CH_0', '不提示', 'CHAlertRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertRole_CH_1', '每天1次', 'CHAlertRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertRole_CH_2', '每天2次', 'CHAlertRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertWay_CH_0', '邮件', 'CHAlertWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertWay_CH_1', '短信', 'CHAlertWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CHAlertWay_CH_2', 'CCIM即时通讯', 'CHAlertWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ChartType_CH_0', '几何图形', 'ChartType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ChartType_CH_1', '肖像图片', 'ChartType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CHRole_CH_0', '禁用', 'CHRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CHRole_CH_1', '启用', 'CHRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CHRole_CH_2', '只读', 'CHRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CHRole_CH_3', '启用并可以调整流程应完成时间', 'CHRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_0', '及时完成', 'CHSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_1', '按期完成', 'CHSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_2', '逾期完成', 'CHSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CHSta_CH_3', '超期完成', 'CHSta', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('CodeStruct_CH_0', '普通的编码表(具有No,Name)', 'CodeStruct', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CodeStruct_CH_1', '树结构(具有No,Name,ParentNo)', 'CodeStruct', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CodeStruct_CH_2', '行政机构编码表(编码以两位编号标识级次树形关系)', 'CodeStruct', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpan_CH_0', '跨0个单元格', 'ColSpan', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpan_CH_1', '跨1个单元格', 'ColSpan', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpan_CH_2', '跨2个单元格', 'ColSpan', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpan_CH_3', '跨3个单元格', 'ColSpan', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ColSpan_CH_4', '跨4个单元格', 'ColSpan', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('CondModel_CH_0', '由连接线条件控制', 'CondModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CondModel_CH_1', '按照用户选择计算', 'CondModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CondModel_CH_2', '发送按钮旁下拉框选择', 'CondModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_0', '当前单元格', 'ConfirmKind', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_1', '左方单元格', 'ConfirmKind', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_2', '上方单元格', 'ConfirmKind', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_3', '右方单元格', 'ConfirmKind', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ConfirmKind_CH_4', '下方单元格', 'ConfirmKind', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('ConnJudgeWay_CH_0', 'or', 'ConnJudgeWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ConnJudgeWay_CH_1', 'and', 'ConnJudgeWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_0', '按岗位', 'CtrlWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_1', '按部门', 'CtrlWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_2', '按人员', 'CtrlWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('CtrlWay_CH_3', '按SQL', 'CtrlWay', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('DataStoreModel_CH_0', '数据轨迹模式', 'DataStoreModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DataStoreModel_CH_1', '数据合并模式', 'DataStoreModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_0', '字符串', 'DataType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_1', '整数', 'DataType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_2', '浮点数', 'DataType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_3', '日期', 'DataType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_4', '日期时间', 'DataType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_5', '外键', 'DataType', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('DataType_CH_6', '枚举', 'DataType', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_0', '应用系统主数据库(默认)', 'DBSrcType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_1', 'SQLServer数据库', 'DBSrcType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_100', 'WebService数据源', 'DBSrcType', 100, 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_2', 'Oracle数据库', 'DBSrcType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_3', 'MySQL数据库', 'DBSrcType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_4', 'Informix数据库', 'DBSrcType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('DBSrcType_CH_50', 'Dubbo服务', 'DBSrcType', 50, 'CH');
INSERT INTO `sys_enum` VALUES ('DefValType_CH_0', '默认值为空', 'DefValType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DefValType_CH_1', '按照设置的默认值设置', 'DefValType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_0', '不能删除', 'DelEnable', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_1', '逻辑删除', 'DelEnable', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_2', '记录日志方式删除', 'DelEnable', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_3', '彻底删除', 'DelEnable', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('DelEnable_CH_4', '让用户决定删除方式', 'DelEnable', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('DeleteWay_CH_0', '不能删除', 'DeleteWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DeleteWay_CH_1', '删除所有', 'DeleteWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DeleteWay_CH_2', '只能删除自己上传的', 'DeleteWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DictType_CH_0', '编号名称', 'DictType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DictType_CH_1', '树结构', 'DictType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DocType_CH_0', '正式公文', 'DocType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DocType_CH_1', '便函', 'DocType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('Draft_CH_0', '无(不设草稿)', 'Draft', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('Draft_CH_1', '保存到待办', 'Draft', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('Draft_CH_2', '保存到草稿箱', 'Draft', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlAddRecModel_CH_0', '按设置的数量初始化空白行', 'DtlAddRecModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlAddRecModel_CH_1', '用按钮增加空白行', 'DtlAddRecModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlOpenType_CH_0', '操作员', 'DtlOpenType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlOpenType_CH_1', '工作ID', 'DtlOpenType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlOpenType_CH_2', '流程ID', 'DtlOpenType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlSaveModel_CH_0', '自动存盘(失去焦点自动存盘)', 'DtlSaveModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlSaveModel_CH_1', '手动存盘(保存按钮触发存盘)', 'DtlSaveModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DtlVer_CH_0', '2017传统版', 'DtlVer', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSearchWay_CH_0', '不启用', 'DTSearchWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSearchWay_CH_1', '按日期', 'DTSearchWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSearchWay_CH_2', '按日期时间', 'DTSearchWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSField_CH_0', '字段名相同', 'DTSField', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSField_CH_1', '按设置的字段匹配', 'DTSField', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSTime_CH_0', '所有的节点发送后', 'DTSTime', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSTime_CH_1', '指定的节点发送后', 'DTSTime', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSTime_CH_2', '当流程结束时', 'DTSTime', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSWay_CH_0', '不考核', 'DTSWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSWay_CH_1', '按照时效考核', 'DTSWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('DTSWay_CH_2', '按照工作量考核', 'DTSWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_0', '无编辑器', 'EditerType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_1', 'Sina编辑器0', 'EditerType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_2', 'FKEditer', 'EditerType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_3', 'KindEditor', 'EditerType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('EditerType_CH_4', '百度的UEditor', 'EditerType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('EditModel_CH_0', '表格', 'EditModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('EditModel_CH_1', '卡片', 'EditModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityEditModel_CH_0', '表格', 'EntityEditModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityEditModel_CH_1', '行编辑', 'EntityEditModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityShowModel_CH_0', '表格', 'EntityShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityShowModel_CH_1', '树干模式', 'EntityShowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityType_CH_0', '独立表单', 'EntityType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityType_CH_1', '单据', 'EntityType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityType_CH_2', '编号名称实体', 'EntityType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('EntityType_CH_3', '树结构实体', 'EntityType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('EventType_CH_0', '禁用', 'EventType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('EventType_CH_1', '执行URL', 'EventType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('EventType_CH_2', '执行CCFromRef.js', 'EventType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ExcelType_CH_0', '普通文件数据提取', 'ExcelType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ExcelType_CH_1', '流程附件数据提取', 'ExcelType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ExcType_CH_0', '超链接', 'ExcType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ExcType_CH_1', '函数', 'ExcType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ExpType_CH_3', '按照SQL计算', 'ExpType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ExpType_CH_4', '按照参数计算', 'ExpType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('FileType_CH_0', '普通附件', 'FileType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FileType_CH_1', '图片文件', 'FileType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_0', '关闭附件', 'FJOpen', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_1', '操作员', 'FJOpen', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_2', '工作ID', 'FJOpen', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FJOpen_CH_3', '流程ID', 'FJOpen', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowAppType_CH_0', '业务流程', 'FlowAppType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowAppType_CH_1', '工程类(项目组流程)', 'FlowAppType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowAppType_CH_2', '公文流程(VSTO)', 'FlowAppType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_0', '超级管理员可以删除', 'FlowDeleteRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_1', '分级管理员可以删除', 'FlowDeleteRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_2', '发起人可以删除', 'FlowDeleteRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowDeleteRole_CH_3', '节点启动删除按钮的操作员', 'FlowDeleteRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_0', '手工启动', 'FlowRunWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_1', '指定人员按时启动', 'FlowRunWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_2', '数据集按时启动', 'FlowRunWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FlowRunWay_CH_3', '触发式启动', 'FlowRunWay', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('FLRole_CH_0', '按接受人', 'FLRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FLRole_CH_1', '按部门', 'FLRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FLRole_CH_2', '按岗位', 'FLRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_0', '始终启用', 'FrmEnableRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_1', '有数据时启用', 'FrmEnableRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_2', '有参数时启用', 'FrmEnableRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_3', '按表单的字段表达式', 'FrmEnableRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_4', '按SQL表达式', 'FrmEnableRole', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_5', '不启用', 'FrmEnableRole', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_6', '按岗位', 'FrmEnableRole', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmEnableRole_CH_7', '按部门', 'FrmEnableRole', 7, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmShowType_CH_0', '普通方式', 'FrmShowType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmShowType_CH_1', '页签方式', 'FrmShowType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmSln_CH_0', '默认方案', 'FrmSln', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmSln_CH_1', '只读方案', 'FrmSln', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmSln_CH_2', '自定义方案', 'FrmSln', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmThreadSta_CH_0', '禁用', 'FrmThreadSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmThreadSta_CH_1', '启用', 'FrmThreadSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_0', '不显示', 'FrmUrlShowWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_1', '自动大小', 'FrmUrlShowWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_2', '指定大小', 'FrmUrlShowWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FrmUrlShowWay_CH_3', '新窗口', 'FrmUrlShowWay', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('FTCWorkModel_CH_0', '简洁模式', 'FTCWorkModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FTCWorkModel_CH_1', '高级模式', 'FTCWorkModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_0', '不启用', 'FWCAth', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_1', '多附件', 'FWCAth', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_2', '单附件(暂不支持)', 'FWCAth', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCAth_CH_3', '图片附件(暂不支持)', 'FWCAth', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCMsgShow_CH_0', '都显示', 'FWCMsgShow', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCMsgShow_CH_1', '仅显示自己的意见', 'FWCMsgShow', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCOrderModel_CH_0', '按审批时间先后排序', 'FWCOrderModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCOrderModel_CH_1', '按照接受人员列表先后顺序(官职大小)', 'FWCOrderModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCShowModel_CH_0', '表格方式', 'FWCShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCShowModel_CH_1', '自由模式', 'FWCShowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCSta_CH_0', '禁用', 'FWCSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCSta_CH_1', '启用', 'FWCSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCSta_CH_2', '只读', 'FWCSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCVer_CH_0', '2018', 'FWCVer', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('FWCVer_CH_1', '2019', 'FWCVer', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('HelpRole_CH_0', '禁用', 'HelpRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('HelpRole_CH_1', '启用', 'HelpRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('HelpRole_CH_2', '强制提示', 'HelpRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('HelpRole_CH_3', '选择性提示', 'HelpRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianLeaderRole_CH_0', '只有一个组长', 'HuiQianLeaderRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianLeaderRole_CH_1', '最后一个组长发送', 'HuiQianLeaderRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianLeaderRole_CH_2', '任意组长可以发送', 'HuiQianLeaderRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianRole_CH_0', '不启用', 'HuiQianRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianRole_CH_1', '协作(同事)模式', 'HuiQianRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('HuiQianRole_CH_4', '组长(领导)模式', 'HuiQianRole', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('HungUpWay_CH_0', '无限挂起', 'HungUpWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('HungUpWay_CH_1', '按指定的时间解除挂起并通知我自己', 'HungUpWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('HungUpWay_CH_2', '按指定的时间解除挂起并通知所有人', 'HungUpWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ImpModel_CH_0', '不导入', 'ImpModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ImpModel_CH_1', '按配置模式导入', 'ImpModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ImpModel_CH_2', '按照xls文件模版导入', 'ImpModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('InvokeTime_CH_0', '发送时', 'InvokeTime', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('InvokeTime_CH_1', '工作到达时', 'InvokeTime', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSLSubFlowOver_CH_0', '不处理', 'IsAutoSendSLSubFlowOver', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSLSubFlowOver_CH_1', '让同级子流程自动运行下一步', 'IsAutoSendSLSubFlowOver', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSLSubFlowOver_CH_2', '结束同级子流程', 'IsAutoSendSLSubFlowOver', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSubFlowOver_CH_0', '不处理', 'IsAutoSendSubFlowOver', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSubFlowOver_CH_1', '让父流程自动运行下一步', 'IsAutoSendSubFlowOver', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('IsAutoSendSubFlowOver_CH_2', '结束父流程', 'IsAutoSendSubFlowOver', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('IsEnableFWC_CH_0', '禁用', 'IsEnableFWC', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('IsEnableFWC_CH_1', '启用', 'IsEnableFWC', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('IsEnableFWC_CH_2', '只读', 'IsEnableFWC', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_0', '无', 'IsSigan', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_1', '图片签名', 'IsSigan', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_2', '山东CA', 'IsSigan', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_3', '广东CA', 'IsSigan', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSigan_CH_4', '图片盖章', 'IsSigan', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_0', 'yyyy-MM-dd', 'IsSupperText', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_1', 'yyyy-MM-dd HH:mm', 'IsSupperText', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_2', 'yyyy-MM-dd HH:mm:ss', 'IsSupperText', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_3', 'yyyy-MM', 'IsSupperText', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_4', 'HH:mm', 'IsSupperText', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_5', 'HH:mm:ss', 'IsSupperText', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('IsSupperText_CH_6', 'MM-dd', 'IsSupperText', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_0', '一般', 'JMCD', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_1', '保密', 'JMCD', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_2', '秘密', 'JMCD', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('JMCD_CH_3', '机密', 'JMCD', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_0', '不能跳转', 'JumpWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_1', '只能向后跳转', 'JumpWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_2', '只能向前跳转', 'JumpWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_3', '任意节点跳转', 'JumpWay', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('JumpWay_CH_4', '按指定规则跳转', 'JumpWay', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('LGType_CH_0', '普通', 'LGType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('LGType_CH_1', '枚举', 'LGType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('LGType_CH_2', '外键', 'LGType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('LGType_CH_3', '打开系统页面', 'LGType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ListShowModel_CH_0', '表格', 'ListShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ListShowModel_CH_1', '卡片', 'ListShowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuCtrlWay_CH_0', '按照设置的控制', 'MenuCtrlWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuCtrlWay_CH_1', '任何人都可以使用', 'MenuCtrlWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuCtrlWay_CH_2', 'Admin用户可以使用', 'MenuCtrlWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_0', '系统根目录', 'MenuType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_1', '系统类别', 'MenuType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_2', '系统', 'MenuType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_3', '目录', 'MenuType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_4', '功能/界面', 'MenuType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('MenuType_CH_5', '功能控制点', 'MenuType', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('MethodDocTypeOfFunc_CH_0', 'SQL', 'MethodDocTypeOfFunc', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MethodDocTypeOfFunc_CH_1', 'URL', 'MethodDocTypeOfFunc', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MethodDocTypeOfFunc_CH_2', 'JavaScript', 'MethodDocTypeOfFunc', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('MethodDocTypeOfFunc_CH_3', '业务单元', 'MethodDocTypeOfFunc', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('MobileShowModel_CH_0', '新页面显示模式', 'MobileShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MobileShowModel_CH_1', '列表模式', 'MobileShowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('Model_CH_0', '普通', 'Model', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('Model_CH_1', '固定行', 'Model', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MoveToShowWay_CH_0', '不显示', 'MoveToShowWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MoveToShowWay_CH_1', '下拉列表0', 'MoveToShowWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MoveToShowWay_CH_2', '平铺', 'MoveToShowWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_0', '不发送', 'MsgCtrl', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_1', '按设置的下一步接受人自动发送（默认）', 'MsgCtrl', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_2', '由本节点表单系统字段(IsSendEmail,IsSendSMS)来决定', 'MsgCtrl', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('MsgCtrl_CH_3', '由SDK开发者参数(IsSendEmail,IsSendSMS)来决定', 'MsgCtrl', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_1', '字符串String', 'MyDataType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_2', '整数类型Int', 'MyDataType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_3', '浮点类型AppFloat', 'MyDataType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_4', '判断类型Boolean', 'MyDataType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_5', '双精度类型Double', 'MyDataType', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_6', '日期型Date', 'MyDataType', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_7', '时间类型Datetime', 'MyDataType', 7, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDataType_CH_8', '金额类型AppMoney', 'MyDataType', 8, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDeptRole_CH_0', '仅部门领导可以查看', 'MyDeptRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDeptRole_CH_1', '部门下所有的人都可以查看', 'MyDeptRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('MyDeptRole_CH_2', '本部门里指定岗位的人可以查看', 'MyDeptRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('NoteEnable_CH_0', '禁用', 'NoteEnable', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('NoteEnable_CH_1', '启用', 'NoteEnable', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('NoteEnable_CH_2', '只读', 'NoteEnable', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('OpenWay_CH_0', '新窗口', 'OpenWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('OpenWay_CH_1', '本窗口', 'OpenWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('OpenWay_CH_2', '覆盖新窗口', 'OpenWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('PicUploadType_CH_0', '拍照上传或者相册上传', 'PicUploadType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('PicUploadType_CH_1', '只能拍照上传', 'PicUploadType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('PopValFormat_CH_0', 'No(仅编号)', 'PopValFormat', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('PopValFormat_CH_1', 'Name(仅名称)', 'PopValFormat', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('PopValFormat_CH_2', 'No,Name(编号与名称,比如zhangsan,张三;lisi,李四;)', 'PopValFormat', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('PowerCtrlType_CH_0', '岗位', 'PowerCtrlType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('PowerCtrlType_CH_1', '人员', 'PowerCtrlType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('PrintPDFModle_CH_0', '全部打印', 'PrintPDFModle', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('PrintPDFModle_CH_1', '单个表单打印(针对树形表单)', 'PrintPDFModle', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('PRI_CH_0', '低', 'PRI', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('PRI_CH_1', '中', 'PRI', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('PRI_CH_2', '高', 'PRI', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_0', '按照指定节点的工作人员', 'PushWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_1', '按照指定的工作人员', 'PushWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_2', '按照指定的工作岗位', 'PushWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_3', '按照指定的部门', 'PushWay', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_4', '按照指定的SQL', 'PushWay', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('PushWay_CH_5', '按照系统指定的字段', 'PushWay', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('QRModel_CH_0', '不生成', 'QRModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('QRModel_CH_1', '生成二维码', 'QRModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('RBShowModel_CH_0', '竖向', 'RBShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('RBShowModel_CH_3', '横向', 'RBShowModel', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_0', '不回执', 'ReadReceipts', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_1', '自动回执', 'ReadReceipts', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_2', '由上一节点表单字段决定', 'ReadReceipts', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadReceipts_CH_3', '由SDK开发者参数决定', 'ReadReceipts', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadRole_CH_0', '不控制', 'ReadRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadRole_CH_1', '未阅读阻止发送', 'ReadRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ReadRole_CH_2', '未阅读做记录', 'ReadRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('RefBillRole_CH_0', '不启用', 'RefBillRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('RefBillRole_CH_1', '非必须选择关联单据', 'RefBillRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('RefBillRole_CH_2', '必须选择关联单据', 'RefBillRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('RefMethodType_CH_1', '模态窗口打开', 'RefMethodType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('RefMethodType_CH_2', '新窗口打开', 'RefMethodType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('RefMethodType_CH_3', '右侧窗口打开', 'RefMethodType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnOneNodeRole_CH_0', '不启用', 'ReturnOneNodeRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnOneNodeRole_CH_1', '按照[退回信息填写字段]作为退回意见直接退回', 'ReturnOneNodeRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnOneNodeRole_CH_2', '按照[审核组件]填写的信息作为退回意见直接退回', 'ReturnOneNodeRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_0', '不能退回', 'ReturnRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_1', '只能退回上一个节点', 'ReturnRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_2', '可退回以前任意节点', 'ReturnRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_3', '可退回指定的节点', 'ReturnRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnRole_CH_4', '由流程图设计的退回路线决定', 'ReturnRole', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnSendModel_CH_0', '从退回节点正常执行', 'ReturnSendModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnSendModel_CH_1', '直接发送到当前节点', 'ReturnSendModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ReturnSendModel_CH_2', '直接发送到当前节点的下一个节点', 'ReturnSendModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('RowOpenModel_CH_0', '新窗口打开', 'RowOpenModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('RowOpenModel_CH_1', '在本窗口打开', 'RowOpenModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('RowOpenModel_CH_2', '弹出窗口打开,关闭后不刷新列表', 'RowOpenModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('RowOpenModel_CH_3', '弹出窗口打开,关闭后刷新列表', 'RowOpenModel', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('RowSpan_CH_1', '跨1个行', 'RowSpan', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('RowSpan_CH_2', '跨2行', 'RowSpan', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('RowSpan_CH_3', '跨3行', 'RowSpan', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('SaveModel_CH_0', '仅节点表', 'SaveModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SaveModel_CH_1', '节点表与Rpt表', 'SaveModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SearchUrlOpenType_CH_0', 'En.htm', 'SearchUrlOpenType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SearchUrlOpenType_CH_1', 'EnOnly.htm', 'SearchUrlOpenType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SearchUrlOpenType_CH_2', '自定义url', 'SearchUrlOpenType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_0', '按岗位', 'SelectorModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_1', '按部门', 'SelectorModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_2', '按人员', 'SelectorModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_3', '按SQL', 'SelectorModel', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_4', '按SQL模版计算', 'SelectorModel', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_5', '使用通用人员选择器', 'SelectorModel', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_6', '部门与岗位的交集', 'SelectorModel', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_7', '自定义Url', 'SelectorModel', 7, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_8', '使用通用部门岗位人员选择器', 'SelectorModel', 8, 'CH');
INSERT INTO `sys_enum` VALUES ('SelectorModel_CH_9', '按岗位智能计算(操作员所在部门)', 'SelectorModel', 9, 'CH');
INSERT INTO `sys_enum` VALUES ('SendModel_CH_0', '给当前人员设置开始节点待办', 'SendModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SendModel_CH_1', '发送到下一个节点', 'SendModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SFOpenType_CH_0', '工作查看器', 'SFOpenType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SFOpenType_CH_1', '傻瓜表单轨迹查看器', 'SFOpenType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SFShowCtrl_CH_0', '可以看所有的子流程', 'SFShowCtrl', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SFShowCtrl_CH_1', '仅仅可以看自己发起的子流程', 'SFShowCtrl', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SFShowModel_CH_0', '表格方式', 'SFShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SFShowModel_CH_1', '自由模式', 'SFShowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SFSta_CH_0', '禁用', 'SFSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SFSta_CH_1', '启用', 'SFSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SFSta_CH_2', '只读', 'SFSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SharingType_CH_0', '共享', 'SharingType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SharingType_CH_1', '私有', 'SharingType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ShowModel_CH_0', '按钮', 'ShowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ShowModel_CH_1', '超链接', 'ShowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ShowWhere_CH_0', '树形表单', 'ShowWhere', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ShowWhere_CH_1', '工具栏', 'ShowWhere', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ShowWhere_CH_2', '抄送工具栏', 'ShowWhere', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SignType_CH_0', '不签名', 'SignType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SignType_CH_1', '图片签名', 'SignType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SignType_CH_2', '电子签名', 'SignType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_0', '方向条件', 'SQLType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_1', '接受人规则', 'SQLType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_2', '下拉框数据过滤', 'SQLType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_3', '级联下拉框', 'SQLType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_4', 'PopVal开窗返回值', 'SQLType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('SQLType_CH_5', '人员选择器人员选择范围', 'SQLType', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_0', '本地的类', 'SrcType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_1', '创建表', 'SrcType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_2', '表或视图', 'SrcType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_3', 'SQL查询表', 'SrcType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('SrcType_CH_4', 'WebServices', 'SrcType', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('SSOType_CH_0', 'SID验证', 'SSOType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SSOType_CH_1', '连接', 'SSOType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SSOType_CH_2', '表单提交', 'SSOType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SSOType_CH_3', '不传值', 'SSOType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('StartGuideWay_CH_0', '无', 'StartGuideWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('StartGuideWay_CH_1', '按系统的URL-(父子流程)单条模式', 'StartGuideWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('StartGuideWay_CH_10', '按自定义的Url', 'StartGuideWay', 10, 'CH');
INSERT INTO `sys_enum` VALUES ('StartGuideWay_CH_11', '按用户输入参数', 'StartGuideWay', 11, 'CH');
INSERT INTO `sys_enum` VALUES ('StartGuideWay_CH_2', '按系统的URL-(子父流程)多条模式', 'StartGuideWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('StartGuideWay_CH_3', '按系统的URL-(实体记录,未完成)单条模式', 'StartGuideWay', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('StartGuideWay_CH_4', '按系统的URL-(实体记录,未完成)多条模式', 'StartGuideWay', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('StartGuideWay_CH_5', '从开始节点Copy数据', 'StartGuideWay', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('StartLimitRole_CH_0', '不限制', 'StartLimitRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('StartLimitRole_CH_1', '每人每天一次', 'StartLimitRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('StartLimitRole_CH_2', '每人每周一次', 'StartLimitRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('StartLimitRole_CH_3', '每人每月一次', 'StartLimitRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('StartLimitRole_CH_4', '每人每季一次', 'StartLimitRole', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('StartLimitRole_CH_5', '每人每年一次', 'StartLimitRole', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('StartLimitRole_CH_6', '发起的列不能重复,(多个列可以用逗号分开)', 'StartLimitRole', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('StartLimitRole_CH_7', '设置的SQL数据源为空,或者返回结果为零时可以启动.', 'StartLimitRole', 7, 'CH');
INSERT INTO `sys_enum` VALUES ('StartLimitRole_CH_8', '设置的SQL数据源为空,或者返回结果为零时不可以启动.', 'StartLimitRole', 8, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowModel_CH_0', '下级子流程', 'SubFlowModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowModel_CH_1', '同级子流程', 'SubFlowModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowStartWay_CH_0', '不启动', 'SubFlowStartWay', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowStartWay_CH_1', '指定的字段启动', 'SubFlowStartWay', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowStartWay_CH_2', '按明细表启动', 'SubFlowStartWay', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowType_CH_0', '手动启动子流程', 'SubFlowType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowType_CH_1', '触发启动子流程', 'SubFlowType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('SubFlowType_CH_2', '延续子流程', 'SubFlowType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('SubThreadType_CH_0', '同表单', 'SubThreadType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('SubThreadType_CH_1', '异表单', 'SubThreadType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TableCol_CH_0', '4列', 'TableCol', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('TableCol_CH_1', '6列', 'TableCol', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TableCol_CH_2', '上下模式3列', 'TableCol', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('TabType_CH_0', '本地表或视图', 'TabType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('TabType_CH_1', '通过一个SQL确定的一个外部数据源', 'TabType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TabType_CH_2', '通过WebServices获得的一个数据源', 'TabType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('Target_CH_0', '新窗口', 'Target', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('Target_CH_1', '本窗口', 'Target', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('Target_CH_2', '父窗口', 'Target', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_0', '未开始', 'TaskSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_1', '进行中', 'TaskSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_2', '完成', 'TaskSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('TaskSta_CH_3', '推迟', 'TaskSta', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('TemplateFileModel_CH_0', 'rtf模版', 'TemplateFileModel', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('TemplateFileModel_CH_1', 'vsto模式的word模版', 'TemplateFileModel', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TemplateFileModel_CH_2', 'vsto模式的excel模版', 'TemplateFileModel', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('TextColSpan_CH_1', '跨1个单元格', 'TextColSpan', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TextColSpan_CH_2', '跨2个单元格', 'TextColSpan', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('TextColSpan_CH_3', '跨3个单元格', 'TextColSpan', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('TextColSpan_CH_4', '跨4个单元格', 'TextColSpan', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('ThreadKillRole_CH_0', '不能删除', 'ThreadKillRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('ThreadKillRole_CH_1', '手工删除', 'ThreadKillRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('ThreadKillRole_CH_2', '自动删除', 'ThreadKillRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_0', '本周', 'TSpan', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_1', '上周', 'TSpan', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_2', '上上周', 'TSpan', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('TSpan_CH_3', '更早', 'TSpan', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('UIContralType_CH_1', '下拉框', 'UIContralType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('UIContralType_CH_3', '单选按钮', 'UIContralType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('UIRowStyleGlo_CH_0', '无风格', 'UIRowStyleGlo', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('UIRowStyleGlo_CH_1', '交替风格', 'UIRowStyleGlo', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('UIRowStyleGlo_CH_2', '鼠标移动', 'UIRowStyleGlo', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('UIRowStyleGlo_CH_3', '交替并鼠标移动', 'UIRowStyleGlo', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileCheck_CH_0', '不控制', 'UploadFileCheck', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileCheck_CH_1', '上传附件个数不能为0', 'UploadFileCheck', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileCheck_CH_2', '每个类别下面的个数不能为0', 'UploadFileCheck', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileNumCheck_CH_0', '不用校验', 'UploadFileNumCheck', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileNumCheck_CH_1', '不能为空', 'UploadFileNumCheck', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadFileNumCheck_CH_2', '每个类别下不能为空', 'UploadFileNumCheck', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadType_CH_0', '单个', 'UploadType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadType_CH_1', '多个', 'UploadType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('UploadType_CH_2', '指定', 'UploadType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('UrlSrcType_CH_0', '自定义', 'UrlSrcType', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('UrlSrcType_CH_1', '地图', 'UrlSrcType', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('UrlSrcType_CH_2', '流程轨迹表', 'UrlSrcType', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('UrlSrcType_CH_3', '流程轨迹图', 'UrlSrcType', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_10', '批处理', 'WFStateApp', 10, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_2', '运行中', 'WFStateApp', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_3', '已完成', 'WFStateApp', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_4', '挂起', 'WFStateApp', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_5', '退回', 'WFStateApp', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_6', '转发', 'WFStateApp', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_7', '删除', 'WFStateApp', 7, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_8', '加签', 'WFStateApp', 8, 'CH');
INSERT INTO `sys_enum` VALUES ('WFStateApp_CH_9', '冻结', 'WFStateApp', 9, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_0', '空白', 'WFState', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_1', '草稿', 'WFState', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_10', '批处理', 'WFState', 10, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_11', '加签回复状态', 'WFState', 11, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_2', '运行中', 'WFState', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_3', '已完成', 'WFState', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_4', '挂起', 'WFState', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_5', '退回', 'WFState', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_6', '转发', 'WFState', 6, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_7', '删除', 'WFState', 7, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_8', '加签', 'WFState', 8, 'CH');
INSERT INTO `sys_enum` VALUES ('WFState_CH_9', '冻结', 'WFState', 9, 'CH');
INSERT INTO `sys_enum` VALUES ('WFSta_CH_0', '运行中', 'WFSta', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('WFSta_CH_1', '已完成', 'WFSta', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WFSta_CH_2', '其他', 'WFSta', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WhatAreYouTodo_CH_0', '关闭提示窗口', 'WhatAreYouTodo', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('WhatAreYouTodo_CH_1', '关闭提示窗口并刷新', 'WhatAreYouTodo', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WhatAreYouTodo_CH_2', '转入到Search.htm页面上去', 'WhatAreYouTodo', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WhenOverSize_CH_0', '不处理', 'WhenOverSize', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('WhenOverSize_CH_1', '向下顺增行', 'WhenOverSize', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WhenOverSize_CH_2', '次页显示', 'WhenOverSize', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoExeIt_CH_0', '操作员执行', 'WhoExeIt', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoExeIt_CH_1', '机器执行', 'WhoExeIt', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoExeIt_CH_2', '混合执行', 'WhoExeIt', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_0', 'WorkID是主键', 'WhoIsPK', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_1', 'FID是主键(干流程的WorkID)', 'WhoIsPK', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_2', '父流程ID是主键', 'WhoIsPK', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_3', '延续流程ID是主键', 'WhoIsPK', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_4', 'P2WorkID是主键', 'WhoIsPK', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('WhoIsPK_CH_5', 'P3WorkID是主键', 'WhoIsPK', 5, 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_0', '不能退回', 'YBFlowReturnRole', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_1', '退回到父流程的开始节点', 'YBFlowReturnRole', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_2', '退回到父流程的任何节点', 'YBFlowReturnRole', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_3', '退回父流程的启动节点', 'YBFlowReturnRole', 3, 'CH');
INSERT INTO `sys_enum` VALUES ('YBFlowReturnRole_CH_4', '可退回到指定的节点', 'YBFlowReturnRole', 4, 'CH');
INSERT INTO `sys_enum` VALUES ('显示方式_CH_0', '4列', '显示方式', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('显示方式_CH_1', '6列', '显示方式', 1, 'CH');
INSERT INTO `sys_enum` VALUES ('显示方式_CH_2', '上下模式3列', '显示方式', 2, 'CH');
INSERT INTO `sys_enum` VALUES ('表单展示方式_CH_0', '普通方式', '表单展示方式', 0, 'CH');
INSERT INTO `sys_enum` VALUES ('表单展示方式_CH_1', '页签方式', '表单展示方式', 1, 'CH');

-- ----------------------------
-- Table structure for sys_enummain
-- ----------------------------
DROP TABLE IF EXISTS `sys_enummain`;
CREATE TABLE `sys_enummain`  (
  `No` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `CfgVal` varchar(1500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '配置信息',
  `Lang` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '语言',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '枚举' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_enver
-- ----------------------------
DROP TABLE IF EXISTS `sys_enver`;
CREATE TABLE `sys_enver`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体类',
  `Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体名',
  `PKValue` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主键值',
  `EVer` int(11) NULL DEFAULT NULL COMMENT '版本号',
  `Rec` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '实体版本号' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_enverdtl
-- ----------------------------
DROP TABLE IF EXISTS `sys_enverdtl`;
CREATE TABLE `sys_enverdtl`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `EnName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体名',
  `EnVerPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本主表PK',
  `AttrKey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段',
  `AttrName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名',
  `OldVal` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '旧值',
  `NewVal` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '新值',
  `EnVer` int(11) NULL DEFAULT NULL COMMENT '版本号(日期)',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日期',
  `Rec` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本号',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '实体修改明细' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_excelfield
-- ----------------------------
DROP TABLE IF EXISTS `sys_excelfield`;
CREATE TABLE `sys_excelfield`  (
  `No` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `CellName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单元格名称',
  `CellRow` int(11) NULL DEFAULT NULL COMMENT '行号',
  `CellColumn` int(11) NULL DEFAULT NULL COMMENT '列号',
  `FK_ExcelSheet` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属ExcelSheet表,外键:对应物理表:Sys_ExcelSheet,表描述:ExcelSheet',
  `Field` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储字段名',
  `FK_ExcelTable` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储数据表,外键:对应物理表:Sys_ExcelTable,表描述:Excel数据表',
  `DataType` int(11) NULL DEFAULT NULL COMMENT '值类型,枚举类型:0 字符串;1 整数;2 浮点数;3 日期;4 日期时间;5 外键;6 枚举;',
  `UIBindKey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外键表/枚举',
  `UIRefKey` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外键表No',
  `UIRefKeyText` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外键表Name',
  `Validators` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '校验器',
  `FK_ExcelFile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属Excel模板,外键:对应物理表:Sys_ExcelFile,表描述:Excel模板',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '参数',
  `ConfirmKind` int(11) NULL DEFAULT NULL COMMENT '单元格确认方式,枚举类型:0 当前单元格;1 左方单元格;2 上方单元格;3 右方单元格;4 下方单元格;',
  `ConfirmCellCount` int(11) NULL DEFAULT NULL COMMENT '单元格确认方向移动量',
  `ConfirmCellValue` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应单元格值',
  `ConfirmRepeatIndex` int(11) NULL DEFAULT NULL COMMENT '对应单元格值重复选定次序',
  `SkipIsNull` int(11) NULL DEFAULT NULL COMMENT '不计非空',
  `SyncToField` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '同步到字段',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Excel字段' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_excelfile
-- ----------------------------
DROP TABLE IF EXISTS `sys_excelfile`;
CREATE TABLE `sys_excelfile`  (
  `No` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Mark` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识',
  `ExcelType` int(11) NULL DEFAULT NULL COMMENT '类型,枚举类型:0 普通文件数据提取;1 流程附件数据提取;',
  `Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '上传说明',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板文件',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float NULL DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Excel模板' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_excelsheet
-- ----------------------------
DROP TABLE IF EXISTS `sys_excelsheet`;
CREATE TABLE `sys_excelsheet`  (
  `No` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'No - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Sheet名称',
  `FK_ExcelFile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Excel模板,外键:对应物理表:Sys_ExcelFile,表描述:Excel模板',
  `SheetIndex` int(11) NULL DEFAULT NULL COMMENT 'Sheet索引',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'ExcelSheet' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_exceltable
-- ----------------------------
DROP TABLE IF EXISTS `sys_exceltable`;
CREATE TABLE `sys_exceltable`  (
  `No` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据表名',
  `FK_ExcelFile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Excel模板,外键:对应物理表:Sys_ExcelFile,表描述:Excel模板',
  `IsDtl` int(11) NULL DEFAULT NULL COMMENT '是否明细表',
  `Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '数据表说明',
  `SyncToTable` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '同步到表',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Excel数据表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_filemanager
-- ----------------------------
DROP TABLE IF EXISTS `sys_filemanager`;
CREATE TABLE `sys_filemanager`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `AttrFileName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定名称',
  `AttrFileNo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定编号',
  `EnName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联的表',
  `RefVal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主键值',
  `WebPath` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Web路径',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float NULL DEFAULT NULL COMMENT 'MyFileSize',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上传时间',
  `Rec` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上传人',
  `Doc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文件管理者' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_formtree
-- ----------------------------
DROP TABLE IF EXISTS `sys_formtree`;
CREATE TABLE `sys_formtree`  (
  `No` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `ParentNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父节点No',
  `OrgNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OrgNo',
  `Idx` int(11) NULL DEFAULT NULL COMMENT 'Idx',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单树' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_formtree
-- ----------------------------
INSERT INTO `sys_formtree` VALUES ('1', '表单树', '0', '', 0);
INSERT INTO `sys_formtree` VALUES ('120', '流程独立表单', '1', '', 0);
INSERT INTO `sys_formtree` VALUES ('121', '常用信息管理', '1', '', 0);
INSERT INTO `sys_formtree` VALUES ('122', '常用单据', '1', '', 0);

-- ----------------------------
-- Table structure for sys_frmattachment
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmattachment`;
CREATE TABLE `sys_frmattachment`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `NoOfObj` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件标识',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点控制(对sln有效)',
  `AthRunModel` int(11) NULL DEFAULT NULL COMMENT '运行模式,枚举类型:0 流水模式;1 固定模式;2 自定义页面;',
  `AthSaveWay` int(11) NULL DEFAULT NULL COMMENT '保存方式,枚举类型:0 保存到web服务器;1 保存到数据库;2 ftp服务器;',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件名称',
  `Exts` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件格式',
  `NumOfUpload` int(11) NULL DEFAULT NULL COMMENT '最小上传数量',
  `TopNumOfUpload` int(11) NULL DEFAULT NULL COMMENT '最大上传数量',
  `FileMaxSize` int(11) NULL DEFAULT NULL COMMENT '附件最大限制(KB)',
  `UploadFileNumCheck` int(11) NULL DEFAULT NULL COMMENT '上传校验方式,枚举类型:0 不用校验;1 不能为空;2 每个类别下不能为空;',
  `SaveTo` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存到',
  `Sort` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `W` float NULL DEFAULT NULL COMMENT '宽度',
  `H` float NULL DEFAULT NULL COMMENT '高度',
  `IsUpload` int(11) NULL DEFAULT NULL COMMENT '是否可以上传',
  `IsVisable` int(11) NULL DEFAULT NULL COMMENT '是否显示附件分组',
  `FileType` int(11) NULL DEFAULT NULL COMMENT '附件类型,枚举类型:0 普通附件;1 图片文件;',
  `PicUploadType` int(11) NULL DEFAULT NULL COMMENT '图片附件上传方式,枚举类型:0 拍照上传或者相册上传;1 只能拍照上传;',
  `DeleteWay` int(11) NULL DEFAULT NULL COMMENT '附件删除规则,枚举类型:0 不能删除;1 删除所有;2 只能删除自己上传的;',
  `IsDownload` int(11) NULL DEFAULT NULL COMMENT '是否可以下载',
  `IsOrder` int(11) NULL DEFAULT NULL COMMENT '是否可以排序',
  `IsAutoSize` int(11) NULL DEFAULT NULL COMMENT '自动控制大小',
  `IsNote` int(11) NULL DEFAULT NULL COMMENT '是否增加备注',
  `IsExpCol` int(11) NULL DEFAULT NULL COMMENT '是否启用扩展列',
  `IsShowTitle` int(11) NULL DEFAULT NULL COMMENT '是否显示标题列',
  `UploadType` int(11) NULL DEFAULT NULL COMMENT '上传类型,枚举类型:0 单个;1 多个;2 指定;',
  `CtrlWay` int(11) NULL DEFAULT NULL COMMENT '控制呈现控制方式,枚举类型:0 按岗位;1 按部门;2 按人员;3 按SQL;',
  `AthUploadWay` int(11) NULL DEFAULT NULL COMMENT '控制上传控制方式,枚举类型:0 继承模式;1 协作模式;',
  `ReadRole` int(11) NULL DEFAULT NULL COMMENT '阅读规则,枚举类型:0 不控制;1 未阅读阻止发送;2 未阅读做记录;',
  `DataRefNoOfObj` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应附件标识',
  `IsWoEnableWF` int(11) NULL DEFAULT NULL COMMENT '是否启用weboffice',
  `IsWoEnableSave` int(11) NULL DEFAULT NULL COMMENT '是否启用保存',
  `IsWoEnableReadonly` int(11) NULL DEFAULT NULL COMMENT '是否只读',
  `IsWoEnableRevise` int(11) NULL DEFAULT NULL COMMENT '是否启用修订',
  `IsWoEnableViewKeepMark` int(11) NULL DEFAULT NULL COMMENT '是否查看用户留痕',
  `IsWoEnablePrint` int(11) NULL DEFAULT NULL COMMENT '是否打印',
  `IsWoEnableOver` int(11) NULL DEFAULT NULL COMMENT '是否启用套红',
  `IsWoEnableSeal` int(11) NULL DEFAULT NULL COMMENT '是否启用签章',
  `IsWoEnableTemplete` int(11) NULL DEFAULT NULL COMMENT '是否启用公文模板',
  `IsWoEnableCheck` int(11) NULL DEFAULT NULL COMMENT '是否自动写入审核信息',
  `IsWoEnableInsertFlow` int(11) NULL DEFAULT NULL COMMENT '是否插入流程',
  `IsWoEnableInsertFengXian` int(11) NULL DEFAULT NULL COMMENT '是否插入风险点',
  `IsWoEnableMarks` int(11) NULL DEFAULT NULL COMMENT '是否启用留痕模式',
  `IsWoEnableDown` int(11) NULL DEFAULT NULL COMMENT '是否启用下载',
  `AtPara` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  `GroupID` int(11) NULL DEFAULT NULL COMMENT 'GroupID',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `IsTurn2Html` int(11) NULL DEFAULT NULL COMMENT '是否转换成html(方便手机浏览)',
  `IsRowLock` int(11) NULL DEFAULT NULL COMMENT '是否启用锁定行',
  `IsToHeLiuHZ` int(11) NULL DEFAULT NULL COMMENT '该附件是否要汇总到合流节点上去？(对子线程节点有效)',
  `IsHeLiuHuiZong` int(11) NULL DEFAULT NULL COMMENT '是否是合流节点的汇总附件组件？(对合流节点有效)',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_frmattachmentdb
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmattachmentdb`;
CREATE TABLE `sys_frmattachmentdb`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_MapData',
  `FK_FrmAttachment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件主键',
  `NoOfObj` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '附件标识',
  `RefPKVal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体主键',
  `FID` int(11) NULL DEFAULT NULL COMMENT 'FID',
  `NodeID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点ID',
  `Sort` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别',
  `FileFullName` varchar(700) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件路径',
  `FileName` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `FileExts` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展',
  `FileSize` float NULL DEFAULT NULL COMMENT '文件大小',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  `Rec` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录人',
  `RecName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录人名字',
  `MyNote` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `IsRowLock` int(11) NULL DEFAULT NULL COMMENT '是否锁定行',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '排序',
  `UploadGUID` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上传GUID',
  `AtPara` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件数据存储' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_frmbtn
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmbtn`;
CREATE TABLE `sys_frmbtn`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `Text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '标签',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `IsView` int(11) NULL DEFAULT NULL COMMENT '是否可见',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否起用',
  `UAC` int(11) NULL DEFAULT NULL COMMENT '控制类型',
  `UACContext` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '控制内容',
  `EventType` int(11) NULL DEFAULT NULL COMMENT '事件类型,枚举类型:0 禁用;1 执行URL;2 执行CCFromRef.js;',
  `EventContext` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '事件内容',
  `MsgOK` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运行成功提示',
  `MsgErr` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运行失败提示',
  `BtnID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮ID',
  `GroupID` int(11) NULL DEFAULT NULL COMMENT '所在分组',
  `GroupIDText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所在分组',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '按钮' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_frmele
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmele`;
CREATE TABLE `sys_frmele`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `EleType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `EleID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控件ID值(对部分控件有效)',
  `EleName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控件名称(对部分控件有效)',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `H` float NULL DEFAULT NULL COMMENT 'H',
  `W` float NULL DEFAULT NULL COMMENT 'W',
  `Tag1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '链接URL',
  `Tag2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag4',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单元素扩展' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_frmeledb
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmeledb`;
CREATE TABLE `sys_frmeledb`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_MapData',
  `EleID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'EleID',
  `RefPKVal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'RefPKVal',
  `FID` int(11) NULL DEFAULT NULL COMMENT 'FID',
  `Tag1` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag5',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单元素扩展DB' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_frmevent
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmevent`;
CREATE TABLE `sys_frmevent`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Event` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '事件名称',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `EventDoType` int(11) NULL DEFAULT NULL COMMENT '事件类型',
  `DoDoc` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '执行内容',
  `MsgOK` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '成功执行提示',
  `MsgError` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '异常信息提示',
  `MsgCtrl` int(11) NULL DEFAULT NULL COMMENT '消息发送控制,枚举类型:0 不发送;1 按设置的下一步接受人自动发送（默认）;2 由本节点表单系统字段(IsSendEmail,IsSendSMS)来决定;3 由SDK开发者参数(IsSendEmail,IsSendSMS)来决定;',
  `MailEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用邮件发送？(如果启用就要设置邮件模版，支持ccflow表达式。)',
  `MailTitle` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮件标题模版',
  `MailDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '邮件内容模版',
  `SMSEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用短信发送？(如果启用就要设置短信模版，支持ccflow表达式。)',
  `SMSDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '短信内容模版',
  `MobilePushEnable` int(11) NULL DEFAULT NULL COMMENT '是否推送到手机、pad端。',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '事件' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_frmimg
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmimg`;
CREATE TABLE `sys_frmimg`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_MapData',
  `KeyOfEn` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应字段',
  `ImgAppType` int(11) NULL DEFAULT NULL COMMENT '应用类型',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `H` float NULL DEFAULT NULL COMMENT 'H',
  `W` float NULL DEFAULT NULL COMMENT 'W',
  `ImgURL` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ImgURL',
  `ImgPath` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ImgPath',
  `LinkURL` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'LinkURL',
  `LinkTarget` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'LinkTarget',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `Tag0` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `ImgSrcType` int(11) NULL DEFAULT NULL COMMENT '图片来源0=本地,1=URL',
  `IsEdit` int(11) NULL DEFAULT NULL COMMENT '是否可以编辑',
  `Name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '中文名称',
  `EnPK` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '英文名称',
  `ColSpan` int(11) NULL DEFAULT NULL COMMENT '单元格数量',
  `TextColSpan` int(11) NULL DEFAULT NULL COMMENT '文本单元格数量',
  `RowSpan` int(11) NULL DEFAULT NULL COMMENT '行数',
  `GroupID` int(11) NULL DEFAULT NULL COMMENT '显示的分组',
  `GroupIDText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的分组',
  `UIWidth` int(11) NULL DEFAULT 0,
  `UIHeight` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_frmimgath
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmimgath`;
CREATE TABLE `sys_frmimgath`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `CtrlID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控件ID',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '中文名称',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `H` float NULL DEFAULT NULL COMMENT 'H',
  `W` float NULL DEFAULT NULL COMMENT 'W',
  `IsEdit` int(11) NULL DEFAULT NULL COMMENT '是否可编辑',
  `IsRequired` int(11) NULL DEFAULT NULL COMMENT '是否必填项',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `GroupID` int(11) NULL DEFAULT NULL COMMENT '显示的分组',
  `GroupIDText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的分组',
  `ColSpan` int(11) NULL DEFAULT NULL COMMENT '单元格数量',
  `TextColSpan` int(11) NULL DEFAULT NULL COMMENT '文本单元格数量,枚举类型:1 跨1个单元格;2 跨2个单元格;3 跨3个单元格;4 跨4个单元格;',
  `RowSpan` int(11) NULL DEFAULT NULL COMMENT '行数,枚举类型:1 跨1个行;2 跨2行;3 跨3行;',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '图片附件' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_frmimgathdb
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmimgathdb`;
CREATE TABLE `sys_frmimgathdb`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `FK_FrmImgAth` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片附件编号',
  `RefPKVal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体主键',
  `FileFullName` varchar(700) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件全路径',
  `FileName` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `FileExts` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '扩展名',
  `FileSize` float NULL DEFAULT NULL COMMENT '文件大小',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  `Rec` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录人',
  `RecName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录人名字',
  `MyNote` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '剪切图片附件数据存储' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_frmlab
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmlab`;
CREATE TABLE `sys_frmlab`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_MapData',
  `Text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Label',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `FontSize` int(11) NULL DEFAULT NULL COMMENT '字体大小',
  `FontColor` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '颜色',
  `FontName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字体名称',
  `FontStyle` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字体风格',
  `FontWeight` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字体宽度',
  `IsBold` int(11) NULL DEFAULT NULL COMMENT '是否粗体',
  `IsItalic` int(11) NULL DEFAULT NULL COMMENT '是否斜体',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标签' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_frmlab
-- ----------------------------
INSERT INTO `sys_frmlab` VALUES ('0fbceb8d9fb6454f9a8fe322a0c069e1', 'ND104', 'Made&nbsp;by&nbsp;ccform', 605, 490, 14, '#FF000000', 'Portable User Interface', 'Normal', '', 0, 0, '');
INSERT INTO `sys_frmlab` VALUES ('1b1c2995624346918bce740606187dda', 'ND103', '填写xxxx申请单', 79.67, 4.27, 23, '#FF000000', 'Portable User Interface', 'Normal', 'normal', 0, 0, '');
INSERT INTO `sys_frmlab` VALUES ('2c6da854dc0d4fb5b0a066f63b3acce6', 'ND102', 'Made&nbsp;by&nbsp;ccform', 605, 490, 14, '#FF000000', 'Portable User Interface', 'Normal', '', 0, 0, '');
INSERT INTO `sys_frmlab` VALUES ('80249e130d764c2586ad97423ca81d21', 'ND101', '填写xxxx申请单', 79.67, 4.27, 23, '#FF000000', 'Portable User Interface', 'Normal', 'normal', 0, 0, '');
INSERT INTO `sys_frmlab` VALUES ('89a7e2a60a184dd1bb9f469e167c5e82', 'ND103', 'Made&nbsp;by&nbsp;ccform', 605, 490, 14, '#FF000000', 'Portable User Interface', 'Normal', '', 0, 0, '');
INSERT INTO `sys_frmlab` VALUES ('9009012f3c344eb88ae2192e99636058', 'ND104', '填写xxxx申请单', 79.67, 4.27, 23, '#FF000000', 'Portable User Interface', 'Normal', 'normal', 0, 0, '');
INSERT INTO `sys_frmlab` VALUES ('9638b4a7d8cf4d448791e0fcd557fb84', 'ND102', '填写xxxx申请单', 79.67, 4.27, 23, '#FF000000', 'Portable User Interface', 'Normal', 'normal', 0, 0, '');
INSERT INTO `sys_frmlab` VALUES ('dc8f478b21f640fd8d28d3a62f406730', 'ND101', 'Made&nbsp;by&nbsp;ccform', 605, 490, 14, '#FF000000', 'Portable User Interface', 'Normal', '', 0, 0, '');

-- ----------------------------
-- Table structure for sys_frmline
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmline`;
CREATE TABLE `sys_frmline`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主表',
  `X1` float NULL DEFAULT NULL COMMENT 'X1',
  `Y1` float NULL DEFAULT NULL COMMENT 'Y1',
  `X2` float NULL DEFAULT NULL COMMENT 'X2',
  `Y2` float NULL DEFAULT NULL COMMENT 'Y2',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `BorderWidth` float NULL DEFAULT NULL COMMENT '宽度',
  `BorderColor` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '颜色',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '初始的GUID',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '线' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_frmline
-- ----------------------------
INSERT INTO `sys_frmline` VALUES ('0672536dd03b44408ef5ad6937a7af74', 'ND102', 81.82, 40, 81.82, 480.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('0ab78d6fd3e24cefaa4b86ab96969130', 'ND104', 83.36, 120.91, 717.91, 120.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('1e1acc1f1df741f4b49af2dffe600fd0', 'ND104', 81.55, 80, 718.82, 80, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('20dbee90bd384cf895dd88eea6b03871', 'ND103', 83.36, 40.91, 717.91, 40.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('2a86f4b528ad48569e8124e11f83cce5', 'ND102', 83.36, 120.91, 717.91, 120.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('30fc1352f71d4429be1a9165f726bfc8', 'ND101', 81.82, 481.82, 720, 481.82, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('3f3e050bda8e4bcc9fdc5af30153fa22', 'ND101', 719.09, 40, 719.09, 482.73, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('475b65406b7740a2a29959771dca1395', 'ND103', 719.09, 40, 719.09, 482.73, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('4eab306c6b23406bad310cb6b16f9d9f', 'ND103', 83.36, 120.91, 717.91, 120.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('4f27aececf7e4e43850f97ec337af87e', 'ND104', 81.82, 481.82, 720, 481.82, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('4f4af6815599436da25cb1096562f4fe', 'ND101', 83.36, 40.91, 717.91, 40.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('55e662b451ff41baabf80ca1bd231425', 'ND103', 81.55, 80, 718.82, 80, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('9a812a4775154835bbc826dbce00f59f', 'ND102', 719.09, 40, 719.09, 482.73, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('a99214ca022540a584ea181bfac1d0b3', 'ND101', 81.55, 80, 718.82, 80, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('beb53cd3b4df4947ab2ea65352d8f336', 'ND102', 81.55, 80, 718.82, 80, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('bf56a24c8f214390a40434bbf282a870', 'ND101', 81.82, 40, 81.82, 480.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('bf625f08ed6148a2a919ad6909298af3', 'ND103', 81.82, 40, 81.82, 480.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('c5e461d670f246408891afeed826a001', 'ND104', 81.82, 40, 81.82, 480.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('c68eb21b924c41268092686279313bd3', 'ND102', 83.36, 40.91, 717.91, 40.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('c88bdbfec4ca4d389ac6c7fe0ed5df8c', 'ND103', 81.82, 481.82, 720, 481.82, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('d26c654da9f44b2d9069acf889343ff0', 'ND104', 719.09, 40, 719.09, 482.73, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('d8b1372960d44f48bb42945fd7513749', 'ND104', 83.36, 40.91, 717.91, 40.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('dfaaa08359e14ac3a1d94f44b4714ca7', 'ND101', 83.36, 120.91, 717.91, 120.91, 9, 9, 2, 'Black', '');
INSERT INTO `sys_frmline` VALUES ('f6bdc61f4fbc4b68a77af1b47336fdd5', 'ND102', 81.82, 481.82, 720, 481.82, 9, 9, 2, 'Black', '');

-- ----------------------------
-- Table structure for sys_frmlink
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmlink`;
CREATE TABLE `sys_frmlink`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `Text` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
  `URLExt` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'URL',
  `Target` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连接目标(_blank,_parent,_self)',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `FontSize` int(11) NULL DEFAULT NULL COMMENT 'FontSize',
  `FontColor` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FontColor',
  `FontName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FontName',
  `FontStyle` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FontStyle',
  `IsBold` int(11) NULL DEFAULT NULL COMMENT 'IsBold',
  `IsItalic` int(11) NULL DEFAULT NULL COMMENT 'IsItalic',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `GroupID` int(11) NULL DEFAULT NULL COMMENT '显示的分组',
  `GroupIDText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的分组',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '超连接' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_frmrb
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmrb`;
CREATE TABLE `sys_frmrb`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `KeyOfEn` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段',
  `EnumKey` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '枚举值',
  `Lab` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
  `IntKey` int(11) NULL DEFAULT NULL COMMENT 'IntKey',
  `UIIsEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `Script` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '要执行的脚本',
  `FieldsCfg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '配置信息@FieldName=Sta',
  `SetVal` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设置的值',
  `Tip` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '选择后提示的信息',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单选框' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_frmreportfield
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmreportfield`;
CREATE TABLE `sys_frmreportfield`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单编号',
  `KeyOfEn` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示中文名',
  `UIWidth` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '宽度',
  `UIVisible` int(11) NULL DEFAULT NULL COMMENT '是否显示',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单报表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_frmrpt
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmrpt`;
CREATE TABLE `sys_frmrpt`  (
  `No` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主表',
  `PTable` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物理表',
  `SQLOfColumn` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列的数据源',
  `SQLOfRow` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '行数据源',
  `RowIdx` int(11) NULL DEFAULT NULL COMMENT '位置',
  `GroupID` int(11) NULL DEFAULT NULL COMMENT 'GroupID',
  `IsShowSum` int(11) NULL DEFAULT NULL COMMENT 'IsShowSum',
  `IsShowIdx` int(11) NULL DEFAULT NULL COMMENT 'IsShowIdx',
  `IsCopyNDData` int(11) NULL DEFAULT NULL COMMENT 'IsCopyNDData',
  `IsHLDtl` int(11) NULL DEFAULT NULL COMMENT '是否是合流汇总',
  `IsReadonly` int(11) NULL DEFAULT NULL COMMENT 'IsReadonly',
  `IsShowTitle` int(11) NULL DEFAULT NULL COMMENT 'IsShowTitle',
  `IsView` int(11) NULL DEFAULT NULL COMMENT '是否可见',
  `IsExp` int(11) NULL DEFAULT NULL COMMENT 'IsExp',
  `IsImp` int(11) NULL DEFAULT NULL COMMENT 'IsImp',
  `IsInsert` int(11) NULL DEFAULT NULL COMMENT 'IsInsert',
  `IsDelete` int(11) NULL DEFAULT NULL COMMENT 'IsDelete',
  `IsUpdate` int(11) NULL DEFAULT NULL COMMENT 'IsUpdate',
  `IsEnablePass` int(11) NULL DEFAULT NULL COMMENT '是否启用通过审核功能?',
  `IsEnableAthM` int(11) NULL DEFAULT NULL COMMENT '是否启用多附件',
  `IsEnableM2M` int(11) NULL DEFAULT NULL COMMENT '是否启用M2M',
  `IsEnableM2MM` int(11) NULL DEFAULT NULL COMMENT '是否启用M2M',
  `WhenOverSize` int(11) NULL DEFAULT NULL COMMENT 'WhenOverSize,枚举类型:0 不处理;1 向下顺增行;2 次页显示;',
  `DtlOpenType` int(11) NULL DEFAULT NULL COMMENT '数据开放类型,枚举类型:0 操作员;1 工作ID;2 流程ID;',
  `EditModel` int(11) NULL DEFAULT NULL COMMENT '显示格式,枚举类型:0 表格;1 卡片;',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `H` float NULL DEFAULT NULL COMMENT 'H',
  `W` float NULL DEFAULT NULL COMMENT 'W',
  `FrmW` float NULL DEFAULT NULL COMMENT 'FrmW',
  `FrmH` float NULL DEFAULT NULL COMMENT 'FrmH',
  `MTR` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '多表头列',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '纬度报表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_frmsln
-- ----------------------------
DROP TABLE IF EXISTS `sys_frmsln`;
CREATE TABLE `sys_frmsln`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `KeyOfEn` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段',
  `Name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段名',
  `EleType` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `UIIsEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用',
  `UIVisible` int(11) NULL DEFAULT NULL COMMENT '是否可见',
  `IsSigan` int(11) NULL DEFAULT NULL COMMENT '是否签名',
  `IsNotNull` int(11) NULL DEFAULT NULL COMMENT '是否为空',
  `RegularExp` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '正则表达式',
  `IsWriteToFlowTable` int(11) NULL DEFAULT NULL COMMENT '是否写入流程表',
  `IsWriteToGenerWorkFlow` int(11) NULL DEFAULT NULL COMMENT '是否写入流程注册表',
  `DefVal` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认值',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '表单字段方案' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_glovar
-- ----------------------------
DROP TABLE IF EXISTS `sys_glovar`;
CREATE TABLE `sys_glovar`  (
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '键 - 主键',
  `Name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Val` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '值',
  `GroupKey` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组值',
  `Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '说明',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '全局变量' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_glovar
-- ----------------------------
INSERT INTO `sys_glovar` VALUES ('0', '选择系统约定默认值', NULL, 'DefVal', NULL, 0);
INSERT INTO `sys_glovar` VALUES ('@FK_ND', '当前年度', NULL, 'DefVal', NULL, 0);
INSERT INTO `sys_glovar` VALUES ('@FK_YF', '当前月份', NULL, 'DefVal', NULL, 0);
INSERT INTO `sys_glovar` VALUES ('@OrgName', '登录人员组织名称', NULL, 'DefVal', NULL, 0);
INSERT INTO `sys_glovar` VALUES ('@OrgNo', '登录人员组织', NULL, 'DefVal', NULL, 0);
INSERT INTO `sys_glovar` VALUES ('@WebUser.FK_Dept', '登陆人员部门编号', NULL, 'DefVal', NULL, 0);
INSERT INTO `sys_glovar` VALUES ('@WebUser.FK_DeptFullName', '登陆人员部门全称', NULL, 'DefVal', NULL, 0);
INSERT INTO `sys_glovar` VALUES ('@WebUser.FK_DeptName', '登陆人员部门名称', NULL, 'DefVal', NULL, 0);
INSERT INTO `sys_glovar` VALUES ('@WebUser.Name', '登陆人员名称', NULL, 'DefVal', NULL, 0);
INSERT INTO `sys_glovar` VALUES ('@WebUser.No', '登陆人员账号', NULL, 'DefVal', NULL, 0);
INSERT INTO `sys_glovar` VALUES ('@yyyy年MM月dd日', '当前日期(yyyy年MM月dd日)', NULL, 'DefVal', NULL, 0);
INSERT INTO `sys_glovar` VALUES ('@yyyy年MM月dd日HH时mm分', '当前日期(yyyy年MM月dd日HH时mm分)', NULL, 'DefVal', NULL, 0);
INSERT INTO `sys_glovar` VALUES ('@yy年MM月dd日', '当前日期(yy年MM月dd日)', NULL, 'DefVal', NULL, 0);
INSERT INTO `sys_glovar` VALUES ('@yy年MM月dd日HH时mm分', '当前日期(yy年MM月dd日HH时mm分)', NULL, 'DefVal', NULL, 0);

-- ----------------------------
-- Table structure for sys_groupenstemplate
-- ----------------------------
DROP TABLE IF EXISTS `sys_groupenstemplate`;
CREATE TABLE `sys_groupenstemplate`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `EnName` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表称',
  `Name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报表名',
  `EnsName` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报表类名',
  `OperateCol` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作属性',
  `Attrs` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运算属性',
  `Rec` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录人',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '报表模板' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_groupfield
-- ----------------------------
DROP TABLE IF EXISTS `sys_groupfield`;
CREATE TABLE `sys_groupfield`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Lab` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
  `FrmID` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `CtrlType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控件类型',
  `CtrlID` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '控件ID',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '傻瓜表单分组' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_groupfield
-- ----------------------------
INSERT INTO `sys_groupfield` VALUES (110, '填写请假申请单', 'ND101', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (111, '流程信息', 'ND1Rpt', '', '', 2, '', '');
INSERT INTO `sys_groupfield` VALUES (112, '填写请假申请单', 'ND102', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (113, '部门经理审批', 'ND102', '', '', 2, '', '');
INSERT INTO `sys_groupfield` VALUES (114, '填写请假申请单', 'ND103', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (115, '部门经理审批', 'ND103', '', '', 2, '', '');
INSERT INTO `sys_groupfield` VALUES (116, '总经理审批', 'ND103', '', '', 3, '', '');
INSERT INTO `sys_groupfield` VALUES (117, '填写请假申请单', 'ND104', '', '', 1, '', '');
INSERT INTO `sys_groupfield` VALUES (118, '部门经理审批', 'ND104', '', '', 2, '', '');
INSERT INTO `sys_groupfield` VALUES (119, '总经理审批', 'ND104', '', '', 3, '', '');

-- ----------------------------
-- Table structure for sys_langue
-- ----------------------------
DROP TABLE IF EXISTS `sys_langue`;
CREATE TABLE `sys_langue`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Langue` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '语言ID',
  `Model` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块',
  `ModelKey` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块实例',
  `Sort` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别',
  `SortKey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别PK',
  `Val` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '语言值',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '语言定义' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_mapattr
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapattr`;
CREATE TABLE `sys_mapattr`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `KeyOfEn` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段',
  `Name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `DefVal` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '默认值',
  `DefValType` int(11) NULL DEFAULT NULL COMMENT '默认值类型',
  `UIContralType` int(11) NULL DEFAULT NULL COMMENT '控件',
  `MyDataType` int(11) NULL DEFAULT NULL COMMENT '数据类型',
  `LGType` int(11) NULL DEFAULT NULL COMMENT '逻辑类型,枚举类型:0 普通;1 枚举;2 外键;3 打开系统页面;',
  `UIWidth` float NULL DEFAULT NULL COMMENT '宽度',
  `UIHeight` float NULL DEFAULT NULL COMMENT '高度',
  `MinLen` int(11) NULL DEFAULT NULL COMMENT '最小长度',
  `MaxLen` int(11) NULL DEFAULT NULL COMMENT '最大长度',
  `UIBindKey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的信息',
  `UIRefKey` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的Key',
  `UIRefKeyText` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '绑定的Text',
  `ExtIsSum` int(11) NULL DEFAULT NULL COMMENT '是否显示合计(对从表有效)',
  `UIVisible` int(11) NULL DEFAULT NULL COMMENT '是否可见',
  `UIIsEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `UIIsLine` int(11) NULL DEFAULT NULL COMMENT '是否单独栏显示',
  `UIIsInput` int(11) NULL DEFAULT NULL COMMENT '是否必填字段',
  `IsSecret` int(11) NULL DEFAULT NULL COMMENT '是否保密',
  `IsRichText` int(11) NULL DEFAULT NULL COMMENT '富文本',
  `IsSupperText` int(11) NULL DEFAULT NULL COMMENT '是否为大文本',
  `FontSize` int(11) NULL DEFAULT NULL COMMENT '字体大小',
  `IsSigan` int(11) NULL DEFAULT NULL COMMENT '签字？',
  `X` float NULL DEFAULT NULL COMMENT 'X',
  `Y` float NULL DEFAULT NULL COMMENT 'Y',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `EditType` int(11) NULL DEFAULT NULL COMMENT '编辑类型',
  `Tag` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识',
  `Tag1` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识1',
  `Tag2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识2',
  `Tag3` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识3',
  `Tip` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '激活提示',
  `ColSpan` int(11) NULL DEFAULT NULL COMMENT '单元格数量',
  `TextColSpan` int(11) NULL DEFAULT NULL COMMENT '文本单元格数量',
  `RowSpan` int(11) NULL DEFAULT NULL COMMENT '行数',
  `CSS` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义样式',
  `GroupID` int(11) NULL DEFAULT NULL COMMENT '显示的分组',
  `IsEnableInAPP` int(11) NULL DEFAULT NULL COMMENT '是否在移动端中显示',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '序号',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  `GroupIDText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的分组',
  `CSSText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义样式',
  `DefValText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认值（选中）',
  `RBShowModel` int(11) NULL DEFAULT NULL COMMENT '单选按钮的展现方式,枚举类型:0 竖向;3 横向;',
  `IsEnableJS` int(11) NULL DEFAULT NULL COMMENT '是否启用JS高级设置？',
  `ExtDefVal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统默认值',
  `ExtDefValText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统默认值',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程进度图' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_mapattr
-- ----------------------------
INSERT INTO `sys_mapattr` VALUES ('ND101_CDT', 'ND101', 'CDT', '发起时间', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 110, 1, 1001, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_Dao', 'ND101', 'Dao', '到', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 110, 1, 1008, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_Emps', 'ND101', 'Emps', 'Emps', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 110, 1, 1003, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_FID', 'ND101', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 110, 1, 1000, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_FK_Dept', 'ND101', 'FK_Dept', '操作员部门', '', 0, 1, 1, 2, 100, 23, 0, 100, 'BP.Port.Depts', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 110, 1, 1004, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_FK_NY', 'ND101', 'FK_NY', '年月', '', 0, 0, 1, 0, 100, 23, 0, 7, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 110, 1, 1005, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_MyNum', 'ND101', 'MyNum', '个数', '1', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 110, 1, 1006, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_OID', 'ND101', 'OID', 'OID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 110, 1, 999, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_QingJiaRiQiCong', 'ND101', 'QingJiaRiQiCong', '请假日期从', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 110, 1, 1007, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_QingJiaTianShu', 'ND101', 'QingJiaTianShu', '请假天数', '10002', 0, 0, 3, 0, 100, 23, 0, 50, '', '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 110, 1, 1009, '@IsSum=0@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_QingJiaYuanYin', 'ND101', 'QingJiaYuanYin', '请假原因', '', 0, 0, 1, 0, 100, 123, 0, 50, '', '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 110, 1, 1010, '@IsRichText=0@FontSize=12@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_RDT', 'ND101', 'RDT', '更新时间', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 110, 1, 999, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_Rec', 'ND101', 'Rec', '发起人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 110, 1, 1002, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_ShenQingRen', 'ND101', 'ShenQingRen', '申请人', '@WebUser.Name', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 110, 1, 1, '@IsRichText=0@FontSize=12@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_ShenQingRenBuMen', 'ND101', 'ShenQingRenBuMen', '申请人部门', '@WebUser.FK_DeptName', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 3, 1, 1, '0', 110, 1, 3, '@FontSize=12@IsRichText=0@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_ShenQingRiJi', 'ND101', 'ShenQingRiJi', '申请日期', '@RDT', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 110, 1, 2, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND101_Title', 'ND101', 'Title', '标题', '', 0, 0, 1, 0, 251, 23, 0, 200, '', '', '', 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 171.2, 68.4, '', 1, '', '', '', '', '', 1, 1, 1, '0', 110, 1, -1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_BMJLSP_Checker', 'ND102', 'BMJLSP_Checker', '审核人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 113, 1, 2, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_BMJLSP_Note', 'ND102', 'BMJLSP_Note', '审核意见', '', 0, 0, 1, 0, 100, 69, 0, 4000, '', '', '', 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 113, 1, 1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_BMJLSP_RDT', 'ND102', 'BMJLSP_RDT', '审核日期', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 113, 1, 3, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_CDT', 'ND102', 'CDT', '发起时间', '', 0, 0, 7, 0, 145, 23, 0, 20, '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 112, 1, 1001, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_Dao', 'ND102', 'Dao', '到', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 1008, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_Emps', 'ND102', 'Emps', 'Emps', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 1003, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_FID', 'ND102', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 1000, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_FK_Dept', 'ND102', 'FK_Dept', '操作员部门', '', 0, 1, 1, 2, 100, 23, 0, 100, 'BP.Port.Depts', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 1004, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_FK_NY', 'ND102', 'FK_NY', '年月', '', 0, 0, 1, 0, 100, 23, 0, 7, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 1005, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_MyNum', 'ND102', 'MyNum', '个数', '1', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 1006, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_OID', 'ND102', 'OID', 'OID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 999, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_QingJiaRiQiCong', 'ND102', 'QingJiaRiQiCong', '请假日期从', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 1007, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_QingJiaTianShu', 'ND102', 'QingJiaTianShu', '请假天数', '10002', 0, 0, 3, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 1009, '@IsSum=0@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_QingJiaYuanYin', 'ND102', 'QingJiaYuanYin', '请假原因', '', 0, 0, 1, 0, 100, 123, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 112, 1, 1010, '@IsRichText=0@FontSize=12@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_RDT', 'ND102', 'RDT', '更新时间', '', 0, 0, 7, 0, 145, 23, 0, 20, '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 112, 1, 999, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_Rec', 'ND102', 'Rec', '发起人', '', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 1002, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_ShenQingRen', 'ND102', 'ShenQingRen', '申请人', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 1, '@IsRichText=0@FontSize=12@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_ShenQingRenBuMen', 'ND102', 'ShenQingRenBuMen', '申请人部门', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 3, 1, 1, '0', 112, 1, 3, '@FontSize=12@IsRichText=0@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_ShenQingRiJi', 'ND102', 'ShenQingRiJi', '申请日期', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 112, 1, 2, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND102_Title', 'ND102', 'Title', '标题', '', 0, 0, 1, 0, 251, 23, 0, 200, '', '', '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 171.2, 68.4, '', 1, '', '', '', '', '', 1, 1, 1, '0', 112, 1, -1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_BMJLSP_Checker', 'ND103', 'BMJLSP_Checker', '审核人', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 115, 1, 2, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_BMJLSP_Note', 'ND103', 'BMJLSP_Note', '审核意见', '', 0, 0, 1, 0, 100, 69, 0, 4000, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 115, 1, 1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_BMJLSP_RDT', 'ND103', 'BMJLSP_RDT', '审核日期', '', 0, 0, 7, 0, 145, 23, 0, 20, '', '', '', 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 115, 1, 3, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_CDT', 'ND103', 'CDT', '发起时间', '', 0, 0, 7, 0, 145, 23, 0, 20, '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 114, 1, 1001, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_Dao', 'ND103', 'Dao', '到', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 114, 1, 1008, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_Emps', 'ND103', 'Emps', 'Emps', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, 1003, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_FID', 'ND103', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, 1000, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_FK_Dept', 'ND103', 'FK_Dept', '操作员部门', '', 0, 1, 1, 2, 100, 23, 0, 100, 'BP.Port.Depts', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, 1004, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_FK_NY', 'ND103', 'FK_NY', '年月', '', 0, 0, 1, 0, 100, 23, 0, 7, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, 1005, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_MyNum', 'ND103', 'MyNum', '个数', '1', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, 1006, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_OID', 'ND103', 'OID', 'OID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 114, 1, 999, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_QingJiaRiQiCong', 'ND103', 'QingJiaRiQiCong', '请假日期从', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 114, 1, 1007, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_QingJiaTianShu', 'ND103', 'QingJiaTianShu', '请假天数', '10002', 0, 0, 3, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 114, 1, 1009, '@IsSum=0@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_QingJiaYuanYin', 'ND103', 'QingJiaYuanYin', '请假原因', '', 0, 0, 1, 0, 100, 123, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 114, 1, 1010, '@IsRichText=0@FontSize=12@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_RDT', 'ND103', 'RDT', '更新时间', '', 0, 0, 7, 0, 145, 23, 0, 20, '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 114, 1, 999, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_Rec', 'ND103', 'Rec', '发起人', '', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, 1002, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_ShenQingRen', 'ND103', 'ShenQingRen', '申请人', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 114, 1, 1, '@IsRichText=0@FontSize=12@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_ShenQingRenBuMen', 'ND103', 'ShenQingRenBuMen', '申请人部门', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 3, 1, 1, '0', 114, 1, 3, '@FontSize=12@IsRichText=0@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_ShenQingRiJi', 'ND103', 'ShenQingRiJi', '申请日期', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 114, 1, 2, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_Title', 'ND103', 'Title', '标题', '', 0, 0, 1, 0, 251, 23, 0, 200, '', '', '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 171.2, 68.4, '', 1, '', '', '', '', '', 1, 1, 1, '0', 114, 1, -1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_ZJLSP_Checker', 'ND103', 'ZJLSP_Checker', '审核人', '@WebUser.No', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 116, 1, 2, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_ZJLSP_Note', 'ND103', 'ZJLSP_Note', '审核意见', '', 0, 0, 1, 0, 100, 69, 0, 4000, '', '', '', 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 116, 1, 1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND103_ZJLSP_RDT', 'ND103', 'ZJLSP_RDT', '审核日期', '@RDT', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 116, 1, 3, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_BMJLSP_Checker', 'ND104', 'BMJLSP_Checker', '审核人', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 118, 1, 2, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_BMJLSP_Note', 'ND104', 'BMJLSP_Note', '审核意见', '', 0, 0, 1, 0, 100, 69, 0, 4000, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 118, 1, 1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_BMJLSP_RDT', 'ND104', 'BMJLSP_RDT', '审核日期', '', 0, 0, 7, 0, 145, 23, 0, 20, '', '', '', 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 118, 1, 3, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_CDT', 'ND104', 'CDT', '发起时间', '', 0, 0, 7, 0, 145, 23, 0, 20, '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 117, 1, 1001, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_Dao', 'ND104', 'Dao', '到', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 1008, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_Emps', 'ND104', 'Emps', 'Emps', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 1003, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_FID', 'ND104', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 1000, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_FK_Dept', 'ND104', 'FK_Dept', '操作员部门', '', 0, 1, 1, 2, 100, 23, 0, 100, 'BP.Port.Depts', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 1004, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_FK_NY', 'ND104', 'FK_NY', '年月', '', 0, 0, 1, 0, 100, 23, 0, 7, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 1005, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_MyNum', 'ND104', 'MyNum', '个数', '1', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 1006, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_OID', 'ND104', 'OID', 'OID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 999, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_QingJiaRiQiCong', 'ND104', 'QingJiaRiQiCong', '请假日期从', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 1007, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_QingJiaTianShu', 'ND104', 'QingJiaTianShu', '请假天数', '10002', 0, 0, 3, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 1009, '@IsSum=0@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_QingJiaYuanYin', 'ND104', 'QingJiaYuanYin', '请假原因', '', 0, 0, 1, 0, 100, 123, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 117, 1, 1010, '@IsRichText=0@FontSize=12@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_RDT', 'ND104', 'RDT', '更新时间', '', 0, 0, 7, 0, 145, 23, 0, 20, '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 117, 1, 999, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_Rec', 'ND104', 'Rec', '发起人', '', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 1002, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_ShenQingRen', 'ND104', 'ShenQingRen', '申请人', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 1, '@IsRichText=0@FontSize=12@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_ShenQingRenBuMen', 'ND104', 'ShenQingRenBuMen', '申请人部门', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 3, 1, 1, '0', 117, 1, 3, '@FontSize=12@IsRichText=0@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_ShenQingRiJi', 'ND104', 'ShenQingRiJi', '申请日期', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 117, 1, 2, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_Title', 'ND104', 'Title', '标题', '', 0, 0, 1, 0, 251, 23, 0, 200, '', '', '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 171.2, 68.4, '', 1, '', '', '', '', '', 1, 1, 1, '0', 117, 1, -1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_ZJLSP_Checker', 'ND104', 'ZJLSP_Checker', '审核人', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 119, 1, 2, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_ZJLSP_Note', 'ND104', 'ZJLSP_Note', '审核意见', '', 0, 0, 1, 0, 100, 69, 0, 4000, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 119, 1, 1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND104_ZJLSP_RDT', 'ND104', 'ZJLSP_RDT', '审核日期', '', 0, 0, 7, 0, 145, 23, 0, 20, '', '', '', 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 119, 1, 3, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_AtPara', 'ND1Rpt', 'AtPara', '参数', '', 1, 0, 1, 0, 100, 23, 0, 4000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -100, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_BillNo', 'ND1Rpt', 'BillNo', '单据编号', '', 1, 0, 1, 0, 100, 23, 0, 100, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -100, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_BMJLSP_Checker', 'ND1Rpt', 'BMJLSP_Checker', '审核人', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 103, 1, 2, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_BMJLSP_Note', 'ND1Rpt', 'BMJLSP_Note', '审核意见', '', 0, 0, 1, 0, 100, 69, 0, 4000, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 103, 1, 1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_BMJLSP_RDT', 'ND1Rpt', 'BMJLSP_RDT', '审核日期', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 103, 1, 3, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_CDT', 'ND1Rpt', 'CDT', '活动时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 100, 1, 1001, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_Dao', 'ND1Rpt', 'Dao', '到', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 100, 1, 1008, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_Emps', 'ND1Rpt', 'Emps', '参与者', '', 0, 0, 1, 0, 100, 23, 0, 8000, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 100, 1, 1003, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FID', 'ND1Rpt', 'FID', 'FID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 100, 1, 1000, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FK_Dept', 'ND1Rpt', 'FK_Dept', '操作员部门', '', 0, 0, 1, 0, 100, 23, 0, 100, 'BP.Port.Depts', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, 1004, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FK_NY', 'ND1Rpt', 'FK_NY', '年月', '', 0, 0, 1, 0, 100, 23, 0, 7, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, 1005, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowDaySpan', 'ND1Rpt', 'FlowDaySpan', '跨度(天)', '', 1, 0, 8, 0, 100, 23, 0, 300, '', '', '', 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -101, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowEmps', 'ND1Rpt', 'FlowEmps', '参与人', '', 1, 0, 1, 0, 100, 23, 0, 1000, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -100, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowEnder', 'ND1Rpt', 'FlowEnder', '结束人', '', 1, 1, 1, 2, 100, 23, 0, 100, 'BP.Port.Emps', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowEnderRDT', 'ND1Rpt', 'FlowEnderRDT', '结束时间', '', 1, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -101, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowEndNode', 'ND1Rpt', 'FlowEndNode', '结束节点', '0', 1, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -101, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowNote', 'ND1Rpt', 'FlowNote', '流程信息', '', 1, 0, 1, 0, 100, 23, 0, 500, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -100, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowStarter', 'ND1Rpt', 'FlowStarter', '发起人', '', 1, 1, 1, 2, 100, 23, 0, 100, 'BP.Port.Emps', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_FlowStartRDT', 'ND1Rpt', 'FlowStartRDT', '发起时间', '', 1, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -101, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_GUID', 'ND1Rpt', 'GUID', 'GUID', '', 1, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -100, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_MyNum', 'ND1Rpt', 'MyNum', '个数', '1', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, 1006, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_OID', 'ND1Rpt', 'OID', 'OID', '0', 0, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 2, '', '', '', '', '', 1, 1, 1, '0', 100, 1, 999, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_PEmp', 'ND1Rpt', 'PEmp', '调起子流程的人员', '', 1, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -100, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_PFlowNo', 'ND1Rpt', 'PFlowNo', '父流程流程编号', '', 1, 0, 1, 0, 100, 23, 0, 3, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -100, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_PNodeID', 'ND1Rpt', 'PNodeID', '父流程启动的节点', '0', 1, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -101, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_PrjName', 'ND1Rpt', 'PrjName', '项目名称', '', 1, 0, 1, 0, 100, 23, 0, 100, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -100, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_PrjNo', 'ND1Rpt', 'PrjNo', '项目编号', '', 1, 0, 1, 0, 100, 23, 0, 100, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -100, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_PWorkID', 'ND1Rpt', 'PWorkID', '父流程WorkID', '0', 1, 0, 2, 0, 100, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -101, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_QingJiaRiQiCong', 'ND1Rpt', 'QingJiaRiQiCong', '请假日期从', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 100, 1, 1007, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_QingJiaTianShu', 'ND1Rpt', 'QingJiaTianShu', '请假天数', '10002', 0, 0, 3, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 100, 1, 1009, '@IsSum=0@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_QingJiaYuanYin', 'ND1Rpt', 'QingJiaYuanYin', '请假原因', '', 0, 0, 1, 0, 100, 123, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 100, 1, 1010, '@IsRichText=0@FontSize=12@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_RDT', 'ND1Rpt', 'RDT', '更新时间', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 1, '1', '', '', '', '', 1, 1, 1, '0', 100, 1, 999, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_Rec', 'ND1Rpt', 'Rec', '发起人', '', 0, 0, 1, 0, 100, 23, 0, 32, '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 100, 1, 1002, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_ShenQingRen', 'ND1Rpt', 'ShenQingRen', '申请人', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 100, 1, 1, '@IsRichText=0@FontSize=12@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_ShenQingRenBuMen', 'ND1Rpt', 'ShenQingRenBuMen', '申请人部门', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 3, 1, 1, '0', 100, 1, 3, '@FontSize=12@IsRichText=0@IsSupperText=0', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_ShenQingRiJi', 'ND1Rpt', 'ShenQingRiJi', '申请日期', '', 0, 0, 6, 0, 125, 23, 0, 10, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 100, 1, 2, '@FontSize=12', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_Title', 'ND1Rpt', 'Title', '标题', '', 0, 0, 1, 0, 251, 23, 0, 200, '', '', '', 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 171.2, 68.4, '', 1, '', '', '', '', '', 1, 1, 1, '0', 100, 1, -1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_WFSta', 'ND1Rpt', 'WFSta', '状态', '', 1, 1, 2, 1, 100, 23, 0, 1000, 'WFSta', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_WFState', 'ND1Rpt', 'WFState', '流程状态', '', 1, 1, 2, 1, 100, 23, 0, 1000, 'WFState', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5, '', 1, '', '', '', '', '', 1, 1, 1, '0', 111, 1, -1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_ZJLSP_Checker', 'ND1Rpt', 'ZJLSP_Checker', '审核人', '', 0, 0, 1, 0, 100, 23, 0, 50, '', '', '', 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 103, 1, 2, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_ZJLSP_Note', 'ND1Rpt', 'ZJLSP_Note', '审核意见', '', 0, 0, 1, 0, 100, 69, 0, 4000, '', '', '', 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 5, 5, '', 0, '', '', '', '', '', 4, 1, 1, '0', 103, 1, 1, '', '0', '0', '0', 0, 0, '0', '0');
INSERT INTO `sys_mapattr` VALUES ('ND1Rpt_ZJLSP_RDT', 'ND1Rpt', 'ZJLSP_RDT', '审核日期', '', 0, 0, 7, 0, 145, 23, 0, 300, '', '', '', 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 5, 5, '', 0, '', '', '', '', '', 1, 1, 1, '0', 103, 1, 3, '', '0', '0', '0', 0, 0, '0', '0');

-- ----------------------------
-- Table structure for sys_mapdata
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapdata`;
CREATE TABLE `sys_mapdata`  (
  `No` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'null - 主键',
  `FrmType` int(11) NULL DEFAULT NULL COMMENT '表单类型',
  `PTable` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物理表',
  `Name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'null',
  `FK_FormTree` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单树',
  `TableCol` int(11) NULL DEFAULT NULL COMMENT '表单显示列数,枚举类型:0 4列;1 6列;2 上下模式3列;',
  `RowOpenModel` int(11) NULL DEFAULT NULL COMMENT '行记录打开模式,枚举类型:0 新窗口打开;1 在本窗口打开;2 弹出窗口打开,关闭后不刷新列表;3 弹出窗口打开,关闭后刷新列表;',
  `EntityType` int(11) NULL DEFAULT NULL COMMENT '业务类型,枚举类型:0 独立表单;1 单据;2 编号名称实体;3 树结构实体;',
  `BillNoFormat` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体编号规则',
  `TitleRole` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题生成规则',
  `SortColumns` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '排序字段',
  `ColorSet` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '颜色设置',
  `FieldSet` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字段求和求平均设置',
  `BtnNewLable` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '新建',
  `BtnNewModel` int(11) NULL DEFAULT NULL COMMENT '新建模式,枚举类型:0 表格模式;1 卡片模式;2 不可用;',
  `BtnSaveLable` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存',
  `BtnSubmitLable` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '提交',
  `BtnDelLable` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除',
  `BtnSearchLabel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列表',
  `BtnGroupLabel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分析',
  `BtnGroupEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnPrintHtml` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印Html',
  `BtnPrintHtmlEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnPrintPDF` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印PDF',
  `BtnPrintPDFEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnPrintRTF` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印RTF',
  `BtnPrintRTFEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnPrintCCWord` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印CCWord',
  `BtnPrintCCWordEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnExpZip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '导出zip文件',
  `BtnExpZipEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnRefBill` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联单据',
  `RefBillRole` int(11) NULL DEFAULT NULL COMMENT '关联单据工作模式,枚举类型:0 不启用;1 非必须选择关联单据;2 必须选择关联单据;',
  `RefBill` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联单据ID',
  `BtnImpExcel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '导入Excel文件',
  `BtnImpExcelEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `BtnExpExcel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '导出Excel文件',
  `BtnExpExcelEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用？',
  `Designer` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设计者',
  `DesignerContact` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `DesignerUnit` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `Ver` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本号',
  `Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `Tag0` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag0',
  `Tag1` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Tag1',
  `Tag2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  `PopHeight` int(11) NULL DEFAULT NULL COMMENT '弹窗高度',
  `PopWidth` int(11) NULL DEFAULT NULL COMMENT '弹窗宽度',
  `EntityEditModel` int(11) NULL DEFAULT NULL COMMENT '编辑模式',
  `EntityShowModel` int(11) NULL DEFAULT NULL COMMENT '展示模式,枚举类型:0 表格;1 树干模式;',
  `FormEventEntity` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '事件实体',
  `EnPK` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体主键',
  `PTableModel` int(11) NULL DEFAULT NULL COMMENT '表存储模式',
  `URL` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Url',
  `Dtls` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从表',
  `FrmW` int(11) NULL DEFAULT NULL COMMENT '系统表单宽度',
  `FrmH` int(11) NULL DEFAULT NULL COMMENT '系统表单高度',
  `Tag` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag',
  `FK_FrmSort` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单类别',
  `FrmShowType` int(11) NULL DEFAULT NULL COMMENT '表单展示方式,枚举类型:0 普通方式;1 页签方式;',
  `AppType` int(11) NULL DEFAULT NULL COMMENT '应用类型',
  `DBSrc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源,外键:对应物理表:Sys_SFDBSrc,表描述:数据源',
  `BodyAttr` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单Body属性',
  `FlowCtrls` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程控件',
  `IsTemplate` int(11) NULL DEFAULT NULL COMMENT '是否是表单模版',
  `OfficeOpenLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOverLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeOverEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeMarksEnable` int(11) NULL DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印按钮标签',
  `OfficePrintEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeSealLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeSealEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeNodeInfo` int(11) NULL DEFAULT NULL COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) NULL DEFAULT NULL COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeIsMarks` int(11) NULL DEFAULT NULL COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) NULL DEFAULT NULL COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) NULL DEFAULT NULL COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自动套红模板',
  `FK_Flow` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '独立表单属性:FK_Flow',
  `RightViewWay` int(11) NULL DEFAULT NULL COMMENT '报表查看权限控制方式',
  `RightViewTag` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '报表查看权限控制Tag',
  `RightDeptWay` int(11) NULL DEFAULT NULL COMMENT '部门数据查看控制方式',
  `RightDeptTag` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '部门数据查看控制Tag',
  `DBURL` int(11) NULL DEFAULT NULL COMMENT 'DBURL',
  `TemplaterVer` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模版编号',
  `DBSave` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Word数据文件存储',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单模版',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float(11, 2) NULL DEFAULT NULL COMMENT 'MyFileSize',
  `IsEnableJS` int(11) NULL DEFAULT NULL COMMENT '是否启用自定义JS',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统表单' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_mapdata
-- ----------------------------
INSERT INTO `sys_mapdata` VALUES ('ND101', 0, 'ND1Rpt', '填写请假申请单', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, 1, NULL, 1, '', '', '', '', '2020-07-21 14:50:01', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', 500, 760, 0, 0, '', '', 0, ',/WF/Comm/Handler.ashx', '', 900, 1200, '', '', 0, 0, 'local', '', '', 0, '打开本地', 0, '打开模板', 0, '保存', 1, '接受修订', 0, '拒绝修订', 0, '套红按钮', 0, 1, '打印按钮', 0, '签章按钮', 0, '插入流程', 0, 0, 0, '下载', 0, 1, '', 1, 0, '', NULL, 0, NULL, 0, NULL, 0, '', '', '', '', '', '', 0, 0, 0.00, 0);
INSERT INTO `sys_mapdata` VALUES ('ND102', 0, 'ND1Rpt', '部门经理审批', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, 1, NULL, 1, '', '', '', '', '2020-07-21 14:50:01', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', 500, 760, 0, 0, '', '', 0, ',/WF/Comm/Handler.ashx', '', 900, 1200, '', '', 0, 0, 'local', '', '', 0, '打开本地', 0, '打开模板', 0, '保存', 1, '接受修订', 0, '拒绝修订', 0, '套红按钮', 0, 1, '打印按钮', 0, '签章按钮', 0, '插入流程', 0, 0, 0, '下载', 0, 1, '', 1, 0, '', NULL, 0, NULL, 0, NULL, 0, '', '', '', '', '', '', 0, 0, 0.00, 0);
INSERT INTO `sys_mapdata` VALUES ('ND103', 0, 'ND1Rpt', '总经理审批', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, 1, NULL, 1, '', '', '', '', '2020-07-21 14:50:01', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', 500, 760, 0, 0, '', '', 0, ',/WF/Comm/Handler.ashx', '', 900, 1200, '', '', 0, 0, 'local', '', '', 0, '打开本地', 0, '打开模板', 0, '保存', 1, '接受修订', 0, '拒绝修订', 0, '套红按钮', 0, 1, '打印按钮', 0, '签章按钮', 0, '插入流程', 0, 0, 0, '下载', 0, 1, '', 1, 0, '', NULL, 0, NULL, 0, NULL, 0, '', '', '', '', '', '', 0, 0, 0.00, 0);
INSERT INTO `sys_mapdata` VALUES ('ND104', 0, 'ND1Rpt', '反馈给申请人', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, 1, NULL, 1, '', '', '', '', '2020-07-21 14:50:01', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', 500, 760, 0, 0, '', '', 0, ',/WF/Comm/Handler.ashx', '', 900, 1200, '', '', 0, 0, 'local', '', '', 0, '打开本地', 0, '打开模板', 0, '保存', 1, '接受修订', 0, '拒绝修订', 0, '套红按钮', 0, 1, '打印按钮', 0, '签章按钮', 0, '插入流程', 0, 0, 0, '下载', 0, 1, '', 1, 0, '', NULL, 0, NULL, 0, NULL, 0, '', '', '', '', '', '', 0, 0, 0.00, 0);
INSERT INTO `sys_mapdata` VALUES ('ND1Rpt', 1, 'ND1Rpt', '请假流程-经典表单-演示', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, 1, NULL, 1, '', '', '', '', '2020-07-21 14:50:32', '', 100, NULL, NULL, NULL, '@IsHaveCA=0', 500, 760, 0, 0, '', '', 0, '', '', 900, 1200, '', '', 0, 0, 'local', '', '', 0, '打开本地', 0, '打开模板', 0, '保存', 1, '接受修订', 0, '拒绝修订', 0, '套红按钮', 0, 1, '打印按钮', 0, '签章按钮', 0, '插入流程', 0, 0, 0, '下载', 0, 1, '', 1, 0, '', NULL, 0, NULL, 0, NULL, 0, '', '', '', '', '', '', 0, 0, 0.00, 0);

-- ----------------------------
-- Table structure for sys_mapdtl
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapdtl`;
CREATE TABLE `sys_mapdtl`  (
  `No` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `Alias` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '别名',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `PTable` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '存储表',
  `GroupField` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组字段',
  `RefPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联的主键',
  `FEBD` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '事件类实体类',
  `Model` int(11) NULL DEFAULT NULL COMMENT '工作模式,枚举类型:0 普通;1 固定行;',
  `DtlVer` int(11) NULL DEFAULT NULL COMMENT '使用版本,枚举类型:0 2017传统版;',
  `RowsOfList` int(11) NULL DEFAULT NULL COMMENT '初始化行数',
  `IsEnableGroupField` int(11) NULL DEFAULT NULL COMMENT '是否启用分组字段',
  `IsShowSum` int(11) NULL DEFAULT NULL COMMENT '是否显示合计？',
  `IsShowIdx` int(11) NULL DEFAULT NULL COMMENT '是否显示序号？',
  `IsCopyNDData` int(11) NULL DEFAULT NULL COMMENT '是否允许copy节点数据',
  `IsHLDtl` int(11) NULL DEFAULT NULL COMMENT '是否是合流汇总',
  `IsReadonly` int(11) NULL DEFAULT NULL COMMENT '是否只读？',
  `IsShowTitle` int(11) NULL DEFAULT NULL COMMENT '是否显示标题？',
  `IsView` int(11) NULL DEFAULT NULL COMMENT '是否可见？',
  `IsInsert` int(11) NULL DEFAULT NULL COMMENT '是否可以插入行？',
  `IsDelete` int(11) NULL DEFAULT NULL COMMENT '是否可以删除行？',
  `IsUpdate` int(11) NULL DEFAULT NULL COMMENT '是否可以更新？',
  `IsEnablePass` int(11) NULL DEFAULT NULL COMMENT '是否启用通过审核功能?',
  `IsEnableAthM` int(11) NULL DEFAULT NULL COMMENT '是否启用多附件',
  `IsEnableM2M` int(11) NULL DEFAULT NULL COMMENT '是否启用M2M',
  `IsEnableM2MM` int(11) NULL DEFAULT NULL COMMENT '是否启用M2M',
  `WhenOverSize` int(11) NULL DEFAULT NULL COMMENT '超出行数,枚举类型:0 不处理;1 向下顺增行;2 次页显示;',
  `DtlOpenType` int(11) NULL DEFAULT NULL COMMENT '数据开放类型,枚举类型:0 操作员;1 工作ID;2 流程ID;',
  `ListShowModel` int(11) NULL DEFAULT NULL COMMENT '列表数据显示格式,枚举类型:0 表格;1 卡片;',
  `EditModel` int(11) NULL DEFAULT NULL COMMENT '编辑数据方式,枚举类型:0 表格;1 卡片;',
  `MobileShowModel` int(11) NULL DEFAULT NULL COMMENT '移动端数据显示方式,枚举类型:0 新页面显示模式;1 列表模式;',
  `MobileShowField` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移动端列表显示字段',
  `X` float NULL DEFAULT NULL COMMENT '距左',
  `Y` float NULL DEFAULT NULL COMMENT '距上',
  `H` float NULL DEFAULT NULL COMMENT '高度',
  `W` float NULL DEFAULT NULL COMMENT '宽度',
  `FrmW` float NULL DEFAULT NULL COMMENT '表单宽度',
  `FrmH` float NULL DEFAULT NULL COMMENT '表单高度',
  `IsEnableLink` int(11) NULL DEFAULT NULL COMMENT '是否启用超链接',
  `LinkLabel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '超连接标签',
  `LinkTarget` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连接目标',
  `LinkUrl` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连接URL',
  `FilterSQLExp` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '过滤数据SQL表达式',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点(用户独立表单权限控制)',
  `ShowCols` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的列',
  `IsExp` int(11) NULL DEFAULT NULL COMMENT '是否可以导出？(导出到Excel,Txt,html类型文件.)',
  `ImpModel` int(11) NULL DEFAULT NULL COMMENT '导入方式,枚举类型:0 不导入;1 按配置模式导入;2 按照xls文件模版导入;',
  `ImpSQLSearch` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '查询SQL(SQL里必须包含@Key关键字.)',
  `ImpSQLInit` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '初始化SQL(初始化表格的时候的SQL数据,可以为空)',
  `ImpSQLFullOneRow` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '数据填充一行数据的SQL(必须包含@Key关键字,为选择的主键)',
  `ImpSQLNames` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列的中文名称',
  `IsImp` int(11) NULL DEFAULT NULL COMMENT '是否可以导出？',
  `ColAutoExp` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列自动计算表达式',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `AtPara` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  `SubThreadWorker` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子线程处理人字段',
  `SubThreadWorkerText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子线程处理人字段',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '明细' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_mapext
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapext`;
CREATE TABLE `sys_mapext`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主表',
  `ExtType` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `DoWay` int(11) NULL DEFAULT NULL COMMENT '执行方式',
  `AttrOfOper` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作的Attr',
  `AttrsOfActive` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '激活的字段',
  `Doc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `Tag` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag',
  `Tag1` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag5',
  `H` int(11) NULL DEFAULT NULL COMMENT '高度',
  `W` int(11) NULL DEFAULT NULL COMMENT '宽度',
  `DBType` int(11) NULL DEFAULT NULL COMMENT '数据类型',
  `FK_DBSrc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源',
  `PRI` int(11) NULL DEFAULT NULL COMMENT 'PRI/顺序号',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '参数',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '业务逻辑' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_mapframe
-- ----------------------------
DROP TABLE IF EXISTS `sys_mapframe`;
CREATE TABLE `sys_mapframe`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `UrlSrcType` int(11) NULL DEFAULT NULL COMMENT 'URL来源',
  `FrameURL` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FrameURL',
  `URL` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'URL',
  `Y` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Y',
  `X` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'x',
  `W` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '宽度',
  `H` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '高度',
  `IsAutoSize` int(11) NULL DEFAULT NULL COMMENT '是否自动设置大小',
  `EleType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `GUID` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '框架' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_rptdept
-- ----------------------------
DROP TABLE IF EXISTS `sys_rptdept`;
CREATE TABLE `sys_rptdept`  (
  `FK_Rpt` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '报表 - 主键',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Rpt`, `FK_Dept`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '报表部门对应信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_rptemp
-- ----------------------------
DROP TABLE IF EXISTS `sys_rptemp`;
CREATE TABLE `sys_rptemp`  (
  `FK_Rpt` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '报表 - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Rpt`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '报表人员对应信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_rptstation
-- ----------------------------
DROP TABLE IF EXISTS `sys_rptstation`;
CREATE TABLE `sys_rptstation`  (
  `FK_Rpt` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '报表 - 主键',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Rpt`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '报表岗位对应信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_rpttemplate
-- ----------------------------
DROP TABLE IF EXISTS `sys_rpttemplate`;
CREATE TABLE `sys_rpttemplate`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `EnsName` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类名',
  `FK_Emp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作员',
  `D1` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'D1',
  `D2` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'D2',
  `D3` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'D3',
  `AlObjs` varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '要分析的对象',
  `Height` int(11) NULL DEFAULT NULL COMMENT 'Height',
  `Width` int(11) NULL DEFAULT NULL COMMENT 'Width',
  `IsSumBig` int(11) NULL DEFAULT NULL COMMENT '是否显示大合计',
  `IsSumLittle` int(11) NULL DEFAULT NULL COMMENT '是否显示小合计',
  `IsSumRight` int(11) NULL DEFAULT NULL COMMENT '是否显示右合计',
  `PercentModel` int(11) NULL DEFAULT NULL COMMENT '比率显示方式',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '报表模板' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_serial
-- ----------------------------
DROP TABLE IF EXISTS `sys_serial`;
CREATE TABLE `sys_serial`  (
  `CfgKey` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'CfgKey - 主键',
  `IntVal` int(11) NULL DEFAULT NULL COMMENT '属性',
  PRIMARY KEY (`CfgKey`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '序列号' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_serial
-- ----------------------------
INSERT INTO `sys_serial` VALUES ('BP.WF.Template.FlowSort', 102);
INSERT INTO `sys_serial` VALUES ('OID', 122);
INSERT INTO `sys_serial` VALUES ('UpdataCCFlowVer', 509100226);
INSERT INTO `sys_serial` VALUES ('Ver', 20200411);

-- ----------------------------
-- Table structure for sys_sfdbsrc
-- ----------------------------
DROP TABLE IF EXISTS `sys_sfdbsrc`;
CREATE TABLE `sys_sfdbsrc`  (
  `No` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据源编号(必须是英文) - 主键',
  `Name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源名称',
  `DBSrcType` int(11) NULL DEFAULT NULL COMMENT '数据源类型,枚举类型:0 应用系统主数据库(默认);1 SQLServer数据库;2 Oracle数据库;3 MySQL数据库;4 Informix数据库;50 Dubbo服务;100 WebService数据源;',
  `UserID` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据库登录用户ID',
  `Password` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据库登录用户密码',
  `IP` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IP地址/数据库实例名',
  `DBName` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据库名称/Oracle保持为空',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据源' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_sfdbsrc
-- ----------------------------
INSERT INTO `sys_sfdbsrc` VALUES ('local', '本机数据源(默认)', 0, '', '', '', '');

-- ----------------------------
-- Table structure for sys_sftable
-- ----------------------------
DROP TABLE IF EXISTS `sys_sftable`;
CREATE TABLE `sys_sftable`  (
  `No` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '表英文名称 - 主键',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表中文名称',
  `SrcType` int(11) NULL DEFAULT NULL COMMENT '数据表类型,枚举类型:0 本地的类;1 创建表;2 表或视图;3 SQL查询表;4 WebServices;',
  `CodeStruct` int(11) NULL DEFAULT NULL COMMENT '字典表类型,枚举类型:0 普通的编码表(具有No,Name);1 树结构(具有No,Name,ParentNo);2 行政机构编码表(编码以两位编号标识级次树形关系);',
  `FK_Val` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认创建的字段名',
  `TableDesc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表描述',
  `DefVal` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认值',
  `FK_SFDBSrc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源,外键:对应物理表:Sys_SFDBSrc,表描述:数据源',
  `SrcTable` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源表',
  `ColumnValue` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的值(编号列)',
  `ColumnText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的文字(名称列)',
  `ParentValue` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父级值(父级列)',
  `SelectStatement` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '查询语句',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '加入日期',
  `RootVal` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '根节点值',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_sms
-- ----------------------------
DROP TABLE IF EXISTS `sys_sms`;
CREATE TABLE `sys_sms`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Sender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人(可以为空)',
  `SendTo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送给(可以为空)',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '写入时间',
  `Mobile` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号(可以为空)',
  `MobileSta` int(11) NULL DEFAULT NULL COMMENT '消息状态',
  `MobileInfo` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '短信信息',
  `Email` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Email(可以为空)',
  `EmailSta` int(11) NULL DEFAULT NULL COMMENT 'EmaiSta消息状态',
  `EmailTitle` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `EmailDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `SendDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送时间',
  `IsRead` int(11) NULL DEFAULT NULL COMMENT '是否读取?',
  `IsAlert` int(11) NULL DEFAULT NULL COMMENT '是否提示?',
  `MsgFlag` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息标记(用于防止发送重复)',
  `MsgType` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '消息类型(CC抄送,Todolist待办,Return退回,Etc其他消息...)',
  `AtPara` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_userlogt
-- ----------------------------
DROP TABLE IF EXISTS `sys_userlogt`;
CREATE TABLE `sys_userlogt`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Emp` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户',
  `IP` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IP',
  `LogFlag` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识',
  `Docs` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  `RDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户日志' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_userregedit
-- ----------------------------
DROP TABLE IF EXISTS `sys_userregedit`;
CREATE TABLE `sys_userregedit`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `EnsName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体类名称',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户',
  `Attrs` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '属性s',
  `ContrastKey` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对比项目',
  `KeyVal1` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'KeyVal1',
  `KeyVal2` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'KeyVal2',
  `SortBy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortBy',
  `KeyOfNum` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'KeyOfNum',
  `GroupWay` int(11) NULL DEFAULT NULL COMMENT '求什么?SumAvg',
  `OrderWay` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OrderWay',
  `FK_MapData` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实体',
  `AttrKey` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点对应字段',
  `LB` int(11) NULL DEFAULT NULL COMMENT '类别',
  `CurValue` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '文本',
  `CfgKey` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '键',
  `Vals` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '值',
  `GenerSQL` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GenerSQL',
  `Paras` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Paras',
  `NumKey` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分析的Key',
  `OrderBy` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OrderBy',
  `SearchKey` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SearchKey',
  `MVals` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MVals',
  `IsPic` int(11) NULL DEFAULT NULL COMMENT '是否图片',
  `DTFrom` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询时间从',
  `DTTo` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户注册表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for test_data
-- ----------------------------
DROP TABLE IF EXISTS `test_data`;
CREATE TABLE `test_data`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `test_input` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单行文本',
  `test_textarea` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '多行文本',
  `test_select` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下拉框',
  `test_select_multiple` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下拉多选',
  `test_radio` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单选框',
  `test_checkbox` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '复选框',
  `test_date` datetime(0) NULL DEFAULT NULL COMMENT '日期选择',
  `test_datetime` datetime(0) NULL DEFAULT NULL COMMENT '日期时间',
  `test_user_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户选择',
  `test_office_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构选择',
  `test_area_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域选择',
  `test_area_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域名称',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试数据' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for test_data_child
-- ----------------------------
DROP TABLE IF EXISTS `test_data_child`;
CREATE TABLE `test_data_child`  (
  `id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号',
  `test_sort` int(11) NULL DEFAULT NULL COMMENT '排序号',
  `test_data_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父表主键',
  `test_input` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单行文本',
  `test_textarea` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '多行文本',
  `test_select` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下拉框',
  `test_select_multiple` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下拉多选',
  `test_radio` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单选框',
  `test_checkbox` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '复选框',
  `test_date` datetime(0) NULL DEFAULT NULL COMMENT '日期选择',
  `test_datetime` datetime(0) NULL DEFAULT NULL COMMENT '日期时间',
  `test_user_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户选择',
  `test_office_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构选择',
  `test_area_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域选择',
  `test_area_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试数据子表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for test_tree
-- ----------------------------
DROP TABLE IF EXISTS `test_tree`;
CREATE TABLE `test_tree`  (
  `tree_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '节点编码',
  `parent_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '父级编号',
  `parent_codes` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有父级编号',
  `tree_sort` decimal(10, 0) NOT NULL COMMENT '本级排序号（升序）',
  `tree_sorts` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所有级别排序号',
  `tree_leaf` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否最末级',
  `tree_level` decimal(4, 0) NOT NULL COMMENT '层次级别',
  `tree_names` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '全节点名',
  `tree_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '节点名称',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1删除 2停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建者',
  `create_date` datetime(0) NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新者',
  `update_date` datetime(0) NOT NULL COMMENT '更新时间',
  `remarks` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`tree_code`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试树表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_accepterrole
-- ----------------------------
DROP TABLE IF EXISTS `wf_accepterrole`;
CREATE TABLE `wf_accepterrole`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'null',
  `FK_Node` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点',
  `FK_Mode` int(11) NULL DEFAULT NULL COMMENT '模式类型',
  `Tag0` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag0',
  `Tag1` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag5',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '接受人规则' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_athunreadlog
-- ----------------------------
DROP TABLE IF EXISTS `wf_athunreadlog`;
CREATE TABLE `wf_athunreadlog`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门,外键:对应物理表:Port_Dept,表描述:部门',
  `Title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT 'WorkID',
  `FlowStarter` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人',
  `FlowStartRDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起时间',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程,外键:对应物理表:WF_Flow,表描述:流程',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `NodeName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `FK_Emp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员',
  `FK_EmpDept` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员部门',
  `FK_EmpDeptName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员名称',
  `BeiZhu` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `SendDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日期',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件未读日志' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_auth
-- ----------------------------
DROP TABLE IF EXISTS `wf_auth`;
CREATE TABLE `wf_auth`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Auther` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权人',
  `AuthType` int(11) NULL DEFAULT NULL COMMENT '类型(0=全部流程1=指定流程)',
  `EmpNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '委托给人员编号',
  `EmpName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '委托给人员名称',
  `FlowNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程名称',
  `TakeBackDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '取回日期',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '授权' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_bill
-- ----------------------------
DROP TABLE IF EXISTS `wf_bill`;
CREATE TABLE `wf_bill`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT '工作ID',
  `FID` int(11) NULL DEFAULT NULL COMMENT 'FID',
  `FK_Flow` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `FK_BillType` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据类型',
  `Title` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `FK_Starter` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人',
  `StartDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起时间',
  `Url` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Url',
  `FullPath` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FullPath',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印人,外键:对应物理表:Port_Emp,表描述:用户',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印时间',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隶属部门,外键:对应物理表:Port_Dept,表描述:部门',
  `FK_NY` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隶属年月',
  `Emps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Emps',
  `FK_Node` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点',
  `FK_Bill` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_Bill',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_billtemplate
-- ----------------------------
DROP TABLE IF EXISTS `wf_billtemplate`;
CREATE TABLE `wf_billtemplate`  (
  `No` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'No - 主键',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `TempFilePath` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板路径',
  `NodeID` int(11) NULL DEFAULT NULL COMMENT 'NodeID',
  `FK_MapData` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单编号',
  `BillFileType` int(11) NULL DEFAULT NULL COMMENT '生成的文件类型,枚举类型:0 Word;1 PDF;2 Excel(未完成);3 Html(未完成);',
  `BillOpenModel` int(11) NULL DEFAULT NULL COMMENT '生成的文件打开方式,枚举类型:0 下载本地;1 在线WebOffice打开;',
  `QRModel` int(11) NULL DEFAULT NULL COMMENT '二维码生成方式,枚举类型:0 不生成;1 生成二维码;',
  `TemplateFileModel` int(11) NULL DEFAULT NULL COMMENT '模版模式,枚举类型:0 rtf模版;1 vsto模式的word模版;2 vsto模式的excel模版;',
  `Idx` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Idx',
  `MyFrmID` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单编号',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单据模板' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_ccdept
-- ----------------------------
DROP TABLE IF EXISTS `wf_ccdept`;
CREATE TABLE `wf_ccdept`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点,主外键:对应物理表:WF_Node,表描述:节点',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Node`, `FK_Dept`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '抄送部门' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_ccemp
-- ----------------------------
DROP TABLE IF EXISTS `wf_ccemp`;
CREATE TABLE `wf_ccemp`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Node`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '抄送人员' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_cclist
-- ----------------------------
DROP TABLE IF EXISTS `wf_cclist`;
CREATE TABLE `wf_cclist`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `Sta` int(11) NULL DEFAULT NULL COMMENT '状态',
  `FK_Flow` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程名称',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `NodeName` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT '工作ID',
  `FID` int(11) NULL DEFAULT NULL COMMENT 'FID',
  `Doc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `Rec` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送人员',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  `CCTo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送给',
  `CCToName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送给(人员名称)',
  `CCToDept` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送到部门',
  `CCToDeptName` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送给部门名称',
  `CCToOrgNo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送到组织',
  `CCToOrgName` varchar(600) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送给组织名称',
  `CDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开时间',
  `PFlowNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父流程编号',
  `PWorkID` int(11) NULL DEFAULT NULL COMMENT '父流程WorkID',
  `InEmpWorks` int(11) NULL DEFAULT NULL COMMENT '是否加入待办列表',
  `Domain` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Domain',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '抄送列表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_ccstation
-- ----------------------------
DROP TABLE IF EXISTS `wf_ccstation`;
CREATE TABLE `wf_ccstation`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点,主外键:对应物理表:WF_Node,表描述:节点',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '工作岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Node`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '抄送岗位' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_ch
-- ----------------------------
DROP TABLE IF EXISTS `wf_ch`;
CREATE TABLE `wf_ch`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT '工作ID',
  `FID` int(11) NULL DEFAULT NULL COMMENT 'FID',
  `Title` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `FK_FlowT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程名称',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `FK_NodeT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `Sender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人',
  `SenderT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人名称',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当事人',
  `FK_EmpT` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当事人名称',
  `GroupEmps` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关当事人',
  `GroupEmpsNames` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关当事人名称',
  `GroupEmpsNum` int(11) NULL DEFAULT NULL COMMENT '相关当事人数量',
  `DTFrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务下达时间',
  `DTTo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务处理时间',
  `SDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应完成日期',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隶属部门',
  `FK_DeptT` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `FK_NY` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隶属月份',
  `DTSWay` int(11) NULL DEFAULT NULL COMMENT '考核方式,枚举类型:0 不考核;1 按照时效考核;2 按照工作量考核;',
  `TimeLimit` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规定限期',
  `OverMinutes` float NULL DEFAULT NULL COMMENT '逾期分钟',
  `UseDays` float NULL DEFAULT NULL COMMENT '实际使用天',
  `OverDays` float NULL DEFAULT NULL COMMENT '逾期天',
  `CHSta` int(11) NULL DEFAULT NULL COMMENT '状态',
  `WeekNum` int(11) NULL DEFAULT NULL COMMENT '第几周',
  `Points` float NULL DEFAULT NULL COMMENT '总扣分',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '时效考核' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_cheval
-- ----------------------------
DROP TABLE IF EXISTS `wf_cheval`;
CREATE TABLE `wf_cheval`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Title` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `FK_Flow` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程名称',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT '工作ID',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '评价节点',
  `NodeName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '停留节点',
  `Rec` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价人',
  `RecName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价人名称',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价日期',
  `EvalEmpNo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '被考核的人员编号',
  `EvalEmpName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '被考核的人员名称',
  `EvalCent` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价分值',
  `EvalNote` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价内容',
  `FK_Dept` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `DeptName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `FK_NY` varchar(7) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年月',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工作质量评价' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_cond
-- ----------------------------
DROP TABLE IF EXISTS `wf_cond`;
CREATE TABLE `wf_cond`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `CondType` int(11) NULL DEFAULT NULL COMMENT '条件类型',
  `DataFrom` int(11) NULL DEFAULT NULL COMMENT '条件数据来源0表单,1岗位(对方向条件有效)',
  `FK_Flow` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `NodeID` int(11) NULL DEFAULT NULL COMMENT '发生的事件MainNode',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `FK_Attr` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性',
  `AttrKey` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性键',
  `AttrName` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '中文名称',
  `FK_Operator` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运算符号',
  `OperatorValue` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '要运算的值',
  `OperatorValueT` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '要运算的值T',
  `ToNodeID` int(11) NULL DEFAULT NULL COMMENT 'ToNodeID（对方向条件有效）',
  `ConnJudgeWay` int(11) NULL DEFAULT NULL COMMENT '条件关系,枚举类型:0 or;1 and;',
  `MyPOID` int(11) NULL DEFAULT NULL COMMENT 'MyPOID',
  `PRI` int(11) NULL DEFAULT NULL COMMENT '计算优先级',
  `CondOrAnd` int(11) NULL DEFAULT NULL COMMENT '方向条件类型',
  `Note` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `AtPara` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '条件' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_deptflowsearch
-- ----------------------------
DROP TABLE IF EXISTS `wf_deptflowsearch`;
CREATE TABLE `wf_deptflowsearch`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Emp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作员',
  `FK_Flow` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门编号',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程部门数据查询权限' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_direction
-- ----------------------------
DROP TABLE IF EXISTS `wf_direction`;
CREATE TABLE `wf_direction`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `Node` int(11) NULL DEFAULT NULL COMMENT '从节点',
  `ToNode` int(11) NULL DEFAULT NULL COMMENT '到节点',
  `IsCanBack` int(11) NULL DEFAULT NULL COMMENT '是否可以原路返回(对后退线有效)',
  `Dots` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '轨迹信息',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点方向信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wf_direction
-- ----------------------------
INSERT INTO `wf_direction` VALUES ('001_101_102', '001', 101, 102, 0, '');
INSERT INTO `wf_direction` VALUES ('001_102_103', '001', 102, 103, 0, '');
INSERT INTO `wf_direction` VALUES ('001_103_104', '001', 103, 104, 0, '');

-- ----------------------------
-- Table structure for wf_directionstation
-- ----------------------------
DROP TABLE IF EXISTS `wf_directionstation`;
CREATE TABLE `wf_directionstation`  (
  `FK_Direction` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '工作岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Direction`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点岗位' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_emp
-- ----------------------------
DROP TABLE IF EXISTS `wf_emp`;
CREATE TABLE `wf_emp`  (
  `No` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'No - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Name',
  `UseSta` int(11) NULL DEFAULT NULL COMMENT '用户状态0禁用,1正常.',
  `Tel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tel',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_Dept',
  `Email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Email',
  `AlertWay` int(11) NULL DEFAULT NULL COMMENT '收听方式,枚举类型:0 不接收;1 短信;2 邮件;3 内部消息;4 QQ消息;5 RTX消息;6 MSN消息;',
  `Author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权人',
  `AuthorDate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权日期',
  `AuthorWay` int(11) NULL DEFAULT NULL COMMENT '授权方式',
  `AuthorToDate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '授权到日期',
  `AuthorFlows` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '可以执行的授权流程',
  `Stas` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位s',
  `Depts` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Deptss',
  `Msg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Msg',
  `Style` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Style',
  `OrgNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'OrgNo',
  `SPass` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片签名密码',
  `Idx` int(11) NULL DEFAULT NULL COMMENT 'Idx',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  `StartFlows` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作员' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wf_emp
-- ----------------------------
INSERT INTO `wf_emp` VALUES ('admin', 'admin', 1, NULL, NULL, 'zhoupeng@ccflow.org', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `wf_emp` VALUES ('fuhui', '福惠', 1, NULL, NULL, 'fuhui@ccflow.org', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `wf_emp` VALUES ('guobaogeng', '郭宝庚', 1, NULL, NULL, 'guobaogeng@ccflow.org', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `wf_emp` VALUES ('guoxiangbin', '郭祥斌', 1, NULL, NULL, 'guoxiangbin@ccflow.org', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `wf_emp` VALUES ('liping', '李萍', 1, NULL, NULL, 'liping@ccflow.org', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `wf_emp` VALUES ('liyan', '李言', 1, NULL, NULL, 'liyan@ccflow.org', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `wf_emp` VALUES ('qifenglin', '祁凤林', 1, NULL, NULL, 'qifenglin@ccflow.org', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `wf_emp` VALUES ('yangyilei', '杨依雷', 1, NULL, NULL, 'yangyilei@ccflow.org', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `wf_emp` VALUES ('zhanghaicheng', '张海成', 1, NULL, NULL, 'zhanghaicheng@ccflow.org', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `wf_emp` VALUES ('zhangyifan', '张一帆', 1, NULL, NULL, 'zhangyifan@ccflow.org', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `wf_emp` VALUES ('zhoupeng', '周朋', 1, NULL, NULL, 'zhoupeng@ccflow.org', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `wf_emp` VALUES ('zhoushengyu', '周升雨', 1, NULL, NULL, 'zhoushengyu@ccflow.org', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `wf_emp` VALUES ('zhoutianjiao', '周天娇', 1, NULL, NULL, 'zhoutianjiao@ccflow.org', 3, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL);

-- ----------------------------
-- Table structure for wf_findworkerrole
-- ----------------------------
DROP TABLE IF EXISTS `wf_findworkerrole`;
CREATE TABLE `wf_findworkerrole`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Name',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `SortVal0` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortVal0',
  `SortText0` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortText0',
  `SortVal1` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortVal1',
  `SortText1` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortText1',
  `SortVal2` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortText2',
  `SortText2` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortText2',
  `SortVal3` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortVal3',
  `SortText3` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SortText3',
  `TagVal0` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagVal0',
  `TagVal1` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagVal1',
  `TagVal2` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagVal2',
  `TagVal3` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagVal3',
  `TagText0` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagText0',
  `TagText1` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagText1',
  `TagText2` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagText2',
  `TagText3` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'TagText3',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用',
  `Idx` int(11) NULL DEFAULT NULL COMMENT 'IDX',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '找人规则' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_flow
-- ----------------------------
DROP TABLE IF EXISTS `wf_flow`;
CREATE TABLE `wf_flow`  (
  `No` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `FK_FlowSort` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程类别,外键:对应物理表:WF_FlowSort,表描述:流程类别',
  `Name` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `FlowMark` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程标记',
  `FlowEventEntity` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程事件实体',
  `TitleRole` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题生成规则',
  `IsCanStart` int(11) NULL DEFAULT NULL COMMENT '可以独立启动否？(独立启动的流程可以显示在发起流程列表里)',
  `IsFullSA` int(11) NULL DEFAULT NULL COMMENT '是否自动计算未来的处理人？',
  `IsGuestFlow` int(11) NULL DEFAULT NULL COMMENT '是否外部用户参与流程(非组织结构人员参与的流程)',
  `FlowAppType` int(11) NULL DEFAULT NULL COMMENT '流程应用类型,枚举类型:0 业务流程;1 工程类(项目组流程);2 公文流程(VSTO);',
  `Draft` int(11) NULL DEFAULT NULL COMMENT '草稿规则,枚举类型:0 无(不设草稿);1 保存到待办;2 保存到草稿箱;',
  `FlowDeleteRole` int(11) NULL DEFAULT NULL COMMENT '流程实例删除规则,枚举类型:0 超级管理员可以删除;1 分级管理员可以删除;2 发起人可以删除;3 节点启动删除按钮的操作员;',
  `IsToParentNextNode` int(11) NULL DEFAULT NULL COMMENT '子流程结束时，让父流程自动运行到下一步',
  `HelpUrl` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '帮助文档',
  `SysType` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统类型',
  `Tester` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起测试人',
  `NodeAppType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务类型枚举(可为Null)',
  `NodeAppTypeText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务类型枚举(可为Null)',
  `ChartType` int(11) NULL DEFAULT NULL COMMENT '节点图形类型,枚举类型:0 几何图形;1 肖像图片;',
  `HostRun` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运行主机(IP+端口)',
  `IsBatchStart` int(11) NULL DEFAULT NULL COMMENT '是否可以批量发起流程？(如果是就要设置发起的需要填写的字段,多个用逗号分开)',
  `BatchStartFields` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起字段s',
  `HistoryFields` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '历史查看字段',
  `IsResetData` int(11) NULL DEFAULT NULL COMMENT '是否启用开始节点数据重置按钮？',
  `IsLoadPriData` int(11) NULL DEFAULT NULL COMMENT '是否自动装载上一笔数据？',
  `IsDBTemplate` int(11) NULL DEFAULT NULL COMMENT '是否启用数据模版？',
  `IsStartInMobile` int(11) NULL DEFAULT NULL COMMENT '是否可以在手机里启用？(如果发起表单特别复杂就不要在手机里启用了)',
  `IsMD5` int(11) NULL DEFAULT NULL COMMENT '是否是数据加密流程(MD5数据加密防篡改)',
  `DataStoreModel` int(11) NULL DEFAULT NULL COMMENT '流程数据存储模式,枚举类型:0 数据轨迹模式;1 数据合并模式;',
  `PTable` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程数据存储表',
  `FlowNoteExp` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注的表达式',
  `BillNoFormat` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据编号格式',
  `IsFrmEnable` int(11) NULL DEFAULT NULL COMMENT '是否显示表单',
  `IsTruckEnable` int(11) NULL DEFAULT NULL COMMENT '是否显示轨迹图',
  `IsTimeBaseEnable` int(11) NULL DEFAULT NULL COMMENT '是否显示时间轴',
  `IsTableEnable` int(11) NULL DEFAULT NULL COMMENT '是否显示时间表',
  `IsOPEnable` int(11) NULL DEFAULT NULL COMMENT '是否显示操作',
  `DesignerNo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设计者编号',
  `DesignerName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '设计者名称',
  `DesignTime` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建时间',
  `Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '流程描述',
  `FlowRunWay` int(11) NULL DEFAULT NULL COMMENT '启动方式,枚举类型:0 手工启动;1 指定人员按时启动;2 数据集按时启动;3 触发式启动;',
  `RunObj` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '运行内容',
  `RunSQL` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程结束执行后执行的SQL',
  `NumOfBill` int(11) NULL DEFAULT NULL COMMENT '是否有单据',
  `NumOfDtl` int(11) NULL DEFAULT NULL COMMENT 'NumOfDtl',
  `AvgDay` double NULL DEFAULT NULL COMMENT '平均运行用天',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序号(在发起列表中)',
  `SDTOfFlowRole` int(11) NULL DEFAULT NULL COMMENT '流程计划完成日期计算规则',
  `SDTOfFlowRoleSQL` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程计划完成日期计算规则SQL',
  `Paras` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `FlowFrmType` int(11) NULL DEFAULT NULL COMMENT '流程表单类型',
  `FrmUrl` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单Url',
  `DRCtrlType` int(11) NULL DEFAULT NULL COMMENT '部门查询权限控制方式',
  `StartLimitRole` int(11) NULL DEFAULT NULL COMMENT '启动限制规则,枚举类型:0 不限制;1 每人每天一次;2 每人每周一次;3 每人每月一次;4 每人每季一次;5 每人每年一次;6 发起的列不能重复,(多个列可以用逗号分开);7 设置的SQL数据源为空,或者返回结果为零时可以启动.;8 设置的SQL数据源为空,或者返回结果为零时不可以启动.;',
  `StartLimitPara` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规则参数',
  `StartLimitAlert` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '限制提示',
  `StartLimitWhen` int(11) NULL DEFAULT NULL COMMENT '提示时间',
  `StartGuideWay` int(11) NULL DEFAULT NULL COMMENT '前置导航方式,枚举类型:0 无;1 按系统的URL-(父子流程)单条模式;2 按系统的URL-(子父流程)多条模式;3 按系统的URL-(实体记录,未完成)单条模式;4 按系统的URL-(实体记录,未完成)多条模式;5 从开始节点Copy数据;10 按自定义的Url;11 按用户输入参数;',
  `StartGuideLink` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '右侧的连接',
  `StartGuideLab` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连接标签',
  `StartGuidePara1` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数1',
  `StartGuidePara2` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数2',
  `StartGuidePara3` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数3',
  `IsAutoSendSubFlowOver` int(11) NULL DEFAULT NULL COMMENT '(当前节点为子流程时)是否检查所有子流程完成后父流程自动发送',
  `Ver` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '版本号',
  `AtPara` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  `DTSWay` int(11) NULL DEFAULT NULL COMMENT '同步方式,枚举类型:0 不考核;1 按照时效考核;2 按照工作量考核;',
  `DTSDBSrc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据库,外键:对应物理表:Sys_SFDBSrc,表描述:数据源',
  `DTSBTable` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务表名',
  `DTSBTablePK` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务表主键',
  `DTSTime` int(11) NULL DEFAULT NULL COMMENT '执行同步时间点,枚举类型:0 所有的节点发送后;1 指定的节点发送后;2 当流程结束时;',
  `DTSSpecNodes` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定的节点ID',
  `DTSField` int(11) NULL DEFAULT NULL COMMENT '要同步的字段计算方式,枚举类型:0 字段名相同;1 按设置的字段匹配;',
  `DTSFields` varchar(900) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '要同步的字段s,中间用逗号分开.',
  `MyDeptRole` int(11) NULL DEFAULT NULL COMMENT '本部门发起的流程,枚举类型:0 仅部门领导可以查看;1 部门下所有的人都可以查看;2 本部门里指定岗位的人可以查看;',
  `PStarter` int(11) NULL DEFAULT NULL COMMENT '发起人可看(必选)',
  `PWorker` int(11) NULL DEFAULT NULL COMMENT '参与人可看(必选)',
  `PCCer` int(11) NULL DEFAULT NULL COMMENT '被抄送人可看(必选)',
  `PMyDept` int(11) NULL DEFAULT NULL COMMENT '本部门人可看',
  `PPMyDept` int(11) NULL DEFAULT NULL COMMENT '直属上级部门可看(比如:我是)',
  `PPDept` int(11) NULL DEFAULT NULL COMMENT '上级部门可看',
  `PSameDept` int(11) NULL DEFAULT NULL COMMENT '平级部门可看',
  `PSpecDept` int(11) NULL DEFAULT NULL COMMENT '指定部门可看',
  `PSpecDeptExt` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门编号',
  `PSpecSta` int(11) NULL DEFAULT NULL COMMENT '指定的岗位可看',
  `PSpecStaExt` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位编号',
  `PSpecGroup` int(11) NULL DEFAULT NULL COMMENT '指定的权限组可看',
  `PSpecGroupExt` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限组',
  `PSpecEmp` int(11) NULL DEFAULT NULL COMMENT '指定的人员可看',
  `PSpecEmpExt` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定的人员编号',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程模版主表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wf_flow
-- ----------------------------
INSERT INTO `wf_flow` VALUES ('001', '100', '请假流程-经典表单-演示', '', '', '', 1, 0, 0, 0, 0, 0, 0, NULL, '', NULL, NULL, NULL, 1, '', 0, '', '', 0, 0, 0, 1, 0, 1, '', '', '', 1, 1, 1, 1, 1, NULL, NULL, NULL, '', 0, '', '', 0, 0, 0, 0, 0, '', '@StartNodeX=200@StartNodeY=50@EndNodeX=200@EndNodeY=350', 0, '', 0, 0, '', '', 0, 0, '', '', '', '', '', 0, '2020-07-21 14:49', '', 0, '', '', '', 0, '', 0, '', 0, 1, 1, 1, 1, 1, 1, 1, 1, '', 1, '', 1, '', 1, '');

-- ----------------------------
-- Table structure for wf_floworg
-- ----------------------------
DROP TABLE IF EXISTS `wf_floworg`;
CREATE TABLE `wf_floworg`  (
  `FlowNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '流程 - 主键',
  `OrgNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '到组织,主外键:对应物理表:Port_Org,表描述:独立组织',
  PRIMARY KEY (`FlowNo`, `OrgNo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程对应组织' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_flowsort
-- ----------------------------
DROP TABLE IF EXISTS `wf_flowsort`;
CREATE TABLE `wf_flowsort`  (
  `No` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `ParentNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父节点No',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `OrgNo` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织编号(0为系统组织)',
  `Domain` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '域/系统编号',
  `Idx` int(11) NULL DEFAULT NULL COMMENT 'Idx',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程类别' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wf_flowsort
-- ----------------------------
INSERT INTO `wf_flowsort` VALUES ('1', '0', '流程树', '0', '', 0);
INSERT INTO `wf_flowsort` VALUES ('100', '1', '日常办公类', '0', '', 0);
INSERT INTO `wf_flowsort` VALUES ('101', '1', '财务类', '0', '', 0);
INSERT INTO `wf_flowsort` VALUES ('102', '1', '人力资源类', '0', '', 0);

-- ----------------------------
-- Table structure for wf_frmnode
-- ----------------------------
DROP TABLE IF EXISTS `wf_frmnode`;
CREATE TABLE `wf_frmnode`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Frm` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点编号',
  `FK_Flow` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `OfficeOpenLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOverLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeOverEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeMarksEnable` int(11) NULL DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印按钮标签',
  `OfficePrintEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeSealLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeSealEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeNodeInfo` int(11) NULL DEFAULT NULL COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) NULL DEFAULT NULL COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeIsMarks` int(11) NULL DEFAULT NULL COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) NULL DEFAULT NULL COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) NULL DEFAULT NULL COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自动套红模板',
  `FrmSln` int(11) NULL DEFAULT NULL COMMENT '表单控制方案',
  `FrmType` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单类型',
  `IsPrint` int(11) NULL DEFAULT NULL COMMENT '是否可以打印',
  `IsEnableLoadData` int(11) NULL DEFAULT NULL COMMENT '是否启用装载填充事件',
  `IsDefaultOpen` int(11) NULL DEFAULT NULL COMMENT '是否默认打开',
  `IsCloseEtcFrm` int(11) NULL DEFAULT NULL COMMENT '打开时是否关闭其它的页面？',
  `IsEnableFWC` int(11) NULL DEFAULT NULL COMMENT '是否启用审核组件？',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `WhoIsPK` int(11) NULL DEFAULT NULL COMMENT '谁是主键？',
  `Is1ToN` int(11) NULL DEFAULT NULL COMMENT '是否1变N？',
  `HuiZong` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子线程要汇总的数据表',
  `FrmEnableRole` int(11) NULL DEFAULT NULL COMMENT '表单启用规则',
  `FrmEnableExp` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '启用的表达式',
  `TempleteFile` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模版文件',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否显示',
  `GuanJianZiDuan` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关键字段',
  `CheckField` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签批字段',
  `CheckFieldText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签批字段',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点表单' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_generworkerlist
-- ----------------------------
DROP TABLE IF EXISTS `wf_generworkerlist`;
CREATE TABLE `wf_generworkerlist`  (
  `WorkID` int(11) NOT NULL COMMENT '工作ID - 主键',
  `FK_Emp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '人员 - 主键',
  `FK_Node` int(11) NOT NULL COMMENT '节点ID - 主键',
  `FID` int(11) NULL DEFAULT NULL COMMENT '流程ID',
  `FK_EmpText` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员名称',
  `FK_NodeText` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `FK_Flow` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用部门',
  `SDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应完成日期',
  `DTOfWarning` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '警告日期',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录时间',
  `CDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '完成时间',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否可用',
  `IsRead` int(11) NULL DEFAULT NULL COMMENT '是否读取',
  `IsPass` int(11) NULL DEFAULT NULL COMMENT '是否通过(对合流节点有效)',
  `WhoExeIt` int(11) NULL DEFAULT NULL COMMENT '谁执行它',
  `Sender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人',
  `PRI` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  `PressTimes` int(11) NULL DEFAULT NULL COMMENT '催办次数',
  `DTOfHungUp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '挂起时间',
  `DTOfUnHungUp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预计解除挂起时间',
  `HungUpTimes` int(11) NULL DEFAULT NULL COMMENT '挂起次数',
  `GuestNo` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外部用户编号',
  `GuestName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '外部用户名称',
  `AtPara` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'AtPara',
  PRIMARY KEY (`WorkID`, `FK_Emp`, `FK_Node`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '工作者' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_generworkflow
-- ----------------------------
DROP TABLE IF EXISTS `wf_generworkflow`;
CREATE TABLE `wf_generworkflow`  (
  `WorkID` int(11) NOT NULL COMMENT 'WorkID - 主键',
  `FID` int(11) NULL DEFAULT NULL COMMENT '流程ID',
  `FK_FlowSort` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程类别',
  `SysType` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统类别',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `FlowName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程名称',
  `Title` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `WFSta` int(11) NULL DEFAULT NULL COMMENT '状态,枚举类型:0 运行中;1 已完成;2 其他;',
  `WFState` int(11) NULL DEFAULT NULL COMMENT '流程状态,枚举类型:0 空白;1 草稿;2 运行中;3 已完成;4 挂起;5 退回;6 转发;7 删除;8 加签;9 冻结;10 批处理;11 加签回复状态;',
  `Starter` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人',
  `StarterName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人名称',
  `Sender` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送人',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录日期',
  `SendDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程活动时间',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `NodeName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `DeptName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `PRI` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `SDTOfNode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点应完成时间',
  `SDTOfFlow` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程应完成时间',
  `SDTOfFlowWarning` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程预警时间',
  `PFlowNo` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父流程编号',
  `PWorkID` int(11) NULL DEFAULT NULL COMMENT '父流程ID',
  `PNodeID` int(11) NULL DEFAULT NULL COMMENT '父流程调用节点',
  `PFID` int(11) NULL DEFAULT NULL COMMENT '父流程调用的PFID',
  `PEmp` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子流程的调用人',
  `GuestNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户编号',
  `GuestName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户名称',
  `BillNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据编号',
  `FlowNote` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '备注',
  `TodoEmps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '待办人员',
  `TodoEmpsNum` int(11) NULL DEFAULT NULL COMMENT '待办人员数量',
  `TaskSta` int(11) NULL DEFAULT NULL COMMENT '共享状态',
  `AtPara` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数(流程运行设置临时存储的参数)',
  `Emps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '参与人',
  `GUID` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'GUID',
  `FK_NY` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年月',
  `WeekNum` int(11) NULL DEFAULT NULL COMMENT '周次',
  `TSpan` int(11) NULL DEFAULT NULL COMMENT '时间间隔',
  `TodoSta` int(11) NULL DEFAULT NULL COMMENT '待办状态',
  `Domain` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '域/系统编号',
  `PrjNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'PrjNo',
  `PrjName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'PrjNo',
  PRIMARY KEY (`WorkID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程实例' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_hungup
-- ----------------------------
DROP TABLE IF EXISTS `wf_hungup`;
CREATE TABLE `wf_hungup`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT 'WorkID',
  `HungUpWay` int(11) NULL DEFAULT NULL COMMENT '挂起方式,枚举类型:0 无限挂起;1 按指定的时间解除挂起并通知我自己;2 按指定的时间解除挂起并通知所有人;',
  `Note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '挂起原因(标题与内容支持变量)',
  `Rec` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '挂起人',
  `DTOfHungUp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '挂起时间',
  `DTOfUnHungUp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实际解除挂起时间',
  `DTOfUnHungUpPlan` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预计解除挂起时间',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '挂起' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_labnote
-- ----------------------------
DROP TABLE IF EXISTS `wf_labnote`;
CREATE TABLE `wf_labnote`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Name` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'null',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `X` int(11) NULL DEFAULT NULL COMMENT 'X坐标',
  `Y` int(11) NULL DEFAULT NULL COMMENT 'Y坐标',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标签' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_node
-- ----------------------------
DROP TABLE IF EXISTS `wf_node`;
CREATE TABLE `wf_node`  (
  `NodeID` int(11) NOT NULL COMMENT 'NodeID - 主键',
  `Step` int(11) NULL DEFAULT NULL COMMENT '步骤',
  `FK_Flow` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FlowName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程名',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `Tip` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作提示',
  `WhoExeIt` int(11) NULL DEFAULT NULL COMMENT '谁执行它,枚举类型:0 操作员执行;1 机器执行;2 混合执行;',
  `ReadReceipts` int(11) NULL DEFAULT NULL COMMENT '已读回执,枚举类型:0 不回执;1 自动回执;2 由上一节点表单字段决定;3 由SDK开发者参数决定;',
  `CondModel` int(11) NULL DEFAULT NULL COMMENT '方向条件控制规则,枚举类型:0 由连接线条件控制;1 按照用户选择计算;2 发送按钮旁下拉框选择;',
  `CancelRole` int(11) NULL DEFAULT NULL COMMENT '撤销规则,枚举类型:0 上一步可以撤销;1 不能撤销;2 上一步与开始节点可以撤销;3 指定的节点可以撤销;',
  `CancelDisWhenRead` int(11) NULL DEFAULT NULL COMMENT '对方已经打开就不能撤销',
  `IsTask` int(11) NULL DEFAULT NULL COMMENT '允许分配工作否?',
  `IsExpSender` int(11) NULL DEFAULT NULL COMMENT '本节点接收人不允许包含上一步发送人',
  `IsRM` int(11) NULL DEFAULT NULL COMMENT '是否启用投递路径自动记忆功能?',
  `IsOpenOver` int(11) NULL DEFAULT NULL COMMENT '已阅即完成?',
  `IsToParentNextNode` int(11) NULL DEFAULT NULL COMMENT '子流程运行到该节点时，让父流程自动运行到下一步',
  `IsYouLiTai` int(11) NULL DEFAULT NULL COMMENT '该节点是否是游离态',
  `DTFrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生命周期从',
  `DTTo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生命周期到',
  `IsBUnit` int(11) NULL DEFAULT NULL COMMENT '是否是节点模版（业务单元）?',
  `FocusField` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '焦点字段',
  `IsGuestNode` int(11) NULL DEFAULT NULL COMMENT '是否是外部用户执行的节点(非组织结构人员参与处理工作的节点)?',
  `NodeAppType` int(11) NULL DEFAULT NULL COMMENT '节点业务类型',
  `FWCSta` int(11) NULL DEFAULT NULL COMMENT '审核组件状态,枚举类型:0 禁用;1 启用;2 只读;',
  `FWCAth` int(11) NULL DEFAULT NULL COMMENT '附件上传,枚举类型:0 不启用;1 多附件;2 单附件(暂不支持);3 图片附件(暂不支持);',
  `SelfParas` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义属性',
  `RunModel` int(11) NULL DEFAULT NULL COMMENT '运行模式',
  `SubThreadType` int(11) NULL DEFAULT NULL COMMENT '子线程类型,枚举类型:0 同表单;1 异表单;',
  `PassRate` double NULL DEFAULT NULL COMMENT '完成通过率',
  `SubFlowStartWay` int(11) NULL DEFAULT NULL COMMENT '子线程启动方式,枚举类型:0 不启动;1 指定的字段启动;2 按明细表启动;',
  `SubFlowStartParas` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '启动参数',
  `ThreadIsCanDel` int(11) NULL DEFAULT NULL COMMENT '是否可以删除子线程(当前节点已经发送出去的线程，并且当前节点是分流，或者分合流有效，在子线程退回后的操作)？',
  `ThreadIsCanShift` int(11) NULL DEFAULT NULL COMMENT '是否可以移交子线程(当前节点已经发送出去的线程，并且当前节点是分流，或者分合流有效，在子线程退回后的操作)？',
  `IsAllowRepeatEmps` int(11) NULL DEFAULT NULL COMMENT '是否允许子线程接受人员重复(仅当分流点向子线程发送时有效)?',
  `AutoRunEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用自动运行？(仅当分流点向子线程发送时有效)',
  `AutoRunParas` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自动运行SQL',
  `IsSendBackNode` int(11) NULL DEFAULT NULL COMMENT '是否是发送返回节点(发送当前节点,自动发送给该节点的发送人,发送节点.)?',
  `AutoJumpRole0` int(11) NULL DEFAULT NULL COMMENT '处理人就是发起人',
  `AutoJumpRole1` int(11) NULL DEFAULT NULL COMMENT '处理人已经出现过',
  `AutoJumpRole2` int(11) NULL DEFAULT NULL COMMENT '处理人与上一步相同',
  `WhenNoWorker` int(11) NULL DEFAULT NULL COMMENT '(是)找不到人就跳转,(否)提示错误.',
  `SendLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送按钮标签',
  `SendJS` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮JS函数',
  `SaveLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存按钮标签',
  `SaveEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `ThreadLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子线程按钮标签',
  `ThreadEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `ThreadKillRole` int(11) NULL DEFAULT NULL COMMENT '子线程删除方式,枚举类型:0 不能删除;1 手工删除;2 自动删除;',
  `JumpWayLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '跳转按钮标签',
  `JumpWay` int(11) NULL DEFAULT NULL COMMENT '跳转规则,枚举类型:0 不能跳转;1 只能向后跳转;2 只能向前跳转;3 任意节点跳转;4 按指定规则跳转;',
  `JumpToNodes` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '可跳转的节点',
  `ReturnLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回按钮标签',
  `ReturnRole` int(11) NULL DEFAULT NULL COMMENT '退回规则,枚举类型:0 不能退回;1 只能退回上一个节点;2 可退回以前任意节点;3 可退回指定的节点;4 由流程图设计的退回路线决定;',
  `ReturnAlert` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '被退回后信息提示',
  `IsBackTracking` int(11) NULL DEFAULT NULL COMMENT '是否可以原路返回(启用退回功能才有效)',
  `ReturnField` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回信息填写字段',
  `ReturnReasonsItems` varchar(999) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回原因',
  `ReturnOneNodeRole` int(11) NULL DEFAULT NULL COMMENT '单节点退回规则,枚举类型:0 不启用;1 按照[退回信息填写字段]作为退回意见直接退回;2 按照[审核组件]填写的信息作为退回意见直接退回;',
  `CCLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送按钮标签',
  `CCRole` int(11) NULL DEFAULT NULL COMMENT '抄送规则,枚举类型:0 不能抄送;1 手工抄送;2 自动抄送;3 手工与自动;4 按表单SysCCEmps字段计算;',
  `CCWriteTo` int(11) NULL DEFAULT NULL COMMENT '抄送写入规则,枚举类型:0 写入抄送列表;1 写入待办;2 写入待办与抄送列表;',
  `DoOutTime` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '超时处理内容',
  `DoOutTimeCond` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '执行超时的条件',
  `ShiftLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交按钮标签',
  `ShiftEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `DelLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除按钮标签',
  `DelEnable` int(11) NULL DEFAULT NULL COMMENT '删除规则,枚举类型:0 不能删除;1 逻辑删除;2 记录日志方式删除;3 彻底删除;4 让用户决定删除方式;',
  `EndFlowLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结束流程按钮标签',
  `EndFlowEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `ShowParentFormLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查看父流程按钮标签',
  `ShowParentFormEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeBtnLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公文按钮标签',
  `OfficeBtnEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `PrintHtmlLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印Html标签',
  `PrintHtmlEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `PrintPDFLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印pdf标签',
  `PrintPDFEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `PrintPDFModle` int(11) NULL DEFAULT NULL COMMENT 'PDF打印规则,枚举类型:0 全部打印;1 单个表单打印(针对树形表单);',
  `ShuiYinModle` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印水印规则',
  `PrintZipLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打包下载zip按钮标签',
  `PrintZipEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `PrintDocLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印单据按钮标签',
  `PrintDocEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `TrackLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '轨迹按钮标签',
  `TrackEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `HungLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '挂起按钮标签',
  `HungEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `SearchLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '查询按钮标签',
  `SearchEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `HuiQianLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '会签标签',
  `HuiQianRole` int(11) NULL DEFAULT NULL COMMENT '会签模式,枚举类型:0 不启用;1 协作(同事)模式;4 组长(领导)模式;',
  `HuiQianLeaderRole` int(11) NULL DEFAULT NULL COMMENT '组长会签规则,枚举类型:0 只有一个组长;1 最后一个组长发送;2 任意组长可以发送;',
  `AddLeaderLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '加主持人',
  `AddLeaderEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `TCLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流转自定义',
  `TCEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `WebOffice` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文档按钮标签',
  `WebOfficeEnable` int(11) NULL DEFAULT NULL COMMENT '文档启用方式',
  `PRILab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '重要性',
  `PRIEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `CHLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点时限',
  `CHRole` int(11) NULL DEFAULT NULL COMMENT '时限规则,枚举类型:0 禁用;1 启用;2 只读;3 启用并可以调整流程应完成时间;',
  `AllotLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分配按钮标签',
  `AllotEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `FocusLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关注',
  `FocusEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `ConfirmLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '确认按钮标签',
  `ConfirmEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `ListLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列表按钮标签',
  `ListEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `BatchLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '批量审核标签',
  `BatchEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `NoteLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注标签',
  `NoteEnable` int(11) NULL DEFAULT NULL COMMENT '启用规则,枚举类型:0 禁用;1 启用;2 只读;',
  `HelpLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '帮助标签',
  `HelpRole` int(11) NULL DEFAULT NULL COMMENT '帮助显示规则,枚举类型:0 禁用;1 启用;2 强制提示;3 选择性提示;',
  `TAlertRole` int(11) NULL DEFAULT NULL COMMENT '逾期提醒规则',
  `TAlertWay` int(11) NULL DEFAULT NULL COMMENT '逾期提醒方式',
  `WAlertRole` int(11) NULL DEFAULT NULL COMMENT '预警提醒规则',
  `WAlertWay` int(11) NULL DEFAULT NULL COMMENT '预警提醒方式',
  `TCent` float NULL DEFAULT NULL COMMENT '扣分(每延期1小时)',
  `CHWay` int(11) NULL DEFAULT NULL COMMENT '考核方式',
  `IsEval` int(11) NULL DEFAULT NULL COMMENT '是否工作质量考核',
  `OutTimeDeal` int(11) NULL DEFAULT NULL COMMENT '超时处理方式',
  `CCIsAttr` int(11) NULL DEFAULT NULL COMMENT '按表单字段抄送',
  `CCFormAttr` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送人员字段',
  `CCIsStations` int(11) NULL DEFAULT NULL COMMENT '是否启用？-按照岗位抄送',
  `CCStaWay` int(11) NULL DEFAULT NULL COMMENT '抄送岗位计算方式,枚举类型:0 仅按岗位计算;1 按岗位智能计算(当前节点);2 按岗位智能计算(发送到节点);3 按岗位与部门的交集;4 按直线上级部门找岗位下的人员(当前节点);5 按直线上级部门找岗位下的人员(接受节点);',
  `CCIsDepts` int(11) NULL DEFAULT NULL COMMENT '是否启用？-按照部门抄送',
  `CCIsEmps` int(11) NULL DEFAULT NULL COMMENT '是否启用？-按照人员抄送',
  `CCIsSQLs` int(11) NULL DEFAULT NULL COMMENT '是否启用？-按照SQL抄送',
  `CCSQL` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SQL表达式',
  `CCTitle` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '抄送标题',
  `CCDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '抄送内容(标题与内容支持变量)',
  `FWCLab` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示标签',
  `FWCShowModel` int(11) NULL DEFAULT NULL COMMENT '显示方式,枚举类型:0 表格方式;1 自由模式;',
  `FWCType` int(11) NULL DEFAULT NULL COMMENT '审核组件,枚举类型:0 审核组件;1 日志组件;2 周报组件;3 月报组件;',
  `FWCNodeName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点意见名称',
  `FWCTrackEnable` int(11) NULL DEFAULT NULL COMMENT '轨迹图是否显示？',
  `FWCListEnable` int(11) NULL DEFAULT NULL COMMENT '历史审核信息是否显示？(否,仅出现意见框)',
  `FWCIsShowAllStep` int(11) NULL DEFAULT NULL COMMENT '在轨迹表里是否显示所有的步骤？',
  `FWCOpLabel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作名词(审核/审阅/批示)',
  `FWCDefInfo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认审核信息',
  `SigantureEnabel` int(11) NULL DEFAULT NULL COMMENT '操作人是否显示为图片签名？',
  `FWCIsFullInfo` int(11) NULL DEFAULT NULL COMMENT '如果用户未审核是否按照默认意见填充？',
  `FWC_X` float(11, 2) NULL DEFAULT NULL COMMENT '位置X',
  `FWC_Y` float(11, 2) NULL DEFAULT NULL COMMENT '位置Y',
  `FWC_H` float(11, 2) NULL DEFAULT NULL COMMENT '高度(0=100%)',
  `FWC_W` float(11, 2) NULL DEFAULT NULL COMMENT '宽度(0=100%)',
  `FWCFields` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审批格式字段',
  `FWCNewDuanYu` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自定义常用短语(使用@分隔)',
  `FWCIsShowTruck` int(11) NULL DEFAULT NULL COMMENT '是否显示未审核的轨迹？',
  `FWCIsShowReturnMsg` int(11) NULL DEFAULT NULL COMMENT '是否显示退回信息？',
  `FWCOrderModel` int(11) NULL DEFAULT NULL COMMENT '协作模式下操作员显示顺序,枚举类型:0 按审批时间先后排序;1 按照接受人员列表先后顺序(官职大小);',
  `FWCMsgShow` int(11) NULL DEFAULT NULL COMMENT '审核意见显示方式,枚举类型:0 都显示;1 仅显示自己的意见;',
  `FWCVer` int(11) NULL DEFAULT NULL COMMENT '审核意见版本号,枚举类型:0 2018;1 2019;',
  `CheckField` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签批字段',
  `CheckFieldText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签批字段',
  `FWCView` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核意见立场',
  `CheckNodes` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工作节点s',
  `DeliveryWay` int(11) NULL DEFAULT NULL COMMENT '访问规则',
  `ICON` varchar(70) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点ICON图片路径',
  `NodeWorkType` int(11) NULL DEFAULT NULL COMMENT '节点类型',
  `FrmAttr` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FrmAttr',
  `TimeLimit` float(11, 2) NULL DEFAULT NULL COMMENT '限期(天)',
  `TWay` int(11) NULL DEFAULT NULL COMMENT '时间计算方式',
  `WarningDay` float(11, 2) NULL DEFAULT NULL COMMENT '工作预警(天)',
  `Doc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `DeliveryParas` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访问规则设置',
  `NodeFrmID` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点表单ID',
  `SaveModel` int(11) NULL DEFAULT NULL COMMENT '保存模式',
  `IsCanDelFlow` int(11) NULL DEFAULT NULL COMMENT '是否可以删除流程',
  `TodolistModel` int(11) NULL DEFAULT NULL COMMENT '多人处理规则',
  `TeamLeaderConfirmRole` int(11) NULL DEFAULT NULL COMMENT '组长确认规则',
  `TeamLeaderConfirmDoc` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组长确认设置内容',
  `IsHandOver` int(11) NULL DEFAULT NULL COMMENT '是否可以移交',
  `BlockModel` int(11) NULL DEFAULT NULL COMMENT '阻塞模式',
  `BlockExp` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '阻塞表达式',
  `BlockAlert` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '被阻塞提示信息',
  `SFActiveFlows` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '可触发的子流程编号(多个用逗号分开)',
  `BatchRole` int(11) NULL DEFAULT NULL COMMENT '批处理',
  `BatchListCount` int(11) NULL DEFAULT NULL COMMENT '批处理数量',
  `BatchParas` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `FormType` int(11) NULL DEFAULT NULL COMMENT '表单类型',
  `FormUrl` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单URL',
  `TurnToDeal` int(11) NULL DEFAULT NULL COMMENT '转向处理',
  `TurnToDealDoc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送后提示信息',
  `NodePosType` int(11) NULL DEFAULT NULL COMMENT '位置',
  `IsCCFlow` int(11) NULL DEFAULT NULL COMMENT '是否有流程完成条件',
  `HisStas` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位',
  `HisDeptStrs` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `HisToNDs` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '转到的节点',
  `HisBillIDs` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单据IDs',
  `HisSubFlows` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'HisSubFlows',
  `PTable` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '物理表',
  `GroupStaNDs` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位分组节点',
  `X` int(11) NULL DEFAULT NULL COMMENT 'X坐标',
  `Y` int(11) NULL DEFAULT NULL COMMENT 'Y坐标',
  `RefOneFrmTreeType` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '独立表单类型',
  `AtPara` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  `SF_H` float(11, 2) NULL DEFAULT NULL COMMENT '高度',
  `SF_W` float(11, 2) NULL DEFAULT NULL COMMENT '宽度',
  `SelectAccepterLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接受人按钮标签',
  `SelectAccepterEnable` int(11) NULL DEFAULT NULL COMMENT '方式,枚举类型:0 不启用;1 单独启用;2 在发送前打开;3 转入新页面;',
  `WorkCheckLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审核按钮标签',
  `WorkCheckEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `AskforLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '加签标签',
  `AskforEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOpenTemplateLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开模板标签',
  `OfficeOpenTemplateEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeSaveLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存标签',
  `OfficeSaveEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeAcceptLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接受修订标签',
  `OfficeAcceptEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeRefuseLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeRefuseEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeOverLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套红标签',
  `OfficeOverEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeMarksEnable` int(11) NULL DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficePrintLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印标签',
  `OfficePrintEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeSealLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签章标签',
  `OfficeSealEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeInsertFlowLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插入流程标签',
  `OfficeInsertFlowEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeNodeInfo` int(11) NULL DEFAULT NULL COMMENT '是否记录节点信息',
  `OfficeReSavePDF` int(11) NULL DEFAULT NULL COMMENT '是否该自动保存为PDF',
  `OfficeDownLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '下载按钮标签',
  `OfficeDownEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `OfficeIsMarks` int(11) NULL DEFAULT NULL COMMENT '是否进入留痕模式',
  `OfficeTemplate` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '指定文档模板',
  `OfficeIsParent` int(11) NULL DEFAULT NULL COMMENT '是否使用父流程的文档',
  `OfficeTHEnable` int(11) NULL DEFAULT NULL COMMENT '是否自动套红',
  `OfficeTHTemplate` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '自动套红模板',
  `OfficeOpen` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开本地标签',
  `OfficeOpenTemplate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打开模板标签',
  `OfficeSave` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存标签',
  `OfficeAccept` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接受修订标签',
  `OfficeRefuse` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拒绝修订标签',
  `OfficeOver` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '套红按钮标签',
  `OfficeMarks` int(11) NULL DEFAULT NULL COMMENT '是否查看用户留痕',
  `OfficeReadOnly` int(11) NULL DEFAULT NULL COMMENT '是否只读',
  `OfficePrint` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打印按钮标签',
  `OfficeSeal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签章按钮标签',
  `OfficeInsertFlow` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插入流程标签',
  `OfficeIsTrueTH` int(11) NULL DEFAULT NULL COMMENT '是否自动套红',
  `WebOfficeFrmModel` int(11) NULL DEFAULT NULL COMMENT '表单工作方式,枚举类型:',
  `SFLab` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示标签',
  `SFSta` int(11) NULL DEFAULT NULL COMMENT '组件状态,枚举类型:0 禁用;1 启用;2 只读;',
  `SFShowModel` int(11) NULL DEFAULT NULL COMMENT '显示方式,枚举类型:0 表格方式;1 自由模式;',
  `SFCaption` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连接标题',
  `SFDefInfo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '可启动的子流程编号(多个用逗号分开)',
  `SF_X` float(11, 2) NULL DEFAULT NULL COMMENT '位置X',
  `SF_Y` float(11, 2) NULL DEFAULT NULL COMMENT '位置Y',
  `SFFields` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '审批格式字段',
  `SFShowCtrl` int(11) NULL DEFAULT NULL COMMENT '显示控制方式,枚举类型:0 可以看所有的子流程;1 仅仅可以看自己发起的子流程;',
  `SFOpenType` int(11) NULL DEFAULT NULL COMMENT '打开子流程显示,枚举类型:0 工作查看器;1 傻瓜表单轨迹查看器;',
  `FrmThreadLab` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示标签',
  `FrmThreadSta` int(11) NULL DEFAULT NULL COMMENT '组件状态,枚举类型:0 禁用;1 启用;',
  `FrmThread_X` float(11, 2) NULL DEFAULT NULL COMMENT '位置X',
  `FrmThread_Y` float(11, 2) NULL DEFAULT NULL COMMENT '位置Y',
  `FrmThread_H` float(11, 2) NULL DEFAULT NULL COMMENT '高度',
  `FrmThread_W` float(11, 2) NULL DEFAULT NULL COMMENT '宽度',
  `FrmTrackLab` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示标签',
  `FrmTrackSta` int(11) NULL DEFAULT NULL COMMENT '组件状态,枚举类型:0 禁用;1 显示轨迹图;2 显示轨迹表;',
  `FrmTrack_X` float(11, 2) NULL DEFAULT NULL COMMENT '位置X',
  `FrmTrack_Y` float(11, 2) NULL DEFAULT NULL COMMENT '位置Y',
  `FrmTrack_H` float(11, 2) NULL DEFAULT NULL COMMENT '高度',
  `FrmTrack_W` float(11, 2) NULL DEFAULT NULL COMMENT '宽度',
  `FTCLab` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示标签',
  `FTCSta` int(11) NULL DEFAULT NULL COMMENT '组件状态,枚举类型:0 禁用;1 只读;2 可设置人员;',
  `FTCWorkModel` int(11) NULL DEFAULT NULL COMMENT '工作模式,枚举类型:0 简洁模式;1 高级模式;',
  `FTC_X` float(11, 2) NULL DEFAULT NULL COMMENT '位置X',
  `FTC_Y` float(11, 2) NULL DEFAULT NULL COMMENT '位置Y',
  `FTC_H` float(11, 2) NULL DEFAULT NULL COMMENT '高度',
  `FTC_W` float(11, 2) NULL DEFAULT NULL COMMENT '宽度',
  `SelectorModel` int(11) NULL DEFAULT NULL COMMENT '显示方式,枚举类型:0 按岗位;1 按部门;2 按人员;3 按SQL;4 按SQL模版计算;5 使用通用人员选择器;6 部门与岗位的交集;7 自定义Url;8 使用通用部门岗位人员选择器;9 按岗位智能计算(操作员所在部门);',
  `FK_SQLTemplate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SQL模版',
  `FK_SQLTemplateText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SQL模版',
  `IsAutoLoadEmps` int(11) NULL DEFAULT NULL COMMENT '是否自动加载上一次选择的人员？',
  `IsSimpleSelector` int(11) NULL DEFAULT NULL COMMENT '是否单项选择(只能选择一个人)？',
  `IsEnableDeptRange` int(11) NULL DEFAULT NULL COMMENT '是否启用部门搜索范围限定(对使用通用人员选择器有效)？',
  `IsEnableStaRange` int(11) NULL DEFAULT NULL COMMENT '是否启用岗位搜索范围限定(对使用通用人员选择器有效)？',
  `SelectorP1` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '分组参数:可以为空,比如:SELECT No,Name,ParentNo FROM  Port_Dept',
  `SelectorP2` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '操作员数据源:比如:SELECT No,Name,FK_Dept FROM  Port_Emp',
  `SelectorP3` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '默认选择的数据源:比如:SELECT FK_Emp FROM  WF_GenerWorkerList WHERE FK_Node=102 AND WorkID=@WorkID',
  `SelectorP4` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '强制选择的数据源:比如:SELECT FK_Emp FROM  WF_GenerWorkerList WHERE FK_Node=102 AND WorkID=@WorkID',
  PRIMARY KEY (`NodeID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '选择器' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wf_node
-- ----------------------------
INSERT INTO `wf_node` VALUES (101, 1, '001', '请假流程-经典表单-演示', '填写请假申请单', '', 0, 0, 2, 0, 0, 1, 1, 1, 0, 0, 0, '2020-04-05 17:34', '2020-04-05 17:34', 0, '', 0, 0, 0, 0, '', 0, 0, 100, 0, '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '发送', '', '保存', 1, '子线程', 0, 0, '跳转', 0, '', '退回', 0, '', 1, '', '', 0, '抄送', 0, 0, '', '', '移交', 0, '删除', 0, '结束流程', 0, '查看父流程', 0, '打开公文', 0, '打印Html', 0, '打印pdf', 0, 0, '', '打包下载', 0, '打印单据', 0, '轨迹', 1, '挂起', 0, '查询', 0, '会签', 0, 0, '加主持人', 0, '流转自定义', 0, '公文', 0, '重要性', 0, '节点时限', 0, '分配', 0, '关注', 0, '确认', 0, '列表', 1, '批量审核', 0, '备注', 0, '帮助提示', 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '审核信息', 1, 0, '', 1, 1, 0, '审核', '同意', 0, 1, 300.00, 500.00, 0.00, 400.00, '', '', 0, 0, 0, 0, 0, '', '', '', '', 4, '前台', 1, '', 2.00, 0, 1.00, '', '', '', 0, 0, 0, 0, '', 0, 0, '', '', '', 0, 12, '', 0, 'http://', 0, '', 0, 0, '', '', '@102', '', '', '', '@101@102@104', 52, 46, '', '@IsYouLiTai=0', 300.00, 400.00, '接受人', 0, '审核', 0, '加签', 0, '打开本地', 0, '打开模板', 0, '保存', 1, '接受修订', 0, '拒绝修订', 0, '套红', 0, 1, '打印', 0, '签章', 0, '插入流程', 0, 0, 0, '下载', 0, 1, '', 1, 0, '', '打开本地', '打开模板', '保存', '接受修订', '拒绝修订', '套红按钮', 1, 0, '打印按钮', '签章按钮', '插入流程', 0, 0, '子流程', 0, 1, '启动子流程', '', 5.00, 5.00, '', 0, 0, '子线程', 0, 5.00, 5.00, 300.00, 400.00, '轨迹', 0, 5.00, 5.00, 300.00, 400.00, '流转自定义', 0, 0, 5.00, 5.00, 300.00, 400.00, 5, '', '', 1, 0, 0, 0, '', '', '', '');
INSERT INTO `wf_node` VALUES (102, 2, '001', '请假流程-经典表单-演示', '部门经理审批', '', 0, 0, 2, 0, 0, 1, 1, 1, 0, 0, 0, '2020-04-05 17:34', '2020-04-05 17:34', 0, '审核意见:@BMJLSP_Note', 0, 0, 0, 0, '', 0, 0, 100, 0, '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '发送', '', '保存', 1, '子线程', 0, 0, '跳转', 0, '', '退回', 1, '', 1, '', '', 0, '抄送', 0, 0, '', '', '移交', 0, '删除', 0, '结束流程', 0, '查看父流程', 0, '打开公文', 0, '打印Html', 0, '打印pdf', 0, 0, '', '打包下载', 0, '打印单据', 0, '轨迹', 1, '挂起', 0, '查询', 0, '会签', 0, 0, '加主持人', 0, '流转自定义', 0, '公文', 0, '重要性', 0, '节点时限', 0, '分配', 0, '关注', 0, '确认', 0, '列表', 1, '批量审核', 0, '备注', 0, '帮助提示', 0, 0, 0, 0, 0, 2, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '审核信息', 1, 0, '', 1, 1, 0, '审核', '同意', 0, 1, 300.00, 500.00, 0.00, 400.00, '', '', 0, 0, 0, 0, 0, '', '', '', '', 4, '审核', 0, '', 1.00, 1, 1.00, '', '', '', 0, 0, 0, 0, '', 0, 0, '', '', '', 0, 12, '', 0, 'http://', 0, '', 1, 0, '', '', '@103', '', '', '', '@101@102@104', 244, 221, '', '@IsYouLiTai=0', 300.00, 400.00, '接受人', 0, '审核', 0, '加签', 0, '打开本地', 0, '打开模板', 0, '保存', 1, '接受修订', 0, '拒绝修订', 0, '套红', 0, 1, '打印', 0, '签章', 0, '插入流程', 0, 0, 0, '下载', 0, 1, '', 1, 0, '', '打开本地', '打开模板', '保存', '接受修订', '拒绝修订', '套红按钮', 1, 0, '打印按钮', '签章按钮', '插入流程', 0, 0, '子流程', 0, 1, '启动子流程', '', 5.00, 5.00, '', 0, 0, '子线程', 0, 5.00, 5.00, 300.00, 400.00, '轨迹', 0, 5.00, 5.00, 300.00, 400.00, '流转自定义', 0, 0, 5.00, 5.00, 300.00, 400.00, 5, '', '', 1, 0, 0, 0, '', '', '', '');
INSERT INTO `wf_node` VALUES (103, 3, '001', '请假流程-经典表单-演示', '总经理审批', '', 0, 0, 2, 0, 0, 1, 1, 1, 0, 0, 0, '2020-04-05 17:34', '2020-04-05 17:34', 0, '审核意见:@ZJLSP_Note', 0, 0, 0, 0, '', 0, 0, 100, 0, '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '发送', '', '保存', 1, '子线程', 0, 0, '跳转', 0, '', '退回', 1, '', 1, '', '', 0, '抄送', 0, 0, '', '', '移交', 0, '删除', 0, '结束流程', 0, '查看父流程', 0, '打开公文', 0, '打印Html', 0, '打印pdf', 0, 0, '', '打包下载', 0, '打印单据', 0, '轨迹', 1, '挂起', 0, '查询', 0, '会签', 0, 0, '加主持人', 0, '流转自定义', 0, '公文', 0, '重要性', 0, '节点时限', 0, '分配', 0, '关注', 0, '确认', 0, '列表', 1, '批量审核', 0, '备注', 0, '帮助提示', 0, 0, 0, 0, 0, 2, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '审核信息', 1, 0, '', 1, 1, 0, '审核', '同意', 0, 1, 300.00, 500.00, 0.00, 400.00, '', '', 0, 0, 0, 0, 1, '', '', '', '', 14, '审核.png', 0, '', 1.00, 1, 1.00, '', '', '', 0, 0, 0, 0, '', 0, 0, '', '', '', 0, 12, '', 0, 'http://', 0, '', 1, 0, '@01', '@01', '@104', '', '', '', '@103', 421, 84, '', '@IsYouLiTai=0', 300.00, 400.00, '接受人', 0, '审核', 0, '加签', 0, '打开本地', 0, '打开模板', 0, '保存', 1, '接受修订', 0, '拒绝修订', 0, '套红', 0, 1, '打印', 0, '签章', 0, '插入流程', 0, 0, 0, '下载', 0, 1, '', 1, 0, '', '打开本地', '打开模板', '保存', '接受修订', '拒绝修订', '套红按钮', 1, 0, '打印按钮', '签章按钮', '插入流程', 0, 0, '子流程', 0, 1, '启动子流程', '', 5.00, 5.00, '', 0, 0, '子线程', 0, 5.00, 5.00, 300.00, 400.00, '轨迹', 0, 5.00, 5.00, 300.00, 400.00, '流转自定义', 0, 0, 5.00, 5.00, 300.00, 400.00, 5, '', '', 1, 0, 0, 0, '', '', '', '');
INSERT INTO `wf_node` VALUES (104, 4, '001', '请假流程-经典表单-演示', '反馈给申请人', '', 0, 0, 2, 0, 0, 1, 1, 1, 0, 0, 0, '2020-04-05 17:34', '2020-04-05 17:34', 0, '', 0, 0, 0, 0, '', 0, 0, 100, 0, '', 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '发送', '', '保存', 1, '子线程', 0, 0, '跳转', 0, '', '退回', 1, '', 1, '', '', 0, '抄送', 0, 0, '', '', '移交', 0, '删除', 0, '结束流程', 0, '查看父流程', 0, '打开公文', 0, '打印Html', 0, '打印pdf', 0, 0, '', '打包下载', 0, '打印单据', 0, '轨迹', 1, '挂起', 0, '查询', 0, '会签', 0, 0, '加主持人', 0, '流转自定义', 0, '公文', 0, '重要性', 0, '节点时限', 0, '分配', 0, '关注', 0, '确认', 0, '列表', 1, '批量审核', 0, '备注', 0, '帮助提示', 0, 0, 0, 0, 0, 2, 1, 0, 0, 0, '', 0, 0, 0, 0, 0, '', '', '', '审核信息', 1, 0, '', 1, 1, 0, '审核', '同意', 0, 1, 300.00, 500.00, 0.00, 400.00, '', '', 0, 0, 0, 0, 1, '', '', '', '', 7, '审核.png', 0, '', 1.00, 1, 1.00, '', '', '', 0, 0, 0, 0, '', 0, 0, '', '', '', 0, 12, '', 0, 'http://', 0, '', 2, 0, '', '', '', '', '', '', '@101@102@104', 580, 190, '', '@IsYouLiTai=0', 300.00, 400.00, '接受人', 0, '审核', 0, '加签', 0, '打开本地', 0, '打开模板', 0, '保存', 1, '接受修订', 0, '拒绝修订', 0, '套红', 0, 1, '打印', 0, '签章', 0, '插入流程', 0, 0, 0, '下载', 0, 1, '', 1, 0, '', '打开本地', '打开模板', '保存', '接受修订', '拒绝修订', '套红按钮', 1, 0, '打印按钮', '签章按钮', '插入流程', 0, 0, '子流程', 0, 1, '启动子流程', '', 5.00, 5.00, '', 0, 0, '子线程', 0, 5.00, 5.00, 300.00, 400.00, '轨迹', 0, 5.00, 5.00, 300.00, 400.00, '流转自定义', 0, 0, 5.00, 5.00, 300.00, 400.00, 5, '', '', 1, 0, 0, 0, '', '', '', '');

-- ----------------------------
-- Table structure for wf_nodecancel
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodecancel`;
CREATE TABLE `wf_nodecancel`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `CancelTo` int(11) NOT NULL COMMENT '撤销到 - 主键',
  PRIMARY KEY (`FK_Node`, `CancelTo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '可撤销的节点' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_nodedept
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodedept`;
CREATE TABLE `wf_nodedept`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门,主外键:对应物理表:Port_Dept,表描述:部门',
  PRIMARY KEY (`FK_Node`, `FK_Dept`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点部门' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_nodeemp
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodeemp`;
CREATE TABLE `wf_nodeemp`  (
  `FK_Node` int(11) NOT NULL COMMENT 'Node - 主键',
  `FK_Emp` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '到人员,主外键:对应物理表:Port_Emp,表描述:用户',
  PRIMARY KEY (`FK_Node`, `FK_Emp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点人员' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_nodereturn
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodereturn`;
CREATE TABLE `wf_nodereturn`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `ReturnTo` int(11) NOT NULL COMMENT '退回到 - 主键',
  `Dots` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '轨迹信息',
  PRIMARY KEY (`FK_Node`, `ReturnTo`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '可退回的节点' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_nodestation
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodestation`;
CREATE TABLE `wf_nodestation`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Station` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '工作岗位,主外键:对应物理表:Port_Station,表描述:岗位',
  PRIMARY KEY (`FK_Node`, `FK_Station`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点岗位' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wf_nodestation
-- ----------------------------
INSERT INTO `wf_nodestation` VALUES (103, '01');

-- ----------------------------
-- Table structure for wf_nodesubflow
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodesubflow`;
CREATE TABLE `wf_nodesubflow`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主流程编号',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '主流程节点',
  `SubFlowType` int(11) NULL DEFAULT NULL COMMENT '子流程类型',
  `SubFlowModel` int(11) NULL DEFAULT NULL COMMENT '子流程模式',
  `IsAutoSendSubFlowOver` int(11) NULL DEFAULT NULL COMMENT '父子流程结束规则',
  `IsAutoSendSLSubFlowOver` int(11) NULL DEFAULT NULL COMMENT '同级子流程结束规则',
  `SubFlowNo` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子流程编号',
  `SubFlowName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子流程名称',
  `StartOnceOnly` int(11) NULL DEFAULT NULL COMMENT '仅能被调用1次',
  `IsEnableSpecFlowStart` int(11) NULL DEFAULT NULL COMMENT '指定的流程启动后,才能启动该子流程(请在文本框配置子流程).',
  `SpecFlowStart` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子流程编号',
  `SpecFlowStartNote` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `IsEnableSpecFlowOver` int(11) NULL DEFAULT NULL COMMENT '指定的流程结束后,才能启动该子流程(请在文本框配置子流程).',
  `SpecFlowOver` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子流程编号',
  `SpecFlowOverNote` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ExpType` int(11) NULL DEFAULT NULL COMMENT '表达式类型',
  `CondExp` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '条件表达式',
  `YBFlowReturnRole` int(11) NULL DEFAULT NULL COMMENT '退回方式',
  `ReturnToNode` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '要退回的节点',
  `SendModel` int(11) NULL DEFAULT NULL COMMENT '自动触发的子流程发送方式',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序',
  `InvokeTime` int(11) NULL DEFAULT NULL COMMENT '调用时间,枚举类型:0 发送时;1 工作到达时;',
  `CompleteReStart` int(11) NULL DEFAULT NULL COMMENT '该子流程运行结束后才可以重新发起.',
  `IsEnableSQL` int(11) NULL DEFAULT NULL COMMENT '按照指定的SQL配置.',
  `SpecSQL` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SQL语句',
  `IsEnableSameLevelNode` int(11) NULL DEFAULT NULL COMMENT '按照指定平级子流程节点完成后启动.',
  `SameLevelNode` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '平级子流程节点',
  `ReturnToNodeText` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '要退回的节点',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '子流程(所有类型子流程属性)' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_nodeteam
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodeteam`;
CREATE TABLE `wf_nodeteam`  (
  `FK_Node` int(11) NOT NULL COMMENT '节点 - 主键',
  `FK_Team` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户组,主外键:对应物理表:Port_Team,表描述:用户组',
  PRIMARY KEY (`FK_Node`, `FK_Team`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '节点岗位' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_nodetoolbar
-- ----------------------------
DROP TABLE IF EXISTS `wf_nodetoolbar`;
CREATE TABLE `wf_nodetoolbar`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `Title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `ExcType` int(11) NULL DEFAULT NULL COMMENT '执行类型,枚举类型:0 超链接;1 函数;',
  `UrlExt` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '连接/函数',
  `Target` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '目标',
  `ShowWhere` int(11) NULL DEFAULT NULL COMMENT '显示位置,枚举类型:0 树形表单;1 工具栏;2 抄送工具栏;',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `MyFileName` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `MyFilePath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFilePath',
  `MyFileExt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MyFileExt',
  `WebPath` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'WebPath',
  `MyFileH` int(11) NULL DEFAULT NULL COMMENT 'MyFileH',
  `MyFileW` int(11) NULL DEFAULT NULL COMMENT 'MyFileW',
  `MyFileSize` float NULL DEFAULT NULL COMMENT 'MyFileSize',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '自定义工具栏' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_part
-- ----------------------------
DROP TABLE IF EXISTS `wf_part`;
CREATE TABLE `wf_part`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `PartType` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `Tag0` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag0',
  `Tag1` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag1',
  `Tag2` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag2',
  `Tag3` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag3',
  `Tag4` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag4',
  `Tag5` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag5',
  `Tag6` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag6',
  `Tag7` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag7',
  `Tag8` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag8',
  `Tag9` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag9',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '配件' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_powermodel
-- ----------------------------
DROP TABLE IF EXISTS `wf_powermodel`;
CREATE TABLE `wf_powermodel`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Model` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块',
  `PowerFlag` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `PowerFlagName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标记名称',
  `PowerCtrlType` int(11) NULL DEFAULT NULL COMMENT '控制类型,枚举类型:0 岗位;1 人员;',
  `EmpNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员编号',
  `EmpName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员名称',
  `StaNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位编号',
  `StaName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '岗位名称',
  `FlowNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `FrmID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表单ID',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限模型' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_pushmsg
-- ----------------------------
DROP TABLE IF EXISTS `wf_pushmsg`;
CREATE TABLE `wf_pushmsg`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `FK_Event` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '事件类型',
  `PushWay` int(11) NULL DEFAULT NULL COMMENT '推送方式,枚举类型:0 按照指定节点的工作人员;1 按照指定的工作人员;2 按照指定的工作岗位;3 按照指定的部门;4 按照指定的SQL;5 按照系统指定的字段;',
  `PushDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '推送保存内容',
  `Tag` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Tag',
  `SMSPushWay` int(11) NULL DEFAULT NULL COMMENT '短消息发送方式',
  `SMSField` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '短消息字段',
  `SMSDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '短消息内容模版',
  `SMSNodes` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SMS节点s',
  `SMSPushModel` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '短消息发送设置',
  `MailPushWay` int(11) NULL DEFAULT NULL COMMENT '邮件发送方式',
  `MailAddress` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮件字段',
  `MailTitle` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮件标题模版',
  `MailDoc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '邮件内容模版',
  `MailNodes` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Mail节点s',
  `BySQL` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按照SQL计算',
  `ByEmps` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送给指定的人员',
  `AtPara` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'AtPara',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息推送' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wf_pushmsg
-- ----------------------------
INSERT INTO `wf_pushmsg` VALUES ('298dfb451304424dab44d596468b3ac9', '001', 104, 'SendSuccess', 0, '', '', 1, '', '', '', 'Email', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('3bfc60b05928432abfc276ee4f00dfa4', '001', 102, 'SendSuccess', 0, '', '', 1, '', '', '', 'Email', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('5803b24bccda43499c90e0feef54275f', '001', 103, 'SendSuccess', 0, '', '', 1, '', '', '', 'Email', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('704fa24d8f1b40dda71f25b4bb2f25f1', '001', 103, 'ReturnAfter', 0, '', '', 1, '', '', '', 'Email', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('807de98b45434efab35dda27b754b241', '001', 102, 'ReturnAfter', 0, '', '', 1, '', '', '', 'Email', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('900bc009636a4ab3b498e3c906919c6e', '001', 101, 'SendSuccess', 0, '', '', 1, '', '', '', 'Email', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('cd93f7e6636b415d9fd119449fed2c66', '001', 101, 'ReturnAfter', 0, '', '', 1, '', '', '', 'Email', 0, '', '', '', '', '', '', '');
INSERT INTO `wf_pushmsg` VALUES ('de3edd1f5c244a16870c2717f25d2299', '001', 104, 'ReturnAfter', 0, '', '', 1, '', '', '', 'Email', 0, '', '', '', '', '', '', '');

-- ----------------------------
-- Table structure for wf_rememberme
-- ----------------------------
DROP TABLE IF EXISTS `wf_rememberme`;
CREATE TABLE `wf_rememberme`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点',
  `FK_Emp` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当前操作人员',
  `Objs` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '分配人员',
  `ObjsExt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '分配人员Ext',
  `Emps` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '所有的工作人员',
  `EmpsExt` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '工作人员Ext',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '记忆我' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_returnwork
-- ----------------------------
DROP TABLE IF EXISTS `wf_returnwork`;
CREATE TABLE `wf_returnwork`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT 'WorkID',
  `ReturnNode` int(11) NULL DEFAULT NULL COMMENT '退回节点',
  `ReturnNodeName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回节点名称',
  `Returner` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回人',
  `ReturnerName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回人名称',
  `ReturnToNode` int(11) NULL DEFAULT NULL COMMENT 'ReturnToNode',
  `ReturnToEmp` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '退回给',
  `BeiZhu` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '退回原因',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '退回日期',
  `IsBackTracking` int(11) NULL DEFAULT NULL COMMENT '是否要原路返回?',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '退回轨迹' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_selectaccper
-- ----------------------------
DROP TABLE IF EXISTS `wf_selectaccper`;
CREATE TABLE `wf_selectaccper`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '接受人节点',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT 'WorkID',
  `FK_Emp` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'FK_Emp',
  `EmpName` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'EmpName',
  `DeptName` varchar(400) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门名称',
  `AccType` int(11) NULL DEFAULT NULL COMMENT '类型(@0=接受人@1=抄送人)',
  `Rec` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '记录人',
  `Info` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '办理意见信息',
  `IsRemember` int(11) NULL DEFAULT NULL COMMENT '以后发送是否按本次计算',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号(可以用于流程队列审核模式)',
  `Tag` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '维度信息Tag',
  `TimeLimit` int(11) NULL DEFAULT NULL COMMENT '时限-天',
  `TSpanHour` float NULL DEFAULT NULL COMMENT '时限-小时',
  `ADT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到达日期(计划)',
  `SDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应完成日期(计划)',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '选择接受/抄送人信息' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_shiftwork
-- ----------------------------
DROP TABLE IF EXISTS `wf_shiftwork`;
CREATE TABLE `wf_shiftwork`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT '工作ID',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT 'FK_Node',
  `FK_Emp` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交人',
  `FK_EmpName` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交人名称',
  `ToEmp` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交给',
  `ToEmpName` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交给名称',
  `RDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交时间',
  `Note` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '移交原因',
  `IsRead` int(11) NULL DEFAULT NULL COMMENT '是否读取？',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '移交记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_sqltemplate
-- ----------------------------
DROP TABLE IF EXISTS `wf_sqltemplate`;
CREATE TABLE `wf_sqltemplate`  (
  `No` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `SQLType` int(11) NULL DEFAULT NULL COMMENT '模版SQL类型,枚举类型:0 方向条件;1 接受人规则;2 下拉框数据过滤;3 级联下拉框;4 PopVal开窗返回值;5 人员选择器人员选择范围;',
  `Name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'SQL说明',
  `Docs` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'SQL模版',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'SQL模板' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_task
-- ----------------------------
DROP TABLE IF EXISTS `wf_task`;
CREATE TABLE `wf_task`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `Starter` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人',
  `ToNode` int(11) NULL DEFAULT NULL COMMENT '到达的节点',
  `ToEmps` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到达人员',
  `Paras` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '参数',
  `TaskSta` int(11) NULL DEFAULT NULL COMMENT '任务状态',
  `Msg` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '消息',
  `StartDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起时间',
  `RDT` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '插入数据时间',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '任务' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_testapi
-- ----------------------------
DROP TABLE IF EXISTS `wf_testapi`;
CREATE TABLE `wf_testapi`  (
  `No` varchar(92) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试过程' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_testcase
-- ----------------------------
DROP TABLE IF EXISTS `wf_testcase`;
CREATE TABLE `wf_testcase`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程编号',
  `ParaType` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数类型',
  `Vals` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '值s',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '自定义流程测试' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_testsample
-- ----------------------------
DROP TABLE IF EXISTS `wf_testsample`;
CREATE TABLE `wf_testsample`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '测试名称',
  `FK_API` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '测试的API,外键:对应物理表:WF_TestAPI,表描述:测试过程',
  `FK_Ver` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '测试的版本,外键:对应物理表:WF_TestVer,表描述:测试版本',
  `DTFrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '从',
  `DTTo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到',
  `TimeUse` float NULL DEFAULT NULL COMMENT '用时(毫秒)',
  `TimesPerSecond` float NULL DEFAULT NULL COMMENT '每秒跑多少个?',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试明细' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_testver
-- ----------------------------
DROP TABLE IF EXISTS `wf_testver`;
CREATE TABLE `wf_testver`  (
  `No` varchar(92) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '编号 - 主键',
  `Name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`No`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '测试版本' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_transfercustom
-- ----------------------------
DROP TABLE IF EXISTS `wf_transfercustom`;
CREATE TABLE `wf_transfercustom`  (
  `MyPK` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键MyPK - 主键',
  `WorkID` int(11) NULL DEFAULT NULL COMMENT 'WorkID',
  `FK_Node` int(11) NULL DEFAULT NULL COMMENT '节点ID',
  `NodeName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '节点名称',
  `Worker` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '处理人(多个人用逗号分开)',
  `WorkerName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '处理人(多个人用逗号分开)',
  `SubFlowNo` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '要经过的子流程编号',
  `PlanDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '计划完成日期',
  `TodolistModel` int(11) NULL DEFAULT NULL COMMENT '多人工作处理模式',
  `IsEnable` int(11) NULL DEFAULT NULL COMMENT '是否启用',
  `Idx` int(11) NULL DEFAULT NULL COMMENT '顺序号',
  PRIMARY KEY (`MyPK`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '自定义运行路径' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for wf_workflowdeletelog
-- ----------------------------
DROP TABLE IF EXISTS `wf_workflowdeletelog`;
CREATE TABLE `wf_workflowdeletelog`  (
  `OID` int(11) NOT NULL COMMENT 'OID - 主键',
  `FID` int(11) NULL DEFAULT NULL COMMENT 'FID',
  `FK_Dept` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门,外键:对应物理表:Port_Dept,表描述:部门',
  `Title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `FlowStarter` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起人',
  `FlowStartRDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发起时间',
  `FK_Flow` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '流程,外键:对应物理表:WF_Flow,表描述:流程',
  `FlowEnderRDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后处理时间',
  `FlowEndNode` int(11) NULL DEFAULT NULL COMMENT '停留节点',
  `FlowDaySpan` float NULL DEFAULT NULL COMMENT '跨度(天)',
  `FlowEmps` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参与人',
  `Oper` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除人员',
  `OperDept` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除人员部门',
  `OperDeptName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除人员名称',
  `DeleteNote` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '删除原因',
  `DeleteDT` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '删除日期',
  PRIMARY KEY (`OID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '流程删除日志' ROW_FORMAT = Compact;

-- ----------------------------
-- View structure for port_dept
-- ----------------------------
DROP VIEW IF EXISTS `port_dept`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_dept` AS SELECT
	o.office_code AS No,
	o.office_name AS Name,
	'' AS NameOfPath,
	( CASE WHEN ( o.parent_code = '0' ) THEN '0' ELSE o.parent_code END ) AS ParentNo,
	'' AS TreeNo,
	'admin' AS Leader,
	'' AS Tel,
	o.tree_sort AS Idx,
	( CASE WHEN ( o.tree_leaf = '0' ) THEN 1 ELSE 0 END ) AS IsDir,
	'' AS OrgNo
FROM
	js_sys_office o 
WHERE
	( o.status = '0' ) ;

-- ----------------------------
-- View structure for port_deptemp
-- ----------------------------
DROP VIEW IF EXISTS `port_deptemp`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_deptemp` AS SELECT
	concat(e.FK_Dept,'_',e.No) AS MyPk,
	e.No AS FK_Emp,
	e.FK_Dept AS FK_Dept,
	'' AS FK_Duty,
	0 AS DutyLevel,
	'admin' AS Leader,
	e.OrgNo AS OrgNo
FROM
	port_emp e 
WHERE
	( e.FK_Dept IS NOT NULL ) ;

-- ----------------------------
-- View structure for port_deptempstation
-- ----------------------------
DROP VIEW IF EXISTS `port_deptempstation`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_deptempstation` AS SELECT
	concat(e.FK_Dept,'_',e.No,'_',s.FK_Station) AS MyPk,
	e.FK_Dept AS FK_Dept,
	s.FK_Station FK_Station,
	e.No AS FK_Emp,
	e.OrgNo AS OrgNo
FROM
	port_emp e, port_empstation s
WHERE
	( e.No = s.FK_Emp AND e.FK_Dept IS NOT NULL ) ;

-- ----------------------------
-- View structure for port_deptstation
-- ----------------------------
DROP VIEW IF EXISTS `port_deptstation`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_deptstation` AS SELECT
	e.FK_Dept AS FK_Dept,
	s.FK_Station FK_Station
FROM
	port_emp e, port_empstation s
WHERE
	( e.No = s.FK_Emp AND e.FK_Dept IS NOT NULL ) ;

-- ----------------------------
-- View structure for port_emp
-- ----------------------------
DROP VIEW IF EXISTS `port_emp`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_emp` AS SELECT
	u.user_code AS No,
	e.emp_code AS EmpNo,
	u.user_name AS Name,
	'123' AS Pass,
	( CASE WHEN ( e.office_code != '' ) THEN e.office_code ELSE (
			select oo.office_code from js_sys_office oo where oo.parent_code = '0' limit 1
		) END ) AS FK_Dept,
	'' AS FK_Duty,
	'admin' AS Leader,
	u.extend_s1 AS SID,
	u.mobile AS Tel,
	u.email AS Email,
	1 AS NumOfDept,
	0 AS Idx,
	'' AS OrgNo	
FROM
	( js_sys_user u LEFT JOIN js_sys_employee e ON ( ( e.emp_code = u.ref_code ) ) ) 
WHERE
	(
		( ( u.status = '0' ) AND ( u.user_type = 'employee' ) AND ( e.status = '0' ) ) 
		OR ( u.login_code = 'system' ) 
		OR ( u.login_code = 'admin' ) 
	) ;

-- ----------------------------
-- View structure for port_empstation
-- ----------------------------
DROP VIEW IF EXISTS `port_empstation`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_empstation` AS SELECT
	ur.user_code AS FK_Emp,
	ur.role_code AS FK_Station 
FROM
	js_sys_user_role ur ;

-- ----------------------------
-- View structure for port_inc
-- ----------------------------
DROP VIEW IF EXISTS `port_inc`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_inc` AS SELECT * FROM Port_Dept WHERE (No='100' OR No='1060' OR No='1070') ;

-- ----------------------------
-- View structure for port_station
-- ----------------------------
DROP VIEW IF EXISTS `port_station`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_station` AS SELECT
	r.role_code AS No,
	r.role_name AS Name,
	r.role_type AS FK_StationType,
	'' AS DutyReq,
	'' AS Makings,
	'' AS OrgNo 
FROM
	js_sys_role r ;

-- ----------------------------
-- View structure for port_stationtype
-- ----------------------------
DROP VIEW IF EXISTS `port_stationtype`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `port_stationtype` AS SELECT
	d.dict_value AS No,
	d.dict_label AS Name,
	'' AS Idx,
	'' AS OrgNo	
FROM
	js_sys_dict_data d 
WHERE
	( d.dict_type = 'sys_role_type' ) ;

-- ----------------------------
-- View structure for v_flowstarterbpm
-- ----------------------------
DROP VIEW IF EXISTS `v_flowstarterbpm`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_flowstarterbpm` AS SELECT A.FK_Flow, a.FlowName, C.FK_Emp,C.OrgNo FROM WF_Node a, WF_NodeStation b, Port_DeptEmpStation c 
 WHERE a.NodePosType=0 AND ( a.WhoExeIt=0 OR a.WhoExeIt=2 ) 
AND  a.NodeID=b.FK_Node AND B.FK_Station=C.FK_Station   AND (A.DeliveryWay=0 OR A.DeliveryWay=14)
  UNION  
SELECT A.FK_Flow, a.FlowName, C.FK_Emp,C.OrgNo FROM WF_Node a, WF_NodeDept b, Port_DeptEmp c 
 WHERE a.NodePosType=0 AND ( a.WhoExeIt=0 OR a.WhoExeIt=2 )
AND  a.NodeID=b.FK_Node AND B.FK_Dept=C.FK_Dept   AND A.DeliveryWay=1
  UNION  
SELECT A.FK_Flow, a.FlowName, B.FK_Emp, '' as OrgNo FROM WF_Node A, WF_NodeEmp B 
 WHERE A.NodePosType=0 AND ( A.WhoExeIt=0 OR A.WhoExeIt=2 ) 
AND A.NodeID=B.FK_Node  AND A.DeliveryWay=3
  UNION 
SELECT A.FK_Flow, A.FlowName, B.No AS FK_Emp, B.OrgNo FROM WF_Node A, Port_Emp B 
 WHERE A.NodePosType=0 AND ( A.WhoExeIt=0 OR A.WhoExeIt=2 )  AND A.DeliveryWay=4
  UNION 
SELECT A.FK_Flow, a.FlowName, E.FK_Emp,E.OrgNo FROM WF_Node A, WF_NodeDept B, WF_NodeStation C,  Port_DeptEmpStation E
 WHERE a.NodePosType=0 
 AND ( a.WhoExeIt=0 OR a.WhoExeIt=2 ) 
 AND  A.NodeID=B.FK_Node 
 AND A.NodeID=C.FK_Node 
 AND B.FK_Dept=E.FK_Dept 
 AND C.FK_Station=E.FK_Station AND A.DeliveryWay=9
 UNION 
 SELECT  A.FK_Flow, A.FlowName, C.No as FK_Emp, B.OrgNo FROM WF_Node A, WF_FlowOrg B, Port_Emp C
 WHERE A.FK_Flow=B.FlowNo AND B.OrgNo=C.OrgNo
 AND  A.DeliveryWay=22 ;

-- ----------------------------
-- View structure for v_myflowdata
-- ----------------------------
DROP VIEW IF EXISTS `v_myflowdata`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_myflowdata` AS SELECT A.*, B.EmpNo as MyEmpNo FROM WF_GenerWorkflow A, WF_PowerModel B  WHERE  A.FK_Flow=B.FlowNo AND B.PowerCtrlType=1 AND A.WFState>= 2    UNION   SELECT A.*, c.No as MyEmpNo FROM WF_GenerWorkflow A, WF_PowerModel B, Port_Emp C, Port_DeptEmpStation D WHERE  A.FK_Flow=B.FlowNo  AND B.PowerCtrlType=0 AND C.No=D.FK_Emp AND B.StaNo=D.FK_Station AND A.WFState>=2 ;

-- ----------------------------
-- View structure for v_totalch
-- ----------------------------
DROP VIEW IF EXISTS `v_totalch`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_totalch` AS SELECT FK_Emp, (SELECT count(MyPK) AS Num FROM WF_CH A WHERE A.FK_Emp=WF_CH.FK_Emp) as  AllNum,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta <=1 AND A.FK_Emp=WF_CH.FK_Emp) as  ASNum,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta >=2 AND A.FK_Emp=WF_CH.FK_Emp) as  CSNum,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta =0 AND A.FK_Emp=WF_CH.FK_Emp) as  JiShi,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta =1 AND A.FK_Emp=WF_CH.FK_Emp) as  AnQi,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta =2 AND A.FK_Emp=WF_CH.FK_Emp) as  YuQi,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta =3 AND A.FK_Emp=WF_CH.FK_Emp) as  ChaoQi,
ROUND( (SELECT  convert(count(MyPK),DECIMAL) AS Num FROM WF_CH A WHERE CHSta <=1 AND A.FK_Emp=WF_CH.FK_Emp)/(SELECT count(MyPK) AS Num FROM WF_CH A WHERE  A.FK_Emp=WF_CH.FK_Emp)*100,2) 
AS WCRate
FROM WF_CH GROUP BY FK_Emp ;

-- ----------------------------
-- View structure for v_totalchweek
-- ----------------------------
DROP VIEW IF EXISTS `v_totalchweek`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_totalchweek` AS SELECT FK_Emp,WeekNum,FK_NY, (SELECT count(MyPK) AS Num FROM WF_CH A WHERE A.FK_Emp=WF_CH.FK_Emp AND A.WeekNum=WF_CH.WeekNum) as  AllNum,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta <=1 AND A.FK_Emp=WF_CH.FK_Emp AND A.WeekNum=WF_CH.WeekNum) as  ASNum,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta >=2 AND A.FK_Emp=WF_CH.FK_Emp AND A.WeekNum=WF_CH.WeekNum) as  CSNum,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta =0 AND A.FK_Emp=WF_CH.FK_Emp AND A.WeekNum=WF_CH.WeekNum) as  JiShi,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta =1 AND A.FK_Emp=WF_CH.FK_Emp AND A.WeekNum=WF_CH.WeekNum) as  AnQi,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta =2 AND A.FK_Emp=WF_CH.FK_Emp AND A.WeekNum=WF_CH.WeekNum) as  YuQi,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta =3 AND A.FK_Emp=WF_CH.FK_Emp AND A.WeekNum=WF_CH.WeekNum) as  ChaoQi,
ROUND( (SELECT  convert(count(MyPK),DECIMAL) AS Num FROM WF_CH A WHERE CHSta <=1 AND A.FK_Emp=WF_CH.FK_Emp AND A.WeekNum=WF_CH.WeekNum )/(SELECT count(MyPK) AS Num FROM WF_CH A WHERE  A.FK_Emp=WF_CH.FK_Emp AND A.WeekNum=WF_CH.WeekNum)*100,2) 
AS WCRate
FROM WF_CH GROUP BY FK_Emp,WeekNum,FK_NY ;

-- ----------------------------
-- View structure for v_totalchyf
-- ----------------------------
DROP VIEW IF EXISTS `v_totalchyf`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_totalchyf` AS SELECT FK_Emp,FK_NY, (SELECT count(MyPK) AS Num FROM WF_CH A WHERE A.FK_Emp=WF_CH.FK_Emp AND A.FK_NY=WF_CH.FK_NY) as  AllNum,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta <=1 AND A.FK_Emp=WF_CH.FK_Emp AND A.FK_NY=WF_CH.FK_NY) as  ASNum,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta >=2 AND A.FK_Emp=WF_CH.FK_Emp AND A.FK_NY=WF_CH.FK_NY) as  CSNum,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta =0 AND A.FK_Emp=WF_CH.FK_Emp AND A.FK_NY=WF_CH.FK_NY) as  JiShi,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta =1 AND A.FK_Emp=WF_CH.FK_Emp AND A.FK_NY=WF_CH.FK_NY) as  AnQi,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta =2 AND A.FK_Emp=WF_CH.FK_Emp AND A.FK_NY=WF_CH.FK_NY) as  YuQi,
(SELECT count(MyPK) AS Num FROM WF_CH A WHERE CHSta =3 AND A.FK_Emp=WF_CH.FK_Emp AND A.FK_NY=WF_CH.FK_NY) as  ChaoQi,
ROUND( (SELECT  convert(count(MyPK),DECIMAL)  AS Num FROM WF_CH A WHERE CHSta <=1 AND A.FK_Emp=WF_CH.FK_Emp AND A.FK_NY=WF_CH.FK_NY )/(SELECT count(MyPK) AS Num FROM WF_CH A WHERE  A.FK_Emp=WF_CH.FK_Emp AND A.FK_NY=WF_CH.FK_NY)*100,2) 
AS WCRate
FROM WF_CH GROUP BY FK_Emp,FK_NY ;

-- ----------------------------
-- View structure for v_wf_authtodolist
-- ----------------------------
DROP VIEW IF EXISTS `v_wf_authtodolist`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_wf_authtodolist` AS SELECT B.FK_Emp Auther,B.FK_EmpText AuthName,A.PWorkID,A.FK_Node,A.FID,A.WorkID,C.EmpNo,  C.TakeBackDT, A.FK_Flow, A.FlowName,A.Title  FROM WF_GenerWorkFlow A, WF_GenerWorkerlist B, WF_Auth C WHERE A.WorkID=B.WorkID AND C.AuthType=1 AND B.FK_Emp=C.Auther AND B.IsPass=0 AND B.IsEnable=1 AND A.FK_Node = B.FK_Node AND A.WFState >=2    UNION   SELECT B.FK_Emp Auther,B.FK_EmpText AuthName,A.PWorkID,A.FK_Node,A.FID,A.WorkID, C.EmpNo, C.TakeBackDT, A.FK_Flow, A.FlowName,A.Title FROM WF_GenerWorkFlow A, WF_GenerWorkerlist B, WF_Auth C WHERE A.WorkID=B.WorkID AND C.AuthType=2 AND B.FK_Emp=C.Auther AND B.IsPass=0 AND B.IsEnable=1 AND A.FK_Node = B.FK_Node AND A.WFState >=2 AND A.FK_Flow=C.FlowNo ;

-- ----------------------------
-- View structure for v_wf_delay
-- ----------------------------
DROP VIEW IF EXISTS `v_wf_delay`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_wf_delay` AS SELECT  CONCAT(WorkID,'_',FK_Emp,'_',FK_Node) AS MyPK, PRI, WorkID, IsRead, Starter, StarterName, WFState, FK_Dept, DeptName, FK_Flow, 
                      FlowName, PWorkID, PFlowNo, FK_Node, NodeName, WorkerDept, Title, RDT, ADT, SDT, FK_Emp, FID, FK_FlowSort, SysType, SDTOfNode, PressTimes, GuestNo, GuestName, BillNo, FlowNote, 
                      TodoEmps, TodoEmpsNum, TodoSta, TaskSta, ListType, Sender, AtPara, MyNum
FROM   WF_EmpWorks
WHERE SDT>NOW() ;

-- ----------------------------
-- View structure for wf_empworks
-- ----------------------------
DROP VIEW IF EXISTS `wf_empworks`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `wf_empworks` AS SELECT A.PRI,A.WorkID,B.IsRead, A.Starter,A.StarterName,A.WFState,A.FK_Dept,A.DeptName, A.FK_Flow, A.FlowName,A.PWorkID,
A.PFlowNo,B.FK_Node, B.FK_NodeText AS NodeName,B.FK_Dept as WorkerDept, A.Title, A.RDT, B.RDT AS ADT, 
B.SDT, B.FK_Emp,B.FID ,A.FK_FlowSort,A.SysType,A.SDTOfNode,B.PressTimes,
A.GuestNo,A.GuestName,A.BillNo,A.FlowNote,A.TodoEmps,A.TodoEmpsNum,A.TodoSta,A.TaskSta,0 as ListType,A.Sender,A.AtPara,
1 as MyNum
FROM  WF_GenerWorkFlow A, WF_GenerWorkerlist B
WHERE     (B.IsEnable = 1) AND (B.IsPass = 0)
 AND A.WorkID = B.WorkID AND A.FK_Node = B.FK_Node AND A.WFState!=0  AND WhoExeIt!=1
 UNION
SELECT A.PRI,A.WorkID,B.Sta AS IsRead, A.Starter,
A.StarterName,2 AS WFState,A.FK_Dept,A.DeptName, A.FK_Flow, A.FlowName,A.PWorkID,
A.PFlowNo,B.FK_Node, B.NodeName, B.CCToDept as WorkerDept, A.Title, A.RDT, B.RDT AS ADT, 
B.RDT AS SDT, B.CCTo as FK_Emp,B.FID ,A.FK_FlowSort,A.SysType,A.SDTOfNode, 0 as PressTimes,
A.GuestNo,A.GuestName,A.BillNo,A.FlowNote,A.TodoEmps,A.TodoEmpsNum,0 as TodoSta,0 AS TaskSta,1 as ListType,B.Rec as Sender,
'@IsCC=1'||A.AtPara as AtPara,
1 as MyNum
  FROM WF_GenerWorkFlow A, WF_CCList B WHERE A.WorkID=B.WorkID AND  B.Sta <=1 AND B.InEmpWorks = 1 AND A.WFState!=0 ;

SET FOREIGN_KEY_CHECKS = 1;
