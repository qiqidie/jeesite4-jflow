package BP.WF.Port.Admin2;

import BP.DA.*;
import BP.En.*;
import BP.WF.*;
import BP.WF.Port.*;
import java.util.*;

/** 
 岗位属性
*/
public class StationAttr extends EntityNoNameAttr
{
	/** 
	 岗位类型
	*/
	public static final String FK_StationType = "FK_StationType";
	/** 
	 组织编号
	*/
	public static final String OrgNo = "OrgNo";
	/** 
	 顺序号
	*/
	public static final String Idx = "Idx";

}